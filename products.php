<?php
$page_title = "Products";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

$done = true;
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    
    //print_r($_POST); //DEBUG INFO 
    if (isset($_POST["txtProdCode"])) // Checks if the submit is from ADD NEW...
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "insert into product (ProductCode, Description, Packsize) select :prodcode, :desc, :packsize ";    
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':prodcode', $_POST["txtProdCode"], PDO::PARAM_STR);
        $stmt->bindParam(':desc', $_POST["txtDescription"], PDO::PARAM_STR);
        $stmt->bindParam(':packsize', $_POST["txtPacksize"], PDO::PARAM_STR);
        $stmt->Execute();
    }
    if (isset($_POST["txtEditId"])) // Checks if the submit is from EDIT....
    {
        //echo "I TRIGGERED";
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "update product set ProductCode = :prodc, Description = :desc, Packsize = :packsize where Id = :id"; 
        $stmt = $db->prepare($sql); 
        $stmt->bindParam(':prodc', $_POST["txtEditProd"], PDO::PARAM_STR);
        $stmt->bindParam(':desc', $_POST["txtEditDesc"], PDO::PARAM_STR);
        $stmt->bindParam(':packsize', $_POST["txtEditPackS"], PDO::PARAM_STR);
        $stmt->bindParam(':id', $_POST["txtEditId"], PDO::PARAM_INT);
        $stmt->Execute();
    }
    if (isset($_POST["txtDeleteId"]))
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        /*
        $sql = "delete from price where ProductId = :id";
        $stmt = $db->prepare($sql);  
        $stmt->bindParam(':id', $_POST["txtDeleteId"], PDO::PARAM_INT);
        $stmt->Execute();
        */
        $sql = "update product set Deleted=1 where Id = :id";
        try{
            $stmt = $db->prepare($sql);  
            $stmt->bindParam(':id', $_POST["txtDeleteId"], PDO::PARAM_INT);
            $stmt->Execute();
        }catch(PDOException $e){
            $done = false;
        }
        
        
        //echo json_encode($done);
        //exit();
    }
    if(isset($_POST["txtPriceId"]))
    {
        //connect to the database
        $db = connectDb();   
        //extra bit for adding new price to a �0 (where we need to de-null the dateTo field
        $sql = "update price set DateTo=now() where DateTo is null and ProductId=:id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":id", $_POST["txtPriceId"], PDO::PARAM_INT);
        $stmt->Execute(); 
        //sql query as string
        $sql = "insert into price (ProductId, Price, Notes) select :prodid, :price, :notes";
        $stmt = $db->prepare($sql);  
        $stmt->bindParam(':prodid', $_POST["txtPriceId"], PDO::PARAM_INT);
        $stmt->bindParam(':price', $_POST["txtNewPrice"], PDO::PARAM_STR);
        $stmt->bindParam(':notes', $_POST["txtNewPriceNotes"], PDO::PARAM_STR);
        $stmt->Execute();
    }
}

include ('includes/templates/header.html');
if ($done){
    echo '<input type="hidden" value="'.$done.'" name="ajaxanswer" id="ajaxanswer">';
}
?>


<div id="page-header"><h2>Products ::</h2></div>


<div class="row">
    <div class="col-md-6">
        <input type="button" value="Add New Product" class="btn btn-primary" onclick="addProduct();" />
        <input type="button" value="Toggle Search" class="btn btn-primary" onclick="toggle('divSearch');" />
        <button type="button" class="btn btn-default" onclick="location.reload();">Reset Search</button>
    </div>
    <div class="col-md-6">
        <p class="pull-right">
            <?php 
            $db = connectDb(); 
            $sql = "SELECT count(*) FROM product where Deleted=0 and Id not in (Select ProductId from price) or Id in (Select ProductId from price where DateTo is null and Price=0)";
            $result = $db->query($sql);
            $row = $result->fetch();
            echo "There are currently " . 
                    "<input type =\"submit\" value=\" " . $row[0] . "\" class=\"btn btn-danger\" onclick=\"addMissingPrices();\"/>" . 
                " Products without a price!";
            ?>
        </p>
        <p class="pull-right">
            <?php 
            $db = connectDb(); 
            $sql = "SELECT count(*) FROM product where Deleted=0 and Id not in (Select ProductId from measure where ForCC=1) and ( Id in (Select ProductIdLeft from productinvoicelink) or Id in (Select ProductId from productmatch) )";
            $result = $db->query($sql);
            $row = $result->fetch();
            echo "There are currently " . 
                    "<input type =\"submit\" value=\" " . $row[0] . "\" class=\"btn btn-danger\" onclick=\"addMissingMeasures();\"/>" . 
                " Matched products requiring CC measure!";
            ?>
        </p>
    </div> 
</div> 

<div id="divSearch" style="display:none;">
    <form id="form1" action="products.php" class="form-inline" method="GET"> 
    <div class="form-group">
        <input type="text" name="search" id="search" placeholder="Search Product Description..." class="form-control" /> </div>
        ||
        <div class="form-group">
        <input type ="submit" value="Search" class="btn btn-primary" />
        </div> 
    </form>
</div>

<div class="table-responsive" id="divViewProducts">
    <p>All Products listed below (Click price to see price history):</p>
    <table class="table table-bordered table-hover text-center">
        <tr>
           <!-- <th>ID</th> -->
            <th>Product Code</th>
            <th>Description</th>
            <th>Packsize</th>
            <th>Price</th>
            <th>Supplier</th>
            <th>Edit</th>
            <th>Delete</th>
            <th>Manage</th>
        </tr>
        <?php
        
        //connect to the database
        $db = connectDb(); 
        if (isset($_GET["search"])) {
            $sql = "SELECT product.Id as ProdId, product.ProductCode, product.Description, product.Packsize, price.Price, supplier.Name
        FROM product 
        inner join price on product.Id = price.ProductId 
        left join productsupplierlink as psl on product.Id=psl.ProductId 
        left join supplier on psl.SupplierId = supplier.Id 
        where Deleted=0 and DateTo is null and  product.Description like '%" . $_GET["search"] . "%' order by 1";
        }
        else {  $sql = "SELECT product.Id as ProdId, product.ProductCode, product.Description, product.Packsize, price.Price, supplier.Name
        FROM product 
        inner join price on product.Id = price.ProductId 
        left join productsupplierlink as psl on product.Id=psl.ProductId 
        left join supplier on psl.SupplierId = supplier.Id 
        where Deleted=0 and DateTo is null order by 1 "; }
        
        //final select statement option, for products by supplierId = this would only happen if NOT searching !
        if (isset($_GET["suppId"])) {
            $sql = "SELECT product.Id as ProdId, product.ProductCode, product.Description, product.Packsize, price.Price, supplier.Name
        FROM product 
        inner join price on product.Id = price.ProductId 
        left join productsupplierlink as psl on product.Id=psl.ProductId 
        left join supplier on psl.SupplierId = supplier.Id 
        where Deleted=0 and DateTo is null and  psl.SupplierId = " . $_GET["suppId"] . " order by 1";
        }
        
        $result = $db->query($sql);
        $db = null;  
        
        if (isset($_GET["page"])) {$page=$_GET["page"];}
        else { $page=0; }
        $page = $page * 100; //Sets the start of row set.
        $start=0; $counter=0; //counter to only bring 100 rows...
        while ($row = $result->fetch())
        { 
            if ($start < $page) { /*Do nothing */ $start=$start+1; }
            else
            {
                if ($counter<100)
                {
                    echo '<tr>';
                    //echo '<td>' .  $row['ProdId'] .  '</td>';
                    echo '<td>' .  $row['ProductCode'] .  '</td>';
                    echo '<td>' .  $row['Description'] .  '</td>';
                    echo '<td><a href="productmeasures.php?prodid=' . $row['ProdId'] . '">' .  htmlspecialchars($row['Packsize']) .  '</a></td>';
                    echo '<td><a href="pricehistory.php?prodid=' . $row['ProdId'] . '">' .  htmlspecialchars($row['Price']) .  '</a></td>';
                    echo '<td>' . $row['Name'] . '</td>';
                    echo '<td> <input type="button" value="edit" class="btn btn-info editBtn" onclick="grabForEdit(\''.$row['ProdId'].'\',\''.htmlspecialchars($row['ProductCode']).'\',\''.htmlspecialchars(addslashes($row['Description'])).'\',\''.htmlspecialchars($row['Packsize']).'\')"  /></td>';
                    echo '<td> <button type="button" value="'.$row['ProdId'].'" class="btn btn-danger delBtn">Delete</button></td>';
                    echo '<td> <a href="manageproduct.php?pid=' . $row['ProdId'] . '" class="btn btn-primary">Manage</a></td>';
                    echo '</tr>';
                    $counter=$counter+1;
                }
            } 
        }
        /*
        print_r($result); echo '<br />';
        
        print_r( $result->fetch());
          */
        ?>
        


    </table>
<?php
/* // THIS METHOD GIVES ALL PAGE NUMBERS! 
    $db = connectDb(); 
    if (isset($_GET["search"])) { $sql = "select count(*) from product where product.Description like '%" . $_GET["search"] . "%' order by 1";}
    else { $sql = "select count(*) from product";  }
    
    //final select statement option, for products by supplierId = this would only happen if NOT searching !
    if (isset($_GET["suppId"])) { $sql = "select count(*) from product where Id in (Select ProductId from productsupplierlink where SupplierId = " . $_GET["suppId"] .")" ;}
    
    $result = $db->query($sql);
    $db = null;  
    $row = $result->fetch(); 
    echo '<ul class="pagination">'; 
    
    for ($i =0; $i<((int)$row[0]/100); $i=$i+1)
    { 
        //final select statement option, for products by supplierId = this would only happen if NOT searching !
        if (isset($_GET["suppId"])) 
            echo '<li><a href=\'products.php?page=' . $i . '&suppId=' . $_GET["suppId"] . '\'>'. $i . '</a></li>';
        else 
        {     
            if (isset($_GET["search"])) 
                echo '<li><a href=\'products.php?page=' . $i . '&search=' . $_GET["search"] . '\'>'. $i . '</a></li>';
            else
                echo '<li><a href=\'products.php?page=' . $i . '\'>'. $i . '</a></li>';
        }  
    }
    echo '</ul>';
    */
// This method gives prev, and next only.
echo '<p>';
echo '<ul class="pagination">'; 

if (isset($_GET["page"]))
    $i = $_GET["page"];
else
    $i = 0;

if (isset($_GET["suppId"])) {
    if ($i == 0)
        echo '<li><a href=\'products.php?suppId=' . $_GET["suppId"] . '\'>« Previous</a></li>';
    else
        echo '<li><a href=\'products.php?page=' . ($i-1) . '&suppId=' . $_GET["suppId"] . '\'>« Previous</a></li>';
    echo '<li><a href=\'products.php?page=' . ($i+1) . '&suppId=' . $_GET["suppId"] . '\'> Next »</a></li>';
}
else 
{     
    if (isset($_GET["search"])) {
        if ($i == 0)
            echo '<li><a href=\'products.php?search=' . $_GET["search"] . '\'>« Previous</a></li>';
        else
            echo '<li><a href=\'products.php?page=' . ($i-1) . '&search=' . $_GET["search"] . '\'>« Previous</a></li>';
        echo '<li><a href=\'products.php?page=' . ($i+1) . '&search=' . $_GET["search"] . '\'> Next »</a></li>'; 
    }
    else{
        if ($i == 0)
            echo '<li><a href=\'products.php\'>« Previous</a></li>';
        else
            echo '<li><a href=\'products.php?page=' . ($i-1) . '\'>« Previous</a></li>';
        echo '<li><a href=\'products.php?page=' . ($i+1) . '\'>Next »</a></li>';
        
    }
}  

echo '</ul>';
echo '</p>';
?>
</div>



<div id="divAddNew" style="display:none;">
    <p>Add New Product Info :</p>
    <form id="formAddNew" action="products.php" class="form-inline" method="post"> 
        <div class="form-group">
        <input type="text" name="txtProdCode" id="txtName" placeholder="Enter Product Code..." class="form-control" required="required" />   
        </div> 
        ||
        <div class="form-group">
        <input type="text" name="txtDescription" id="txtActRef" placeholder="Enter Description" class="form-control" /> </div> 
        ||
        <div class="form-group">
        <input type="text" name="txtPacksize" id="txtNotes" placeholder="Enter Packsize" class="form-control" /> </div>
        ||
        <div class="form-group">
        <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divAddNew'); showDiv('divViewProducts');">
        <input type ="submit" value="Add New" class="btn btn-primary" />
        </div> 
    </form> 
</div>

<div id="divEdit" style="display:none;">
    <p>Edit Product Info :</p>
    <form id="formEdit" action="products.php" class="form-inline" method="post"> 
        <div class="form-group">
        <input type ="text" name="txtEditId" id="txtEditId" class="form-control" readonly="true" /></div>
        || 
        <div class="form-group">
        <input type="text" name="txtEditProd" id="txtEditProd" placeholder="Enter Edited ProductName..." class="form-control" required="required" /></div> 
        ||
        <div class="form-group">
        <input type="text" name="txtEditDesc" id="txtEditDesc" placeholder="Enter Edited Description" class="form-control" /> 
        </div> 
        ||
        <div class="form-group">
        <input type="text" name="txtEditPackS" id="txtEditPackS" placeholder="Enter Edited PackSize" class="form-control" />
        </div>
        ||
        <div class="form-group">
        <input type="button" value="Cancel" class="btn btn-default cancelBtn" onclick="hideDiv('divEdit');">
        <input type ="submit" value="Edit Entry" class="btn btn-primary" />
        </div>
    </form> 
</div>

<div id="divDelete" style="display:none;">
    <p>Delete Product :</p>
    <form id="formDelete" action="products.php" class="form-inline" method="post"> 
        
        <p><span class="bg-danger">Warning you are about to delete this record, this cannot be un-done!</span>
        Are you sure you want to remove :</p>
        <p><div class="form-group">
        <input type="text" name="txtDeleteName" id="txtDeleteName" placeholder="" readonly="true" class="form-control" /> </div> 
        || Id :  
        <div class="form-group">
        <input type ="text" name="txtDeleteId" id="txtDeleteId" class="form-control" readonly="true" /> </div>  ?
        </p>
        <p>
        <div class="form-group">
        <input type="button" value="Cancel" class="btn btn-default cancelBtn" onclick="hideDiv('divDelete');">
        <input type ="submit" value="Confirm Removal" class="btn btn-danger" />
        </div> 
        </p>
     
    </form> 
</div>

<div id="row">
    <div id="divMissingPrices" style="display:none;">
        <p>Missing Prices :</p>
        <form id="formMissingPrices" action="products.php" class="form-inline" method="post">
            <p>The below entries are products without a price 
                <span class="bg-danger">While products have no price, they will not appear in the prices table!</span>
            </p>

            <!-- products without a price -->
            <table class="table table-bordered table-hover text-center">
                <tr>
                <!--    <th>ID</th> -->
                    <th>Product Code</th>
                    <th>Description</th>
                    <th>Packsize</th>
                    <th>Add Price</th>
                </tr>
                <?php
                    $db = connectDb(); 
                    $sql = "SELECT * FROM product where Deleted=0 and Id not in (Select ProductId from price) or Id in (Select ProductId from price where DateTo is null and Price=0)";
                    $result = $db->query($sql);
                    $db = null;  
        
                    while ($row = $result->fetch())
                    {
                        echo '<tr>';
                        //echo '<td>' .  $row['Id'] .  '</td>';
                        echo '<td>' .  $row['ProductCode'] .  '</td>';
                        echo '<td>' .  $row['Description'] .  '</td>';
                        echo '<td>' .  $row['Packsize'] .  '</td>';
                        echo '<td> <input type="button" value="Add Price" class="btn btn-info" onclick="grabForPrice(\''.$row['Id'].'\',\''.htmlspecialchars($row['ProductCode']).'\');" /></td>';
                        echo '</tr>';
                    }
                ?>
            </table>
        </form>
        <input type="button" value="Return to Products" class="btn btn-default" onclick="location.reload();">
     </div>
    <div id="divAddPrice" style="display:none;">
        <!-- dont know why, but need spacing here? CBA investigating, so haxx ftw--><br /><br /><br /><br />
        <form id="formAddPrice" action="products.php" class="form-inline" method="post">
            <div class="form-group">
            <input type ="text" name="txtPriceId" id="txtPriceId" class="form-control" readonly="true" /></div>
            || 
            <div class="form-group">
            <input type="text" name="txtPriceProdCode" id="txtPriceProdCode" readonly="true" class="form-control" /></div> 
            || Enter the Price for this product here : 
            <div class="form-group">
            <input type="text" name="txtNewPrice" id="txtNewPrice" placeholder="Enter Price" class="form-control" /> 
            </div> 
            || 
            <div class="form-group">
            <input type="text" name="txtNewPriceNotes" id="txtNewPriceNotes" placeholder="Enter Any Notes" class="form-control" />
            </div> 

            ||
            <div class="form-group">
            <input type="button" value="Cancel" class="btn btn-default" onclick="location.reload();">
            <input type ="submit" value="Add Price" class="btn btn-primary" />
            </div> 
        </form>
    </div>
</div>

<div id="row">
    <div id="divMissingMeasures" style="display:none;">
        <p>Missing Measures (For CC) :</p>
        <form id="form2" action="products.php" class="form-inline" method="post">
            <p>The below entries are products without a price 
                <span class="bg-danger">While products have no measure value for CC, cost comparisons cannot be calculated!</span>
            </p>

            <!-- products without a price -->
            <table class="table table-bordered table-hover text-center">
                <tr>
                    <th>Product Code</th>
                    <th>Description</th>
                    <th>Packsize</th>
                    <th>Add Measure</th>
                </tr>
                <?php
                    $db = connectDb(); 
                    $sql = "SELECT * FROM product where Deleted=0 and Id not in (Select ProductId from measure where ForCC=1) and ( Id in (Select ProductIdLeft from productinvoicelink) or Id in (Select ProductId from productmatch) )";
                    $result = $db->query($sql);
                    $db = null;  
        
                    while ($row = $result->fetch())
                    {
                        echo '<tr>';
                        echo '<td>' .  $row['ProductCode'] .  '</td>';
                        echo '<td>' .  $row['Description'] .  '</td>';
                        echo '<td>' .  $row['Packsize'] .  '</td>';
                        echo '<td><a href="productmeasures.php?prodid=' . $row['Id'] . '"><input type="button" Value="add measure" class="btn btn-primary"></a></td>';
                        echo '</tr>';
                    }
                ?>
            </table>
        </form>
        <input type="button" value="Return to Products" class="btn btn-default" onclick="location.reload();">
     </div> 
</div>


<script src="js/jsProducts.js"></script>


<?php
include ('includes/templates/footer.html');
?>
