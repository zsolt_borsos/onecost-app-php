<?php
$page_title = "Clients";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}
include ('includes/templates/header.html');

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //print_r($_POST); DEBUG INFO 
    if (isset($_POST["txtName"])) // Checks if the submit is from ADD NEW...
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "insert into client (Name, Actref, Notes) select :name, :actref, :notes ";    
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':name', $_POST["txtName"], PDO::PARAM_STR);
        $stmt->bindParam(':actref', $_POST["txtActRef"], PDO::PARAM_STR);
        $stmt->bindParam(':notes', $_POST["txtNotes"], PDO::PARAM_STR);
        $stmt->Execute();
    }
    if (isset($_POST["txtEditId"])) // Checks if the submit is from EDIT....
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "update client set Name = :name, Actref = :actref, Notes = :notes where Id = :id"; 
        $stmt = $db->prepare($sql); 
        $stmt->bindParam(':name', $_POST["txtEditName"], PDO::PARAM_STR);
        $stmt->bindParam(':actref', $_POST["txtEditActRef"], PDO::PARAM_STR);
        $stmt->bindParam(':notes', $_POST["txtEditNotes"], PDO::PARAM_STR);
        $stmt->bindParam(':id', $_POST["txtEditId"], PDO::PARAM_INT);
        $stmt->Execute();
    }
    if (isset($_POST["txtDeleteId"]))
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "delete from client where Id = :id";
        $stmt = $db->prepare($sql);  
        $stmt->bindParam(':id', $_POST["txtDeleteId"], PDO::PARAM_INT);
        $stmt->Execute();
    }
}

?>
<script src="js/jsClients.js"></script>

<div class="page-header"><h2>Clients</h2></div>

<div>
    <p>
    <input type="button" value="Add New Client" class="btn btn-primary" onclick="addClient();" />
    <input type="button" value="Toggle Search" class="btn btn-primary" onclick="toggle('divSearch');" />
    <button type="button" class="btn btn-default" onclick="location.reload();">Reset Search</button>
    </p>
</div>

<div id="divSearch" style="display:none;">
    <form id="form1" action="clients.php" class="form-inline" method="post"> 
    <div class="form-group">
        <input type="text" name="txtSearch" id="txtSearch" placeholder="Search Name..." class="form-control" /> </div>
        ||
        <div class="form-group">
        <input type ="submit" value="Search" class="btn btn-primary" />
        </div> 
    </form>
</div>

<div class="table-responsive" id="divViewClients">
    <p>All Clients listed below :</p>
    <table class="table table-bordered table-hover text-center">
        <tr>
           <!-- <th>ID</th> -->
            <th>Name</th>
            <th>Act Ref</th>
            <th>Notes</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        <?php
        
        //connect to the database
        $db = connectDb(); 
        if (isset($_POST["txtSearch"])) {  $sql = "SELECT * FROM client where Name like '%" . $_POST["txtSearch"] . "%'"; }
        else {  $sql = "SELECT * FROM client"; }
        //$sql = "SELECT * FROM client"; 
        $result = $db->query($sql);
        $db = null;  
        
        while ($row = $result->fetch())
        {
            echo '<tr>';
            //echo '<td>' .  $row['Id'] .  '</td>';
            echo '<td>' .  $row['Name'] .  '</td>';
            echo '<td>' .  $row['Actref'] .  '</td>';
            echo '<td>' .  $row['Notes'] .  '</td>';
            echo '<td> <input type="button" value="edit" class="btn btn-info" onclick="grabForEdit(\''.$row['Id'].'\',\''.$row['Name'].'\',\''.$row['Actref'].'\',\''.$row['Notes'].'\')"  /></td>';
            echo '<td> <input type="button" value="delete" class="btn btn-danger" onclick="grabForDelete(\''.$row['Id'].'\',\''.$row['Name'].'\');" /></td>';
            echo '</tr>';
        }
        
        /*
        print_r($result); echo '<br />';
        
        print_r( $result->fetch());
          */
        ?>
        


    </table>

</div>

<div id="divAddNew" style="display:none;">
    <p>Add New Client Info :</p>
    <form id="formAddNew" action="clients.php" class="form-inline" method="post"> 
        <div class="form-group">
        <input type="text" name="txtName" id="txtName" placeholder="Enter Name..." class="form-control" required="required" />   
        </div> 
        ||
        <div class="form-group">
        <input type="text" name="txtActRef" id="txtActRef" placeholder="Enter Act Ref" class="form-control" /> </div> 
        ||
        <div class="form-group">
        <input type="text" name="txtNotes" id="txtNotes" placeholder="Enter Notes" class="form-control" /> </div>
        ||
        <div class="form-group">
        <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divAddNew'); showDiv('divViewClients');">
        <input type ="submit" value="Add New" class="btn btn-primary" />
        </div> 
    </form> 
</div>

<div id="divEdit" style="display:none;">
    <p>Edit Client Info :</p>
    <form id="formEdit" action="clients.php" class="form-inline" method="post"> 
        <div class="form-group">
        <input type ="text" name="txtEditId" id="txtEditId" class="form-control" readonly="true" /></div>
        || 
        <div class="form-group">
        <input type="text" name="txtEditName" id="txtEditName" placeholder="Enter Edited Name..." class="form-control" required="required" /></div> 
        ||
        <div class="form-group">
        <input type="text" name="txtEditActRef" id="txtEditActRef" placeholder="Enter Edited Act Ref" class="form-control" /> 
        </div> 
        ||
        <div class="form-group">
        <input type="text" name="txtEditNotes" id="txtEditNotes" placeholder="Enter Edited Notes" class="form-control" />
        </div>
        ||
        <div class="form-group">
        <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divEdit');">
        <input type ="submit" value="Edit Entry" class="btn btn-primary" />
        </div> 
    </form> 
</div>

<div id="divDelete" style="display:none;">
    <form id="formDelete" action="clients.php" class="form-inline" method="post"> 
        <p>Delete Client:</p>    
        <p>
            <span class="bg-danger">Warning you are about to delete this record, this cannot be un-done!</span>
            Are you sure you want to remove :
        </p>
        <p>
            <div class="form-group">
            <input type="text" name="txtDeleteName" id="txtDeleteName" readonly="true" class="form-control"> </div> 
            Id :  
            <div class="form-group">
            <input type ="text" name="txtDeleteId" id="txtDeleteId" class="form-control" readonly="true"> </div>  ?
            <div class="form-group">
            <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divDelete');">
            <input type ="submit" value="Confirm Removal" class="btn btn-danger">
            </div> 
        </p>     
    </form> 
</div>

<?php
include ('includes/templates/footer.html');
?>
