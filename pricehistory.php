<?php
$page_title = "Price History";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}
include ('includes/templates/header.html');


if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //print_r($_POST); //DEBUG INFO 
    
    if (isset($_POST["txtProdCode"])) // Checks if the submit is from ADD NEW...
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "update price set DateTo = now() where DateTo is null and ProductId = :id ";    
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':id', $_POST["txtProdCode"], PDO::PARAM_STR);
        $stmt->Execute();
        
        $sql = "insert into price (ProductId, Price, DateFrom, Notes) select :id, :price, now(), :notes";    
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':id', $_POST["txtProdCode"], PDO::PARAM_STR);
        $stmt->bindParam(':price', $_POST["txtNewPrice"], PDO::PARAM_STR);
        $stmt->bindParam(':notes', $_POST["txtNewPriceNotes"], PDO::PARAM_STR);
        $stmt->Execute();
    }
    if (isset($_POST["txtEditPriceId"])) // Checks if the submit is from ADD NEW...
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "update price set Price = :price where Id = :id ";    
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':price', $_POST["txtEditPrice"], PDO::PARAM_STR);
        $stmt->bindParam(':id', $_POST["txtEditPriceId"], PDO::PARAM_STR);
        $stmt->Execute();
    }
}

?>


<div id="page-header"><h2>Price History ::</h2> </div> 

<div class="table-responsive" id="divViewPrices">
    <p>Complete Price History for product 
        <b id="productname"> 
            <?php 
            if (isset($_GET["prodid"]))
            {
                $id = $_GET["prodid"]; 
                $db = connectDb(); 
                $sql = "SELECT Description, Packsize from product where  Id = {$id}";
                $result = $db->query($sql);
                $row = $result->fetch();
                echo "<a href=\"products.php\">" . $row[0] . " (" . $row[1] . ") </a>"  ;
            }
            ?> 
        </b> listed below :</p>
    <table class="table table-bordered table-hover text-center">
        <tr>
            <th>Product Code</th>
            <th>From</th>
            <th>To</th>
            <th>Price</th>
            <th>Notes</th>
            <th>Type</th>
            <th>Edit</th>
        </tr>
        <?php
        
        //connect to the database
        if (isset($_GET["prodid"])){ // only attempt to load page if has prodid in querystring
            $id = $_GET["prodid"]; 
            $db = connectDb(); 
            $sql = "SELECT price.Id as EditId, product.ProductCode, price.DateFrom, price.DateTo, price.Price, price.Notes, price.Type
            FROM product inner join price on product.Id = price.ProductId 
            where price.ProductId = {$id} order by price.Id";
            
            $result = $db->query($sql);
            $db = null;  
            
            while ($row = $result->fetch())
            {
                echo '<tr>';
                echo '<td>' .  $row['ProductCode'] .  '</td>';
                echo '<td>' .  $row['DateFrom'] .  '</td>';
                echo '<td>' .  $row['DateTo'] .  '</td>';
                echo '<td>' .  $row['Price'] .  '</td>';
                echo '<td>' .  $row['Notes'] .  '</td>';
                echo '<td>' .  $row['Type'] .  '</td>';
                echo '<td> <input type="button" value="edit" class="btn btn-info" onclick="grabForEdit(\''.$row[0].'\',\''.$row['Price'].'\')"  /></td>';
                echo '</tr>';
            }
        }
        
        /*
        print_r($result); echo '<br />';
        
        print_r( $result->fetch());
          */
        ?>
    </table>
    <input type="button" value="Add New Price" class="btn btn-primary" onclick="addPrice();" />
</div>

<div id="divAddNew" style="display:none;">
    <p>Add New Price :</p>
    <form id="formAddNew" action="pricehistory.php?prodid=<?php echo $_GET["prodid"]; ?>" class="form-inline" method="post"> 
        <div class="form-group">
        <input type="text" name="txtProdCode" id="txtName" readonly="true" class="form-control" value="<?php echo $_GET["prodid"]; ?>"/>   
        </div> 
        ||
        <div class="form-group">
        <input type="text" name="txtNewPrice" id="txtNewPrice" placeholder="Enter Price" class="form-control" /> 
        </div> 
        || 
        <div class="form-group">
        <input type="text" name="txtNewPriceNotes" id="txtNewPriceNotes" placeholder="Enter Any Notes" class="form-control" />
        </div> 

        <div class="form-group">
        <input type="button" value="Cancel" class="btn btn-default" onclick="location.reload();">
        <input type ="submit" value="Add New" class="btn btn-primary" />
        </div> 
    </form> 
</div>

<div id="divEditPrice" style="display:none;">
    <p>Edit Price Below.</p> 
    <form id="form1" action="pricehistory.php?prodid=<?php echo $_GET["prodid"]; ?>" class="form-inline" method="post"> 
        <div class="form-group">
        <input type="text" name="txtEditPriceId" id="txtEditPriceId" readonly="true" class="form-control" />  
        </div> 
        ||
        <div class="form-group">
        <input type="text" name="txtEditPrice" id="txtEditPrice" placeholder="Enter Price" class="form-control" /> 
        </div>  

        <div class="form-group">
        <input type="button" value="Cancel" class="btn btn-default" onclick="location.reload();">
        <input type ="submit" value="Edit" class="btn btn-primary" />
        </div> 
    </form> 
</div>

<script src="js/jsPriceHistory.js"></script>


<?php
include ('includes/templates/footer.html');
?>
