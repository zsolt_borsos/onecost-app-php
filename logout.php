<?php # Script 12.11 - logout.php #2
// This page lets the user logout.
// This version uses sessions.
$page_title = 'Logged Out!';
include ('includes/scripts/appfunctions.php');
include ('includes/templates/header.html');

if (isset($_SESSION['myId'])) {
	// Cancel the session:
// Print a customized message:
echo "<h1>Logged Out!</h1>
<p>You are now logged out!</p>";
	echo '
	<script type="text/javascript">
	setTimeout("location.href = \'forcelogout.php\';", 1000);
	</script>
	';
		
} else { 
// If no session variable exists, redirect the user:
	// Need the functions:
	redirect_user("login.php");
}


include ('includes/templates/footer.html');
?>
