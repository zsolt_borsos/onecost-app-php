<?php //users.php
$page_title = "Users";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

include ('includes/templates/header.html');

?>

<h1>Users page</h1>
<p>List of users:</p>

<div class="table-responsive">
		<table class="table table-bordered table-hover text-center">
		<tr>
		<th scope="col">Id</th>
		<th scope="col">Firstname</th>
        <th scope="col">Surname</th>
        <th scope="col">Email</th>
        <th scope="col">Enabled</th>
        <th scope="col">Access level</th>
        <th scope="col">Password</th>
		</tr>

<?php

$result = getAllUser();

    while ($row = $result->fetch()){    
     echo "<tr>";
        echo "<td>";
            echo $row['Id'];
        echo "</td>";
     
     
     
     echo "</tr>";
    }

?>

</table>
</div>


<?php
include ('includes/templates/footer.html');
?>