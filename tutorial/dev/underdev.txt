

!!This is not ready yet!!

3. Lets check the login page.       

Lets have a look at the login page, as it is a good example. I will try to explain as much as I can to make it clear what is actually happening here.


<?php //login.php
// This page processes the login form submission.
// The script now stores the HTTP_USER_AGENT value for added security.

// set the page title
$page_title = 'Login';

//include our appfunctions. Note that our FUNCTIONS ARE IN THIS FILE. So when we want a new functionality we just add it in there
//and use it on the page wherever you need it. This will be useful later (modularity).

include ('includes/scripts/appfunctions.php');

// Check if the form has been submitted:
//now this is a common technique, we r checking if a form (or AJAX, or something (hackers...)) have POST-ed something, and not just a random user navigated to this page. POST is a HTML FORM parameter that tells us 
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		
	// Check the login:
	list ($check, $data) = check_login($_POST['email'], $_POST['pass']);
	//echo 'Debug info:';
	//print_r($data);
	//print_r($check);
	
	if ($check) { // OK!
		//echo "Setting session info.";
		// Set the session data:
        session_start();
		$_SESSION['myId'] = $data['Id'];
		$_SESSION['fname'] = $data['Firstname'];
		$_SESSION['Enabled'] = $data['Enabled'];
		// Store the HTTP_USER_AGENT:
		$_SESSION['agent'] = md5($_SERVER['HTTP_USER_AGENT']);

		// Redirect:
		redirect_user('index.php');
			
	} else { // Unsuccessful!

		// debug
		//echo "Login failed.";
		setErrorMsg("Failed to login. Please try again or contact an admin.");
	}
		

} // End of the main submit conditional.

// Create the page:
include ('includes/templates/header.html');
include ('includes/forms/login_page.inc.php');
include ('includes/templates/footer.html');
?>


4. Create the clients page

Lets create some functionality for the client page.
What we want:   add clients
                delete clients
                update clients
                view client
                view all clients
These are the basic functionalities that we need on most of our pages.
Now, we can go about doing this in many ways, I like it sort of backwords. (for me, this makes sense... :)
1st lets create the API for these. Start with the view all clients, as this will give our main structure of the page.
We already have the design for this page from earlier development that we can use.





