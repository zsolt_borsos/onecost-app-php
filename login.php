<?php # Script 12.12 - login.php #4
// This page processes the login form submission.
// The script now stores the HTTP_USER_AGENT value for added security.

// Include the header:
$page_title = 'Login';
include ('includes/scripts/appfunctions.php');

// Check if the form has been submitted:
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		
	// Check the login:
	list ($check, $data) = check_login($_POST['email'], $_POST['pass']);
	//echo 'Debug info:';
	//print_r($data);
	//print_r($check);
	
	if ($check) { // OK!
		//echo "Setting session info.";
		// Set the session data:
        session_start();
		$_SESSION['myId'] = $data['Id'];
		$_SESSION['fname'] = $data['Firstname'];
        $_SESSION['sname'] = $data['Surname'];
		$_SESSION['Enabled'] = $data['Enabled'];
        $_SESSION['Access'] = $data['AccessLevel'];
		// Store the HTTP_USER_AGENT:
		$_SESSION['agent'] = md5($_SERVER['HTTP_USER_AGENT']);
        setMsg("Login successful. Welcome " . $data['Firstname'] . " " . $data['Surname'] ."!");
		// Redirect:
		redirect_user('index.php');
			
	} else { // Unsuccessful!

		// debug
		//echo "Login failed.";
		setErrorMsg("Failed to login. Please try again or contact an admin.");
	}
		

} // End of the main submit conditional.

// Create the page:
include ('includes/templates/header.html');
include ('includes/forms/login_page.inc.php');
include ('includes/templates/footer.html');
?>
