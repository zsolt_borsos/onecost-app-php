<?php
$page_title = "Suppliers";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}


if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //debug
    //print_r($_POST); //DEBUG INFO 
    
    if (isset($_POST["txtName"])) // Checks if the submit is from ADD NEW...
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "insert into supplier (Name, Description, Notes, Type) select :name, :desc, :notes, :type ";    
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':name', $_POST["txtName"], PDO::PARAM_STR);
        $stmt->bindParam(':desc', $_POST["txtDescription"], PDO::PARAM_STR);
        $stmt->bindParam(':notes', $_POST["txtNotes"], PDO::PARAM_STR);
        $stmt->bindParam(':type', $_POST["type"], PDO::PARAM_INT);
        try{
            $stmt->Execute();
        }catch (PDOException $e){
            setErrorMsg("Failed to add new supplier.");
            redirect_user('suppliers.php');
        }
    }
    if (isset($_POST["txtEditId"])) // Checks if the submit is from EDIT....
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "update supplier set Name = :name, Description = :desc, Notes = :notes, Type = :type where Id = :id"; 
        $stmt = $db->prepare($sql); 
        $stmt->bindParam(':name', $_POST["txtEditName"], PDO::PARAM_STR);
        $stmt->bindParam(':desc', $_POST["txtEditDescription"], PDO::PARAM_STR);
        $stmt->bindParam(':notes', $_POST["txtEditNotes"], PDO::PARAM_STR);
        $stmt->bindParam(':id', $_POST["txtEditId"], PDO::PARAM_INT);
        $stmt->bindParam(':type', $_POST["etype"], PDO::PARAM_INT);
        try{
            $stmt->Execute();
        }catch (PDOException $e){
            setErrorMsg("Failed to update supplier.");
            redirect_user('suppliers.php');
        }
    }
    if (isset($_POST["txtDeleteId"]))
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "delete from supplier where Id = :id";
        $stmt = $db->prepare($sql);  
        $stmt->bindParam(':id', $_POST["txtDeleteId"], PDO::PARAM_INT);
        try{
            $stmt->Execute();
        }catch (PDOException $e){
            setErrorMsg("Could not delete supplier. There are products linked to this supplier!");
            redirect_user('suppliers.php');
        }
    }
    
}


//include at the bottom of code so we can use PHP redirect!
include ('includes/templates/header.html');


?>
<div class="page-header" id="title"><h2>Suppliers</h2></div>

<div>
    <p>
    <input type="button" value="Add New Supplier" class="btn btn-primary" onclick="addSuppliers();">
    <input type="button" value="Toggle Search" class="btn btn-primary" onclick="toggle('divSearch');">
    <button type="button" class="btn btn-default" onclick="location.reload();">Reset Search</button>
    </p>
</div>

<div id="divSearch" style="display:none;">
    <form id="form1" action="suppliers.php" class="form-inline" method="post"> 
    <div class="form-group">
        <input type="text" name="txtSearch" id="txtSearch" placeholder="Search Name..." class="form-control"> </div>
        ||
        <div class="form-group">
        <input type ="submit" value="Search" class="btn btn-primary">
        </div> 
    </form>
</div>

<div class="table-responsive" id="divViewSuppliers">
    <p>List of Suppliers:</p>
    <table class="table table-bordered table-hover text-center">
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Notes</th>
            <th>Type</th>
            <th>Product Count</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        <?php
        
        //connect to the database
        $db = connectDb();         
        if (isset($_POST["txtSearch"])) {  $sql = "SELECT * FROM supplier where Name like '%" . $_POST["txtSearch"] . "%'"; 
        $sql = "SELECT supp.Id, supp.Name, supp.Description, supp.Notes, supp.Type , count(prod.Id) as Count "
        . "FROM supplier supp left join productsupplierlink psl on supp.Id=psl.SupplierId left join product prod on psl.ProductId=prod.Id "
        . "where Name like '%" . $_POST["txtSearch"] . "%' "
        . "group by supp.Id, supp.Name, supp.Description, supp.Notes, supp.Type"; 
        }
        else {  $sql = "SELECT supp.Id, supp.Name, supp.Description, supp.Notes, supp.Type , count(prod.Id) as Count "
        . "FROM supplier supp left join productsupplierlink psl on supp.Id=psl.SupplierId left join product prod on psl.ProductId=prod.Id "
        . "group by supp.Id, supp.Name, supp.Description, supp.Notes, supp.Type"; }
        //$sql = "SELECT * FROM client"; 
        $result = $db->query($sql);
        $db = null;  
        
        while ($row = $result->fetch())
        {
            echo '<tr>';
            //echo '<td>' .  $row['Id'] .  '</td>'; removed, no longer need Id
            echo '<td>' .  $row['Name'] .  '</td>';
            echo '<td>' .  $row['Description'] .  '</td>';
            echo '<td>' .  $row['Notes'] .  '</td>';
            //lets make this nice :) show predefined text based on int value
            echo '<td>';
            switch ($row['Type']){
                case 0 : echo 'OneCost Supplier';
                    break;
                case 1 : echo 'Client Supplier';
                    break;
                case 2 : echo 'Both';
                    break;
            }
            echo '</td>';
            echo '<td><a href=\'products.php?suppId=' .$row['Id'] . '\'>'  . $row['Count'] . '</a></td>';
            
            //added type here for js function
            echo '<td> <input type="button" value="edit" class="btn btn-info" onclick="grabForEdit(\''.$row['Id'].'\',\''.$row['Name'].'\',\''.$row['Description'].'\',\''.$row['Notes'].'\', \''.$row['Type'].'\')"></td>';
            echo '<td> <input type="button" value="delete" class="btn btn-danger" onclick="grabForDelete(\''.$row['Id'].'\',\''.$row['Name'].'\');"></td>';
            echo '</tr>';
        }
        ?>
        


    </table>

</div>

<div id="divAddNew" style="display:none;">
        
    <form id="formAddNew" action="suppliers.php" class="form-inline" method="post"> 
        <p>Add New Supplier:</p>
        <div class="form-group">
        <input type="text" name="txtName" id="txtName" placeholder="Enter Name..." class="form-control" required="required">   
        </div> 
        ||
        <div class="form-group">
        <input type="text" name="txtDescription" id="txtDescription" placeholder="Enter Description" class="form-control"> </div> 
        ||
        <div class="form-group">
        <input type="text" name="txtNotes" id="txtNotes" placeholder="Enter Notes" class="form-control"> </div>
        ||
        <div class="form-group">
            <select class="form-control" name="type" id="type">
                <option value="0" selected>OneCost Supplier</option>
                <option value="1">Client Supplier</option>
                <option value="2">Both</option>
            </select>
        </div>    
        ||
        <div class="form-group">
        <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divAddNew'); showDiv('divViewSuppliers');">
        <input type ="submit" value="Add New" class="btn btn-primary">
        </div> 
    </form> 
</div>

<div id="divEdit" style="display:none;">
    <p>Edit Supplier Info :</p>
    <form id="formEdit" action="suppliers.php" class="form-inline" method="post" role="form">  
        <p>
        <div class="form-group">
        <input type ="text" name="txtEditId" id="txtEditId" class="form-control" readonly="true"></div>
        || 
        <div class="form-group">
        <input type="text" name="txtEditName" id="txtEditName" placeholder="Enter Edited Name..." class="form-control" required="required"></div> 
        ||
        <div class="form-group">
        <input type="text" name="txtEditDescription" id="txtEditDescription" placeholder="Enter Edited Description" class="form-control"> 
        </div> 
        ||
        <div class="form-group">
        <input type="text" name="txtEditNotes" id="txtEditNotes" placeholder="Enter Edited Notes" class="form-control">
        </div>
        ||
        <div class="form-group">
            <select class="form-control" name="etype" id="etype">
                <option value="0">OneCost Supplier</option>
                <option value="1">Client Supplier</option>
                <option value="2">Both</option>
            </select>
        </div> 
        </p>
        <p>
            <div class="form-group">
            <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divEdit');">
            <input type ="submit" value="Edit Entry" class="btn btn-primary">
            </div> 
        </p>
    </form> 
</div>
<div id="divDelete" style="display:none;">
    <form id="formDelete" action="suppliers.php" class="form-inline" method="post" role="form"> 
        <p>Delete Supplier:</p>    
        <p>
            <span class="bg-danger">Warning you are about to delete this record, this cannot be un-done!</span>
            Are you sure you want to remove :
        </p>
        <p>
            <div class="form-group">
            <input type="text" name="txtDeleteName" id="txtDeleteName" readonly="true" class="form-control"> </div> 
            Id :  
            <div class="form-group">
            <input type ="text" name="txtDeleteId" id="txtDeleteId" class="form-control" readonly="true"> </div>  ?
            <div class="form-group">
            <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divDelete');">
            <input type ="submit" value="Confirm Removal" class="btn btn-danger">
            </div> 
        </p>     
    </form> 
</div>

<script src="js/jsSuppliers.js"></script> 

<?php
include ('includes/templates/footer.html');
?>
