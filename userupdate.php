<?php //this page only checks the db for current details. Form actions are handled in scripts/userupdate.php
$page_title = 'User profile';
session_start();
include ('includes/scripts/appfunctions.php');
include ('includes/templates/header.html');

// get current profile details
$result = getUser($_SESSION['myId']);
$details = $result->fetch();
//set first name
if (isset($details['Firstname'])){
	$fname = $details['Firstname'];
}else{
	$fname = "";
}
// set surname
if (isset($details['Surname'])){
	$sname = $details['Surname'];
}else{
	$sname = "";	
}
// set username

// set email
if (isset($details['Email'])){
	$email = $details['Email'];
}else{
	$email = "";	
}


// Display the form:

include ("includes/forms/userupdate.inc.php");

//add footer
include ('includes/templates/footer.html'); 

?>