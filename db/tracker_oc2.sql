-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 19, 2014 at 07:19 AM
-- Server version: 5.5.37-log
-- PHP Version: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `uktatto2_onecost`
--

--
-- Dumping data for table `tracker`
--

INSERT INTO `tracker` (`Id`, `Task`, `Assigned`, `Status`) VALUES(NULL, 'Clients', '', 2);
INSERT INTO `tracker` (`Id`, `Task`, `Assigned`, `Status`) VALUES(NULL, 'Suppliers', 'C G', 2);
INSERT INTO `tracker` (`Id`, `Task`, `Assigned`, `Status`) VALUES(NULL, 'Roles', 'Zsolt Borsos', 2);
INSERT INTO `tracker` (`Id`, `Task`, `Assigned`, `Status`) VALUES(NULL, 'Products', 'C G', 1);
INSERT INTO `tracker` (`Id`, `Task`, `Assigned`, `Status`) VALUES(NULL, 'Measures Names', 'Zsolt Borsos', 2);
INSERT INTO `tracker` (`Id`, `Task`, `Assigned`, `Status`) VALUES(NULL, 'CCs', 'Zsolt Borsos', 2);
INSERT INTO `tracker` (`Id`, `Task`, `Assigned`, `Status`) VALUES(NULL, 'Invoices', 'Zsolt Borsos', 1);
INSERT INTO `tracker` (`Id`, `Task`, `Assigned`, `Status`) VALUES(NULL, 'Category', 'Zsolt Borsos', 2);
INSERT INTO `tracker` (`Id`, `Task`, `Assigned`, `Status`) VALUES(NULL, 'Price', 'C G', 2);
INSERT INTO `tracker` (`Id`, `Task`, `Assigned`, `Status`) VALUES(NULL, 'Users', 'Zsolt Borsos', 2);
INSERT INTO `tracker` (`Id`, `Task`, `Assigned`, `Status`) VALUES(NULL, 'Assign Measures to Products', 'C G', 2);
INSERT INTO `tracker` (`Id`, `Task`, `Assigned`, `Status`) VALUES(NULL, 'Assign Roles to Users', '', 0);
INSERT INTO `tracker` (`Id`, `Task`, `Assigned`, `Status`) VALUES(NULL, 'Assign Category and Supplier to Products', 'C G', 1);
INSERT INTO `tracker` (`Id`, `Task`, `Assigned`, `Status`) VALUES(NULL, 'CC manager', 'Zsolt Borsos', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
