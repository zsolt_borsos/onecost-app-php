-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 27, 2014 at 03:08 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `onecost`
--
CREATE DATABASE IF NOT EXISTS `onecost` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `onecost`;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `ParentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cc`
--

DROP TABLE IF EXISTS `cc`;
CREATE TABLE IF NOT EXISTS `cc` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ClientId` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  `Priority` int(11) NOT NULL,
  `Notes` text NOT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedAt` timestamp NULL DEFAULT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_cc_client1_idx` (`ClientId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `Actref` varchar(30) NOT NULL,
  `Notes` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
CREATE TABLE IF NOT EXISTS `invoice` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CcId` int(11) NOT NULL,
  `ImageName` varchar(150) NOT NULL,
  `InvoiceDate` date NOT NULL,
  `Status` int(11) NOT NULL,
  `SupplierId` int(11) DEFAULT NULL,
  `Notes` text,
  PRIMARY KEY (`Id`),
  KEY `fk_invoice_cc1_idx` (`CcId`),
  KEY `fk_invoice_supplier_idx` (`SupplierId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `measure`
--

DROP TABLE IF EXISTS `measure`;
CREATE TABLE IF NOT EXISTS `measure` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductId` int(11) NOT NULL,
  `MeasureNameId` int(11) NOT NULL,
  `Value` decimal(10,2) NOT NULL,
  `ForCC` bit(1) DEFAULT b'0',
  PRIMARY KEY (`Id`),
  KEY `fk_measure_product1_idx` (`ProductId`),
  KEY `fk_measure_measurename1_idx` (`MeasureNameId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `measurename`
--

DROP TABLE IF EXISTS `measurename`;
CREATE TABLE IF NOT EXISTS `measurename` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Description` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `msgboard`
--

DROP TABLE IF EXISTS `msgboard`;
CREATE TABLE IF NOT EXISTS `msgboard` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `Message` text NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CcId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `price`
--

DROP TABLE IF EXISTS `price`;
CREATE TABLE IF NOT EXISTS `price` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductId` int(11) NOT NULL,
  `Price` decimal(10,2) NOT NULL,
  `DateFrom` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateTo` timestamp NULL DEFAULT NULL,
  `Notes` varchar(100) NOT NULL,
  `Type` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_price_product1_idx` (`ProductId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductCode` varchar(70) NOT NULL,
  `Description` varchar(100) NOT NULL,
  `Packsize` varchar(150) NOT NULL,
  `NewProduct` tinyint(4) NOT NULL DEFAULT '1',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productcategorylink`
--

DROP TABLE IF EXISTS `productcategorylink`;
CREATE TABLE IF NOT EXISTS `productcategorylink` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductId` int(11) NOT NULL,
  `CategoryId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_productcategorylink_product1_idx` (`ProductId`),
  KEY `fk_productcategorylink_category1_idx` (`CategoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productinvoicelink`
--

DROP TABLE IF EXISTS `productinvoicelink`;
CREATE TABLE IF NOT EXISTS `productinvoicelink` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `InvoiceId` int(11) NOT NULL,
  `ProductIdLeft` int(11) NOT NULL,
  `Qty` int(11) NOT NULL,
  `PriceIdLeft` int(11) NOT NULL,
  `Discount` int(11) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `fk_productinvoicelink_invoice1_idx` (`InvoiceId`),
  KEY `fk_productinvoicelink_product1_idx` (`ProductIdLeft`),
  KEY `fk_productinvoicelink_price1` (`PriceIdLeft`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productmatch`
--

DROP TABLE IF EXISTS `productmatch`;
CREATE TABLE IF NOT EXISTS `productmatch` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `InvoiceLinkId` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `PriceId` int(11) NOT NULL,
  `Discount` int(11) DEFAULT '0',
  `Final` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `fk_productmatch_product_idx` (`ProductId`),
  KEY `fk_productmatch_invoice_idx` (`InvoiceLinkId`),
  KEY `fk_productmatch_price_idx` (`PriceId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productsupplierlink`
--

DROP TABLE IF EXISTS `productsupplierlink`;
CREATE TABLE IF NOT EXISTS `productsupplierlink` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductId` int(11) NOT NULL,
  `SupplierId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_productsupplierlink_product1_idx` (`ProductId`),
  KEY `fk_productsupplierlink_supplier1_idx` (`SupplierId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `Description` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
CREATE TABLE IF NOT EXISTS `supplier` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `Description` varchar(150) NOT NULL,
  `Notes` text,
  `Type` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tracker`
--

DROP TABLE IF EXISTS `tracker`;
CREATE TABLE IF NOT EXISTS `tracker` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Task` varchar(100) NOT NULL,
  `Assigned` varchar(100) NOT NULL,
  `Status` smallint(6) NOT NULL DEFAULT '0',
  `Notes` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Passwd` varchar(40) NOT NULL,
  `Firstname` varchar(100) NOT NULL,
  `Surname` varchar(100) NOT NULL,
  `Email` varchar(150) NOT NULL,
  `Enabled` tinyint(1) NOT NULL DEFAULT '0',
  `AccessLevel` int(11) NOT NULL DEFAULT '1',
  `LastLogin` timestamp NULL DEFAULT NULL,
  `LastAction` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `usercclink`
--

DROP TABLE IF EXISTS `usercclink`;
CREATE TABLE IF NOT EXISTS `usercclink` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `CcId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_usercclink_user1_idx` (`UserId`),
  KEY `fk_usercclink_cc1_idx` (`CcId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userroleslink`
--

DROP TABLE IF EXISTS `userroleslink`;
CREATE TABLE IF NOT EXISTS `userroleslink` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `RoleId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_userroleslink_roles_idx` (`RoleId`),
  KEY `fk_userroleslink_user1_idx` (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `vwpcsaving`
--
DROP VIEW IF EXISTS `vwpcsaving`;
CREATE TABLE IF NOT EXISTS `vwpcsaving` (
`ccId` int(11)
,`InvoiceId` int(11)
,`xId` int(11)
,`InvPC` varchar(70)
,`InvDesc` varchar(100)
,`invPrice` decimal(10,2)
,`InvVal` decimal(10,2)
,`MatchPC` varchar(70)
,`MatchDesc` varchar(100)
,`MatchPrice` decimal(10,2)
,`MatchVal` decimal(10,2)
,`InvProRata` decimal(19,6)
,`MatchProRata` decimal(19,6)
,`Pc_Saving` decimal(30,10)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vwrpt_ccsummaryddl`
--
DROP VIEW IF EXISTS `vwrpt_ccsummaryddl`;
CREATE TABLE IF NOT EXISTS `vwrpt_ccsummaryddl` (
`Id` int(11)
,`display` varchar(122)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vwsavingperitem`
--
DROP VIEW IF EXISTS `vwsavingperitem`;
CREATE TABLE IF NOT EXISTS `vwsavingperitem` (
`ccId` int(11)
,`InvoiceId` int(11)
,`xId` int(11)
,`InvPC` varchar(70)
,`InvDesc` varchar(100)
,`invPrice` decimal(10,2)
,`InvVal` decimal(10,2)
,`MatchPC` varchar(70)
,`MatchDesc` varchar(100)
,`MatchPrice` decimal(10,2)
,`MatchVal` decimal(10,2)
,`InvProRata` decimal(19,6)
,`MatchProRata` decimal(19,6)
,`SavingPerItem` decimal(27,8)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vwsavingtotal`
--
DROP VIEW IF EXISTS `vwsavingtotal`;
CREATE TABLE IF NOT EXISTS `vwsavingtotal` (
`ccId` int(11)
,`InvoiceId` int(11)
,`xId` int(11)
,`InvPC` varchar(70)
,`InvDesc` varchar(100)
,`invPrice` decimal(10,2)
,`InvVal` decimal(10,2)
,`MatchPC` varchar(70)
,`MatchDesc` varchar(100)
,`MatchPrice` decimal(10,2)
,`MatchVal` decimal(10,2)
,`InvProRata` decimal(19,6)
,`MatchProRata` decimal(19,6)
,`Qty` int(11)
,`TotalSaving` decimal(37,8)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vwsummary`
--
DROP VIEW IF EXISTS `vwsummary`;
CREATE TABLE IF NOT EXISTS `vwsummary` (
`ccId` int(11)
,`InvoiceId` int(11)
,`xId` int(11)
,`InvPC` varchar(70)
,`InvDesc` varchar(100)
,`InvPS` varchar(150)
,`invPrice` decimal(10,2)
,`Qty` int(11)
,`MatchPC` varchar(70)
,`MatchDesc` varchar(100)
,`matchPS` varchar(150)
,`MatchPrice` decimal(10,2)
);
-- --------------------------------------------------------

--
-- Structure for view `vwpcsaving`
--
DROP TABLE IF EXISTS `vwpcsaving`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vwpcsaving` AS select `cc`.`Id` AS `ccId`,`pil`.`InvoiceId` AS `InvoiceId`,`pil`.`Id` AS `xId`,`invprod`.`ProductCode` AS `InvPC`,`invprod`.`Description` AS `InvDesc`,`invprice`.`Price` AS `invPrice`,`invmeas`.`Value` AS `InvVal`,`matchprod`.`ProductCode` AS `MatchPC`,`matchprod`.`Description` AS `MatchDesc`,`matchprice`.`Price` AS `MatchPrice`,`matchmeas`.`Value` AS `MatchVal`,((`invprice`.`Price` / `invmeas`.`Value`) * 100) AS `InvProRata`,((`matchprice`.`Price` / `matchmeas`.`Value`) * 100) AS `MatchProRata`,((1 - ((`matchprice`.`Price` / `matchmeas`.`Value`) / (`invprice`.`Price` / `invmeas`.`Value`))) * 100) AS `Pc_Saving` from (((((((((`productinvoicelink` `pil` join `product` `invprod` on((`pil`.`ProductIdLeft` = `invprod`.`Id`))) join `price` `invprice` on((`pil`.`PriceIdLeft` = `invprice`.`Id`))) join `productmatch` `matchx` on((`pil`.`Id` = `matchx`.`InvoiceLinkId`))) join `product` `matchprod` on((`matchx`.`ProductId` = `matchprod`.`Id`))) join `price` `matchprice` on((`matchx`.`PriceId` = `matchprice`.`Id`))) join `measure` `invmeas` on((`invprod`.`Id` = `invmeas`.`ProductId`))) join `measure` `matchmeas` on((`matchprod`.`Id` = `matchmeas`.`ProductId`))) join `invoice` on((`pil`.`InvoiceId` = `invoice`.`Id`))) join `cc` on((`invoice`.`CcId` = `cc`.`Id`))) where ((`matchx`.`Final` = 1) and (`invmeas`.`ForCC` = 1) and (`matchmeas`.`ForCC` = 1) and (`invmeas`.`MeasureNameId` = `matchmeas`.`MeasureNameId`)) order by ((1 - ((`matchprice`.`Price` / `matchmeas`.`Value`) / (`invprice`.`Price` / `invmeas`.`Value`))) * 100) desc;

-- --------------------------------------------------------

--
-- Structure for view `vwrpt_ccsummaryddl`
--
DROP TABLE IF EXISTS `vwrpt_ccsummaryddl`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vwrpt_ccsummaryddl` AS select `cc`.`Id` AS `Id`,concat(`client`.`Name`,' : ',`cc`.`CreatedAt`) AS `display` from (`cc` join `client` on((`cc`.`ClientId` = `client`.`Id`)));

-- --------------------------------------------------------

--
-- Structure for view `vwsavingperitem`
--
DROP TABLE IF EXISTS `vwsavingperitem`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vwsavingperitem` AS select `cc`.`Id` AS `ccId`,`pil`.`InvoiceId` AS `InvoiceId`,`pil`.`Id` AS `xId`,`invprod`.`ProductCode` AS `InvPC`,`invprod`.`Description` AS `InvDesc`,`invprice`.`Price` AS `invPrice`,`invmeas`.`Value` AS `InvVal`,`matchprod`.`ProductCode` AS `MatchPC`,`matchprod`.`Description` AS `MatchDesc`,`matchprice`.`Price` AS `MatchPrice`,`matchmeas`.`Value` AS `MatchVal`,((`invprice`.`Price` / `invmeas`.`Value`) * 100) AS `InvProRata`,((`matchprice`.`Price` / `matchmeas`.`Value`) * 100) AS `MatchProRata`,(`invprice`.`Price` - ((`matchprice`.`Price` / `matchmeas`.`Value`) * `invmeas`.`Value`)) AS `SavingPerItem` from (((((((((`productinvoicelink` `pil` join `product` `invprod` on((`pil`.`ProductIdLeft` = `invprod`.`Id`))) join `price` `invprice` on((`pil`.`PriceIdLeft` = `invprice`.`Id`))) join `productmatch` `matchx` on((`pil`.`Id` = `matchx`.`InvoiceLinkId`))) join `product` `matchprod` on((`matchx`.`ProductId` = `matchprod`.`Id`))) join `price` `matchprice` on((`matchx`.`PriceId` = `matchprice`.`Id`))) join `measure` `invmeas` on((`invprod`.`Id` = `invmeas`.`ProductId`))) join `measure` `matchmeas` on((`matchprod`.`Id` = `matchmeas`.`ProductId`))) join `invoice` on((`pil`.`InvoiceId` = `invoice`.`Id`))) join `cc` on((`invoice`.`CcId` = `cc`.`Id`))) where ((`matchx`.`Final` = 1) and (`invmeas`.`ForCC` = 1) and (`matchmeas`.`ForCC` = 1) and (`invmeas`.`MeasureNameId` = `matchmeas`.`MeasureNameId`)) order by (`invprice`.`Price` - ((`matchprice`.`Price` / `matchmeas`.`Value`) * `invmeas`.`Value`)) desc;

-- --------------------------------------------------------

--
-- Structure for view `vwsavingtotal`
--
DROP TABLE IF EXISTS `vwsavingtotal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vwsavingtotal` AS select `cc`.`Id` AS `ccId`,`pil`.`InvoiceId` AS `InvoiceId`,`pil`.`Id` AS `xId`,`invprod`.`ProductCode` AS `InvPC`,`invprod`.`Description` AS `InvDesc`,`invprice`.`Price` AS `invPrice`,`invmeas`.`Value` AS `InvVal`,`matchprod`.`ProductCode` AS `MatchPC`,`matchprod`.`Description` AS `MatchDesc`,`matchprice`.`Price` AS `MatchPrice`,`matchmeas`.`Value` AS `MatchVal`,((`invprice`.`Price` / `invmeas`.`Value`) * 100) AS `InvProRata`,((`matchprice`.`Price` / `matchmeas`.`Value`) * 100) AS `MatchProRata`,`pil`.`Qty` AS `Qty`,((`invprice`.`Price` - ((`matchprice`.`Price` / `matchmeas`.`Value`) * `invmeas`.`Value`)) * `pil`.`Qty`) AS `TotalSaving` from (((((((((`productinvoicelink` `pil` join `product` `invprod` on((`pil`.`ProductIdLeft` = `invprod`.`Id`))) join `price` `invprice` on((`pil`.`PriceIdLeft` = `invprice`.`Id`))) join `productmatch` `matchx` on((`pil`.`Id` = `matchx`.`InvoiceLinkId`))) join `product` `matchprod` on((`matchx`.`ProductId` = `matchprod`.`Id`))) join `price` `matchprice` on((`matchx`.`PriceId` = `matchprice`.`Id`))) join `measure` `invmeas` on((`invprod`.`Id` = `invmeas`.`ProductId`))) join `measure` `matchmeas` on((`matchprod`.`Id` = `matchmeas`.`ProductId`))) join `invoice` on((`pil`.`InvoiceId` = `invoice`.`Id`))) join `cc` on((`invoice`.`CcId` = `cc`.`Id`))) where ((`matchx`.`Final` = 1) and (`invmeas`.`ForCC` = 1) and (`matchmeas`.`ForCC` = 1) and (`invmeas`.`MeasureNameId` = `matchmeas`.`MeasureNameId`)) order by ((`invprice`.`Price` - ((`matchprice`.`Price` / `matchmeas`.`Value`) * `invmeas`.`Value`)) * `pil`.`Qty`) desc;

-- --------------------------------------------------------

--
-- Structure for view `vwsummary`
--
DROP TABLE IF EXISTS `vwsummary`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vwsummary` AS select `cc`.`Id` AS `ccId`,`pil`.`InvoiceId` AS `InvoiceId`,`pil`.`Id` AS `xId`,`invprod`.`ProductCode` AS `InvPC`,`invprod`.`Description` AS `InvDesc`,`invprod`.`Packsize` AS `InvPS`,`invprice`.`Price` AS `invPrice`,`pil`.`Qty` AS `Qty`,`matchprod`.`ProductCode` AS `MatchPC`,`matchprod`.`Description` AS `MatchDesc`,`matchprod`.`Packsize` AS `matchPS`,`matchprice`.`Price` AS `MatchPrice` from (((((((((`productinvoicelink` `pil` join `product` `invprod` on((`pil`.`ProductIdLeft` = `invprod`.`Id`))) join `price` `invprice` on((`pil`.`PriceIdLeft` = `invprice`.`Id`))) join `productmatch` `matchx` on((`pil`.`Id` = `matchx`.`InvoiceLinkId`))) join `product` `matchprod` on((`matchx`.`ProductId` = `matchprod`.`Id`))) join `price` `matchprice` on((`matchx`.`PriceId` = `matchprice`.`Id`))) join `measure` `invmeas` on((`invprod`.`Id` = `invmeas`.`ProductId`))) join `measure` `matchmeas` on((`matchprod`.`Id` = `matchmeas`.`ProductId`))) join `invoice` on((`pil`.`InvoiceId` = `invoice`.`Id`))) join `cc` on((`invoice`.`CcId` = `cc`.`Id`))) where ((`matchx`.`Final` = 1) and (`invmeas`.`ForCC` = 1) and (`matchmeas`.`ForCC` = 1) and (`invmeas`.`MeasureNameId` = `matchmeas`.`MeasureNameId`)) order by `pil`.`Id`;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cc`
--
ALTER TABLE `cc`
  ADD CONSTRAINT `fk_cc_client1` FOREIGN KEY (`ClientId`) REFERENCES `client` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `fk_invoice_cc1` FOREIGN KEY (`CcId`) REFERENCES `cc` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_invoice_supplier` FOREIGN KEY (`SupplierId`) REFERENCES `supplier` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `measure`
--
ALTER TABLE `measure`
  ADD CONSTRAINT `fk_measure_measurename1` FOREIGN KEY (`MeasureNameId`) REFERENCES `measurename` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_measure_product1` FOREIGN KEY (`ProductId`) REFERENCES `product` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `price`
--
ALTER TABLE `price`
  ADD CONSTRAINT `fk_price_product1` FOREIGN KEY (`ProductId`) REFERENCES `product` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `productcategorylink`
--
ALTER TABLE `productcategorylink`
  ADD CONSTRAINT `fk_productcategorylink_category1` FOREIGN KEY (`CategoryId`) REFERENCES `category` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_productcategorylink_product1` FOREIGN KEY (`ProductId`) REFERENCES `product` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `productinvoicelink`
--
ALTER TABLE `productinvoicelink`
  ADD CONSTRAINT `fk_productinvoicelink_invoice1` FOREIGN KEY (`InvoiceId`) REFERENCES `invoice` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_productinvoicelink_price1` FOREIGN KEY (`PriceIdLeft`) REFERENCES `price` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_productinvoicelink_product1` FOREIGN KEY (`ProductIdLeft`) REFERENCES `product` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `productmatch`
--
ALTER TABLE `productmatch`
  ADD CONSTRAINT `fk_productmatch_invoice` FOREIGN KEY (`InvoiceLinkId`) REFERENCES `productinvoicelink` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_productmatch_price` FOREIGN KEY (`PriceId`) REFERENCES `price` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_productmatch_product` FOREIGN KEY (`ProductId`) REFERENCES `product` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `productsupplierlink`
--
ALTER TABLE `productsupplierlink`
  ADD CONSTRAINT `fk_productsupplierlink_product1` FOREIGN KEY (`ProductId`) REFERENCES `product` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_productsupplierlink_supplier1` FOREIGN KEY (`SupplierId`) REFERENCES `supplier` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `usercclink`
--
ALTER TABLE `usercclink`
  ADD CONSTRAINT `fk_usercclink_cc1` FOREIGN KEY (`CcId`) REFERENCES `cc` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_usercclink_user1` FOREIGN KEY (`UserId`) REFERENCES `user` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `userroleslink`
--
ALTER TABLE `userroleslink`
  ADD CONSTRAINT `fk_userroleslink_roles` FOREIGN KEY (`RoleId`) REFERENCES `roles` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_userroleslink_user1` FOREIGN KEY (`UserId`) REFERENCES `user` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
