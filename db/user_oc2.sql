-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 19, 2014 at 07:15 AM
-- Server version: 5.5.37-log
-- PHP Version: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `uktatto2_onecost`
--

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`Id`, `Passwd`, `Firstname`, `Surname`, `Email`, `Enabled`, `AccessLevel`) VALUES(NULL, 'b78b647728101ba462182b4c7e5b2ca57b9f5a99', 'Zsolt', 'Borsos', 'thiniquelle@gmail.com', 1, 2);
INSERT INTO `user` (`Id`, `Passwd`, `Firstname`, `Surname`, `Email`, `Enabled`, `AccessLevel`) VALUES(NULL, 'b75aecaa2330b0080b39c75af85259b8fec11659', 'C', 'G', 'c@g.com', 1, 2);
INSERT INTO `user` (`Id`, `Passwd`, `Firstname`, `Surname`, `Email`, `Enabled`, `AccessLevel`) VALUES(NULL, 'a80777f84ad6f072acdb5cf91163d7f4e0dcc2b0', 'Joanne', 'Hill', 'joanne@onecost.co.uk', 1, 1);
INSERT INTO `user` (`Id`, `Passwd`, `Firstname`, `Surname`, `Email`, `Enabled`, `AccessLevel`) VALUES(NULL, 'b9e3fbe5e86589c79ed90890a2b862256017676a', 'Julie', 'Gibson', 'juliegibson@onecost.co.uk', 1, 1);
INSERT INTO `user` (`Id`, `Passwd`, `Firstname`, `Surname`, `Email`, `Enabled`, `AccessLevel`) VALUES(NULL, '3c8643c0bd2d41ba3e4353b0d18dbed734451905', 'David', 'Gannon', 'david@onecost.co.uk', 1, 1);
INSERT INTO `user` (`Id`, `Passwd`, `Firstname`, `Surname`, `Email`, `Enabled`, `AccessLevel`) VALUES(NULL, 'dd4443da24b091c0bba2179170061f656b4c79d2', 'Andrew', 'Thompson', 'Andrew@onecost.co.uk', 1, 1);
INSERT INTO `user` (`Id`, `Passwd`, `Firstname`, `Surname`, `Email`, `Enabled`, `AccessLevel`) VALUES(NULL, '7c257c6a8cd537400e7297bcb92b9a1ef6e30d00', 'Stuart', 'Ellis', 'stuartellis@onecost.co.uk', 0, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
