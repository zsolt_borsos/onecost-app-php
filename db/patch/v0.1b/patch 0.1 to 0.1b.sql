--
-- Changing constraints on measure table.
--

ALTER TABLE `measure` DROP FOREIGN KEY `fk_measure_measurename1`;
ALTER TABLE `measure` DROP FOREIGN KEY `fk_measure_product1`;

ALTER TABLE `measure`
  ADD CONSTRAINT `fk_measure_measurename1` FOREIGN KEY (`MeasureNameId`) REFERENCES `measurename` (`Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_measure_product1` FOREIGN KEY (`ProductId`) REFERENCES `product` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;