<?php
$page_title = "Manage Product";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}
include ('includes/templates/header.html');


if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
   // print_r($_POST); //DEBUG INFO 
    
    if (isset($_POST["newSupId"])) 
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "insert into productsupplierlink (ProductId, SupplierId) select {$_GET["pid"]}, :suppId ";    
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':suppId', $_POST["newSupId"], PDO::PARAM_STR);
        $stmt->Execute(); 
    } 
    if (isset($_POST["newCatId"])) 
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "insert into productcategorylink (ProductId, CategoryId) select {$_GET["pid"]}, :newCatId ";    
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':newCatId', $_POST["newCatId"], PDO::PARAM_STR);
        $stmt->Execute(); 
    } 
    if (isset($_POST["txtDeleteId"])) 
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "delete from productsupplierlink where Id = :id ";    
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':id', $_POST["txtDeleteId"], PDO::PARAM_STR);
        $stmt->Execute(); 
    } 
    if (isset($_POST["txtDeleteId_Cat"])) 
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "delete from productcategorylink where Id = :id ";    
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':id', $_POST["txtDeleteId_Cat"], PDO::PARAM_STR);
        $stmt->Execute(); 
    } 
}

?>


<div id="page-header"><h2>Manage Product ::</h2>
    <p><button type="button" id="backBtn" class="btn btn-default" onclick="goBack();">Back</button></p>

    <h4>Product management page for product:<span class="text-primary">
    <?php 
            if (isset($_GET["pid"]))
            {
                $id = $_GET["pid"]; 
                $db = connectDb(); 
                $sql = "SELECT Description, Packsize from product where  Id = {$id}";
                $result = $db->query($sql);
                $row = $result->fetch();
                echo $row[0] . " " . $row[1];
            }
            ?> 
    </span></h4>
    
</div> 

<div class="row">
    <div class="col-md-6">
        <h4>List of Suppliers allocated to this product
            (
            <?php
            if (isset($_GET["pid"]))
            {
                $id = $_GET["pid"]; 
                $db = connectDb(); 
                $sql = "SELECT count(*) from productsupplierlink where ProductId = {$id}";
                $result = $db->query($sql);
                $row = $result->fetch();
                echo $row[0];
            }
            ?>
            )
            :</h4>
        <table style="width:100%;">
             <?php 
             if (isset($_GET["pid"])){ // only attempt to load page if has pid in querystring
                 $id = $_GET["pid"]; 
                 $db = connectDb(); 
                 $sql = "select psl.Id as DeleteId, Name from productsupplierlink as psl inner join supplier on psl.SupplierId = supplier.Id where psl.ProductId = {$id} ";
                 $result = $db->query($sql);
                 $db = null;   
                 while ($row = $result->fetch())
                 {
                     echo '<tr>';
                     echo '<td>' . $row['Name'] . '</td>';
                     echo '<td> <input type="button" value="delete" class="btn btn-danger" onclick="grabForDelete_Sup(\''.$row['DeleteId'].'\',\''.$row['Name'].'\');" /></td>';
                 }  
             }
             ?> 
        </table> 
        <p>
            <?php
            if (isset($_GET["pid"]))
            {
                $id = $_GET["pid"]; 
                $db = connectDb(); 
                $sql = "SELECT count(*) from productsupplierlink where ProductId = {$id}";
                $result = $db->query($sql);
                $row = $result->fetch(); 
                if ($row[0] == '0')
                    echo '<input type="button" value="Allocate New" class="btn btn-primary" onclick="addNewSupplier();" />';
                else
                    echo '<input type="button" value="Allocate New" class="btn btn-primary" onclick="addNewSupplier();" disabled />';
            }
            ?>
            
        </p>
    </div>


    <div class="col-md-6">
        <h4>List of Categories allocated to this product
            (
            <?php
            if (isset($_GET["pid"]))
            {
                $id = $_GET["pid"]; 
                $db = connectDb(); 
                $sql = "SELECT count(*) from productcategorylink where ProductId = {$id}";
                $result = $db->query($sql);
                $row = $result->fetch();
                echo $row[0];
            }
            ?>
            )
            :</h4>
        <table style="width:100%;">
             <?php 
             if (isset($_GET["pid"])){ // only attempt to load page if has pid in querystring
                 $id = $_GET["pid"]; 
                 $db = connectDb(); 
                 $sql = "select pcl.Id as DeleteId, Name from productcategorylink as pcl inner join category on pcl.CategoryId = category.Id where pcl.ProductId = {$id} ";
                 $result = $db->query($sql);
                 $db = null;   
                 while ($row = $result->fetch())
                 {
                     echo '<tr>';
                     echo '<td>' . $row['Name'] . '</td>';
                     echo '<td> <input type="button" value="delete" class="btn btn-danger" onclick="grabForDelete_Cat(\''.$row['DeleteId'].'\',\''.$row['Name'].'\');" /></td>';
                 }  
             }
             ?> 
        </table> 
        <p><input type="button" value="Allocate New" class="btn btn-primary" onclick="addNewCategory();" /></p>
    </div>
</div>
 
<div id="divDeleteSupplier" style="display:none;">
    <form id="formDelSup" action="manageproduct.php?pid=<?php echo $_GET["pid"]; ?>" class="form-inline" method="post"> 
    <p>Remove this supplier link:</p>
    <p><span class="bg-danger">Warning you are about to delete this record, this cannot be un-done!</span>
        Are you sure you want to remove :</p>
    
        <div class="form-group">ID:
        <input type="text" name="txtDeleteId" id="txtDeleteId_Sup" readonly="true" class="form-control"/>   
        </div> 
        || 
        <div class="form-group">Supplier:
        <input type="text" name="txtDeleteSupplier" id="txtDeleteSupplier" readonly="true" class="form-control" />
        </div> ?
        <p>
        <div class="form-group">
        <input type="button" value="Cancel" class="btn btn-default" onclick="location.reload();">
        <input type ="submit" value="Confirm Removal" class="btn btn-danger" />
        </div> 
        </p>

    </form>
</div>

<div id="divDeleteCategory" style="display:none;">
    <form id="formDelCat" action="manageproduct.php?pid=<?php echo $_GET["pid"]; ?>" class="form-inline" method="post"> 
    <p>Remove this category link:</p>
    <p><span class="bg-danger">Warning you are about to delete this record, this cannot be un-done!</span>
        Are you sure you want to remove :</p>
    
        <div class="form-group">ID:
        <input type="text" name="txtDeleteId_Cat" id="txtDeleteId_Cat" readonly="true" class="form-control"/>   
        </div> 
        || 
        <div class="form-group">Category:
        <input type="text" name="txtDeleteCategory" id="txtDeleteCategory" readonly="true" class="form-control" />
        </div> ?
        <p>
        <div class="form-group">
        <input type="button" value="Cancel" class="btn btn-default" onclick="location.reload();">
        <input type ="submit" value="Confirm Removal" class="btn btn-danger" />
        </div> 
        </p>

    </form>
</div>

<div id="divAddSup" style="display:none;">
    <form id="formAddSup" action="manageproduct.php?pid=<?php echo $_GET["pid"]; ?>" class="form-inline" method="post"> 
        <p>Add New Supplier Link:</p>
        Select Supplier from list :
        <div class="form-group">
            <?php
                //details: [sid] = id/name, [table] = table name, [col] = column to show, [valCol] = columns for Value,
                $details = array();
                $details['sid'] = "newSupId";
                $details['table'] = "supplier";
                $details['col'] = "Name";
                $details['valCol'] = "Id";
                createSelect($details);
            ?>
        </div>  
        <p>
        <div class="form-group">
        <input type="button" value="Cancel" class="btn btn-default" onclick="location.reload();"> 
        <input type ="submit" value="Select Supplier" class="btn btn-primary" />
        </div> 
        </p>
    </form>
</div>

<div id="divAddCat" style="display:none;">
    <form id="formAddCat" action="manageproduct.php?pid=<?php echo $_GET["pid"]; ?>" class="form-inline" method="post"> 
        <p>Add New Category Link:</p>
        Select Category from list :
        <div class="form-group">
            <?php
                //details: [sid] = id/name, [table] = table name, [col] = column to show, [valCol] = columns for Value,
                $details = array();
                $details['sid'] = "newCatId";
                $details['table'] = "category";
                $details['col'] = "Name";
                $details['valCol'] = "Id";
                createSelect($details);
            ?>
        </div>  
        <p>
        <div class="form-group">
        <input type="button" value="Cancel" class="btn btn-default" onclick="location.reload();">
        <input type ="submit" value="Select Category" class="btn btn-primary" />
        </div> 
        </p>
    </form>
</div>

<script src="js/jsManageProduct.js"></script>


<?php
include ('includes/templates/footer.html');
?>
