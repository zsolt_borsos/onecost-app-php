<?php
$page_title = "Dashboard";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

include ('includes/templates/header.html');
?>


<div class="row">
    <div class="page-header">
        <h2>Dashboard</h2>
    </div>

</div>

<!-- Widgets -->

<div class="row">
        
    <!-- Chat -->
    <div class="col-md-4">
        <div class="row">
            <div class="page-header">
                <h4>Info board</h4>
            </div>
        </div>
        <?php include('includes/forms/messageboard.php'); ?>
       
    </div>


    <!-- Stats -->
    <div class="col-md-offset-1 col-md-2">
        <?php include('includes/forms/appstats.php'); ?>
    </div>
    
    <!-- Stats -->
    <div class="col-md-offset-1 col-md-4">
        <?php include('includes/forms/latestactivity.php'); ?>
    </div>

</div>

 <script type="text/javascript" src="js/dashmsgboard.js"></script>

<?php

include ('includes/templates/footer.html');
?>