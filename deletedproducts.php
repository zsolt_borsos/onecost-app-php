<?php
$page_title = "Deleted Products";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

if ($_SESSION['Access'] < 2){
    setErrorMsg("You have no permission to see that page.");
    redirect_user('index.php');
}

include ('includes/templates/header.html');

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if (isset($_POST["txtRestoreId"]))
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "update product set Deleted=0 where Id = :id";
        $stmt = $db->prepare($sql);  
        $stmt->bindParam(':id', $_POST["txtRestoreId"], PDO::PARAM_INT);
        $stmt->Execute();
    } 
}
?>

<div id="page-header"><h2>Deleted Products ::</h2></div>

<div class="table-responsive" id="divViewProducts">
    <p>All Deleted Products listed below (Click price to see price history):</p>
    <table class="table table-bordered table-hover text-center">
        <tr>
            <th>ID</th>
            <th>Product Code</th>
            <th>Description</th>
            <th>Packsize</th>
            <th>Price</th>
            <th>Supplier</th>
            <th>Restore</th>
        </tr>
        <?php
        
        //connect to the database
        $db = connectDb(); 
        $sql = "SELECT product.Id as ProdId, product.ProductCode, product.Description, product.Packsize, price.Price, supplier.Name
        FROM product 
        inner join price on product.Id = price.ProductId 
        left join productsupplierlink as psl on product.Id=psl.ProductId 
        left join supplier on psl.SupplierId = supplier.Id 
        where Deleted=1 and DateTo is null order by 1 ";
        $result = $db->query($sql);
        $db = null;  
        
        while ($row = $result->fetch())
        { 
            echo '<tr>';
            echo '<td>' .  $row['ProdId'] .  '</td>';
            echo '<td>' .  $row['ProductCode'] .  '</td>';
            echo '<td>' .  $row['Description'] .  '</td>';
            echo '<td><a href="productmeasures.php?prodid=' . $row['ProdId'] . '">' .  htmlspecialchars($row['Packsize']) .  '</a></td>';
            echo '<td><a href="pricehistory.php?prodid=' . $row['ProdId'] . '">' .  htmlspecialchars($row['Price']) .  '</a></td>';
            echo '<td>' . $row['Name'] . '</td>';
            echo '<td> <input type="button" value="restore" class="btn btn-danger" onclick="grabForDelete(\''.$row['ProdId'].'\',\''.htmlspecialchars($row['ProductCode']).'\');" /></td>';
            echo '</tr>';
        }
        /*
        print_r($result); echo '<br />';
        
        print_r( $result->fetch());
          */
        ?>    
    </table>
</div>

<div id="divDelete" style="display:none;">
    <p>Restore Product :</p>
    <form id="formDelete" action="deletedproducts.php" class="form-inline" method="post"> 
        <p><span class="bg-primary">This action will restore this product and make available for future use</span>
        Are you sure you want to restore this product :</p>
        <p><div class="form-group">
        <input type="text" name="txtDeleteName" id="txtDeleteName" placeholder="" readonly="true" class="form-control" /> </div> 
        || Id :  
        <div class="form-group">
        <input type ="text" name="txtRestoreId" id="txtDeleteId" class="form-control" readonly="true" /> </div>  ?
        </p>
        <p>
        <div class="form-group">
        <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divDelete');">
        <input type ="submit" value="Confirm Restore" class="btn btn-primary" />
        </div> 
        </p>     
    </form> 
</div>

<script src="js/jsProducts.js"></script>


<?php
include ('includes/templates/footer.html');
?>
