<?php
$page_title = "CC Manager";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

//check for POST and use search box to filter results
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	//check for ccid
    if (isset($_GET['ccId'])){
        $ccId = $_GET['ccId']; 
    }else{
        redirect_user('cc.php');
    }

}


// Check if the form has been submitted:
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $error = array();
    //print_r($_POST);
    //print_r($_FILES);
    //handle file uppload 1s    
	// Check for an uploaded file:
    if (isset($_FILES['upload'])) {
		
        // Validate the type. Should be JPEG or PNG.
		$allowed = array ('image/pjpeg', 'image/jpeg', 'image/JPG', 'image/X-PNG', 'image/PNG', 'image/png', 'image/x-png');
		if (in_array($_FILES['upload']['type'], $allowed)) {
            
            //check if file already exists
            
            $fileName = "images/invoices/{$_POST['ccId']}/{$_FILES['upload']['name']}";
            if (file_exists($fileName)){
                setErrorMsg('This image is already in this CC!');
                redirect_user('ccmanager.php?ccId=' . $_POST['ccId']);
            }
            
            // Move the file over.
			if (move_uploaded_file ($_FILES['upload']['tmp_name'], $fileName)) {
				//echo '<p><em>The file has been uploaded!</em></p>';
                $msg = "Done";
			} // End of move... IF.
			
		}else{ // Invalid type.
			//echo '<p class="error">Please upload a JPEG or PNG image.</p>';
            $error[] = "Wrong file type.";
		}

	
	// Check for an error:
	if ($_FILES['upload']['error'] > 0) {
        
		// Print a message based upon the error.
		switch ($_FILES['upload']['error']) {
			case 1:
				$error[] = 'The file exceeds the upload_max_filesize setting in php.ini.';
				break;
			case 2:
				$error[] = 'The file exceeds the MAX_FILE_SIZE setting in the HTML form.';
				break;
			case 3:
				$error[] = 'The file was only partially uploaded.';
				break;
			case 4:
				$error[] = 'No file was uploaded.';
				break;
			case 6:
				$error[] = 'No temporary folder was available.';
				break;
			case 7:
				$error[] = 'Unable to write to the disk.';
				break;
			case 8:
				$error[] = 'File upload stopped.';
				break;
			default:
				$error[] = 'A system error occurred.';
				break;
		} // End of switch.
		
	} // End of error IF.
	
    if (!empty($error)){
        foreach($error as $r){
            $err .= $r;
        }
        setErrorMsg($err);
        redirect_user('ccmanager.php?ccId=' . $_POST['ccId']);
    }
	// Delete the file if it still exists:
	if (file_exists ($_FILES['upload']['tmp_name']) && is_file($_FILES['upload']['tmp_name']) ) {
		unlink ($_FILES['upload']['tmp_name']);
	}

    // end of file upload handle
 
    //check posted data
    //create details array
    $details = array();
    //add img name
    $details['img'] = $_FILES['upload']['name'];
    //add ccId
    $details['ccId'] = $_POST['ccId'];
    //if no errors, call the addInvoice function with the details
    if (empty($error)){
        if (addInvoice($details)){
            setMsg("Invoice added.");   
        }else{
            setErrorMsg("Failed to create invoice.");   
        }
    }else{
        setErrorMsg("Something went wrong... Details missing!");   
    }
    //print_r($error);
    
    redirect_user('ccmanager.php?ccId=' . $_POST['ccId']);
} // End of isset($_FILES['upload']) IF.

if (isset($_POST['did'])){
    if (deleteInvoice($_POST['did'])){
        setMsg('Invoice deleted.');
    }else{
        setErrorMsg('Could not delete Invoice.');   
    }
     redirect_user('ccmanager.php?ccId=' . $_POST['ccId']);
}

} // End of the submitted conditional.

//including the header at the bottom so redirect will work above
include ('includes/templates/header.html');

?>

<!-- Modal for: select final match -->
<div class="modal fade" id="finalMatchModal" tabindex="-1" role="dialog" aria-labelledby="finalMatchModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="finalMatchModalLabel">Select Final Match</h4>
        </div>
        <div class="modal-body">
            <form id="finalMatch" name="finalMatch" action="includes/scripts/matcher.php" method="post"> 
                
                <!--  let JS roll -->
                    
            </form>    
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" form="finalMatch"  class="btn btn-primary">Save</button>
        </div>
    </div>
  </div>
</div>



<div class="row">
<a href="cc.php" class="btn btn-default">Back to CC Overview</a>   
<div class="page-header">
    <h2>Manage CC</h2>
</div>
<div class="row">
    <div id="status" class="col-md-4">
        <div class="row">
            <div class="page-header">
                <h4>CC Info Log</h4>
            </div>
        </div>
        <?php include('includes/forms/messageboard.php') ?>    
    
    </div>
    
    <div id="status" class="col-md-offset-1 col-md-3">
        <?php include('includes/forms/ccactivity.php') ?>    
    
    </div>
    <div id="status" class="col-md-offset-1 col-md-3">
        <?php include('includes/forms/ccstats.php') ?>  
    </div>
</div>

  <div class="col-md-6" id="invoiceDetails">
      
	<form class="hidden" id="manageInvoiceForm" name="manageInvoiceForm" role="form" action="ccinvoice.php" method="GET">
        <input type="hidden" id="invId" name="invId">
        <input type="hidden" id="ccId" name="ccId" value="<?php echo $ccId; ?>">
    </form>
    
		<div class="page-header">
            <h3>Invoices</h3>
        </div>
        
        <p class="pull-right">
            <button type="button" id="manageInvoices" class="btn btn-primary btnAdd">Add Invoice</button>  
        </p>
        <div class="clearfix"></div>
		<div class="table-responsive" id="myTable">
			<table class="table table-bordered table-hover text-center">
                <tr>
            <!--    <th scope="col">Id</th> -->
                    <th scope="col">Image Name</th>
                    <th scope="col">Supplier</th>
                    <th scope="col">Invoice Date</th>
                    <th scope="col">Status</th>
                    <th scope="col">Delete</th>
                    <th scope="col">Manage</th>
                </tr>             
               
                    <?php
                    if (isset($ccId)){
                        //echo $ccId;
                        $result = getAllInvoiceWithSupplier($ccId);
                        while($row = $result->fetch(PDO::FETCH_ASSOC)){
                            //print_r($row);
                            echo '<tr>';
                            //echo '<td>' . $row['Id'] . '</td>';
                            echo '<td>' . $row['ImageName'] . '</td>';
                            echo '<td>' . $row['Name'] . '</td>';
                            echo '<td>' . $row['InvoiceDate'] . '</td>';
                            //use glyphs to show status
                            echo '<td>';
                            if ($row['Status'] == 0){
                                echo '<span class="glyphicon glyphicon-remove"></span>';
                            }else{
                                echo '<span class="glyphicon glyphicon-ok"></span>';
                            }
                            echo '</td>';
                            
                            echo '<td> <input type="button" value="Delete" class="btn btn-danger"              onclick="grabForDelete(\''.$row['Id'].'\',\''.$row['ImageName'].'\');"></td>';
                            echo '<td> <button type="submit" value="'. $row['Id'] .'" class="btn btn-primary manageInvoiceBtn" for="manageInvoiceForm">Manage</button> </td>';
                        }
                    }
                    ?>
                
            </table>
        </div>
      
<div id="addForm" class="addForm" style="display:none;">
    <form role="form" action="ccmanager.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="ccId" id="ccId" value="<?php echo $ccId; ?>">
        <div class="form-group">
            <label for="upload">Upload Invoice Image</label>
            <input type="file" name="upload" id="upload" class="form-control" required>
        </div>
    <div class="form-group pull-right">
        <button type="button" class="btn btn-default" onclick="hideDiv('addForm'); showDiv('myTable');">Cancel</button>
        <button type="submit" class="btn btn-primary">Upload Image</button>
    </div>
    </form>
</div>

<div id="divDelete" style="display:none;">
    <form id="formDelete" action="ccmanager.php" method="post" role="form"> 
        <p>Delete Invoice:</p>    
        <p>
            <span class="bg-danger">Warning you are about to delete this record, this cannot be un-done!</span>
        </p>
            <input type="hidden" name="ccId" id="ccId" value="<?php echo $ccId; ?>">
            <div class="form-group">
                <label for="dname">Are you sure you want to remove :</label>
                <input type="text" name="dname" id="dname" readonly="true" class="form-control"> </div> 
              
            <div class="form-group">
                <label for="did">Id</label>
                <input type ="text" name="did" id="did" class="form-control" readonly="true"> </div>
            <div class="form-group">
            <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divDelete');">
            <input type ="submit" value="Confirm Removal" class="btn btn-danger">
            </div> 
            
    </form> 
</div>

      
      
</div>
  

  <div class="col-md-6" id="productMatch">
  
 	 <div class="page-header">
   			<h3>Products in CC</h3>
        </div>

        <p class="pull-right">
            <a href="matcher.php?ccId=<?php echo $ccId; ?>" class="btn btn-primary">Product Match Manager</a>
        </p>
         
		<div class="table-responsive">
			<table class="table table-bordered table-hover text-center small-table">
                <tr>
                    <th scope="col">Product Name</th>
                    <th scope="col">Supplier</th>
                    <th scope="col">Match</th>
                    <th scope="col">Final</th>
                    <th scope="col">Reset Match</th>
                    <th scope="col">Match</th>
                    <th scope="col">Select Final</th>
                </tr>
                
 <?php
    if (isset($ccId)){
       
        //$invoices = getAllInvoiceId($ccId);
        //while ($invoice = $invoices->fetch(PDO::FETCH_ASSOC)){
            //echo '<tr><td colspan="7"></td></tr>';
            //$products = getProductInInvoice($invoice['Id']);
            $products = getProductInInvoiceByCC($ccId);
            while ($row = $products->fetch(PDO::FETCH_ASSOC)){
                //print_r($row);
                echo '<tr>';
                echo '<td>' . $row['Description'] . '</td>';
                echo '<td>' . $row['Name'] . '</td>';
                //check for existing match for this product
                $pilId = $row['pilId'];
                //$pid = $row['ProductIdLeft'];
                $matches = getProductMatch($pilId);
                $match = $matches->fetchAll(PDO::FETCH_ASSOC);
                //print_r($match);
                $final = 0;
                foreach ($match as $m){
                    if ($m['Final'] == 1){
                        $final = 1;   
                    }
                }
                if (count($match) > 0){
                    //match found
                    echo '<td><span class="glyphicon glyphicon-ok"></span></td>';
                    //final status
                    if ($final == 1){
                        echo '<td><span class="glyphicon glyphicon-ok"></span></td>';
                    }else{
                        echo '<td><span class="glyphicon glyphicon-remove"></span></td>';        
                    }
                    echo '<td> <button type="button" value="'. $row['pilId'] .'" class="btn btn-danger resetBtn">Reset</button></td>';
                    echo '<td> <button type="button" value="'. $row['pilId'] .'" class="btn btn-primary matchBtn">Match</button> </td>';    
                    echo '<td> <button type="button" value="'. $row['pilId'] .'" class="btn btn-info selectFinalBtn">Select</button></td>';
                }else{
                    //no match
                    echo '<td><span class="glyphicon glyphicon-remove"></span></td>';
                    echo '<td><span class="glyphicon glyphicon-remove"></span></td>';
                    echo '<td> <button type="button" class="btn btn-danger resetBtn" disabled>Reset</button></td>';
                    echo '<td> <button type="button" value="'. $row['pilId'] .'" class="btn btn-primary matchBtn">Match</button> </td>';    
                    echo '<td> <button type="button" class="btn btn-info selectFinalBtn" disabled>Select</button></td>';
                }                      
                echo '</tr>';
            }
       //}
        
    }
?>
                
            </table>
            
         </div>
        <form action="matcher.php" method="get" name="matchProductForm" id="matchProductForm">
            <input type="hidden" name="ccId" id="ccId" value="<?php echo $ccId; ?>">
            <input type="hidden" name="pilId" id="pilId">
        </form>

  </div>

</div>                   

<script type="text/javascript" src="js/ccmanager.js"></script>
<script type="text/javascript" src="js/ccmsgboard.js"></script>

<?
include ('includes/templates/footer.html');
?>