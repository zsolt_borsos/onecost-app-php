<?php
$page_title = "Reports";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');
    
}

include ('includes/templates/header.html');

?>
<!-- <link rel="stylesheet" type="text/css" href="css/custom.css" /> -->

<div class="page-header"><h2>Reports</h2></div>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4"><h4>Report</h4></div>
            <div class="col-md-8"><h4>Description</h4></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <form method="get" action="ccsummary.php">
                    <button type="submit" class="btn btn-link">Summary (Select CC)</button>
                    <?php
                    
                    //details: [sid] = id/name, [table] = table name, [col] = column to show, [valCol] = columns for Value,
                    $details = array();
                    $details['sid'] = "ccId";
                    $details['table'] = "vwrpt_ccsummaryddl";
                    $details['col'] = "display";
                    $details['valCol'] = "Id";
                    createSelect($details);
                    ?>
                </form>
            </div>
            <div class="col-md-8">This report displays the CC results of matched products for the selected CC.  Broken down by % saving, unit price difference, and total saving.</div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <a href="ccsummary.php">Matched Products - Highest savings</a>
            </div>
            <div class="col-md-8">This report displays ALL CC results of matched products.  Ordered by highest saving.  Broken down by % saving, unit price difference, and total saving.</div>
        </div>
    </div>
</div>


<?php
include ('includes/templates/footer.html');
?>
