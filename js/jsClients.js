function grabForEdit(id, name, actref, notes){
    document.getElementById('divEdit').style.display = 'block';
    document.getElementById('txtEditId').value = id;
    document.getElementById('txtEditName').value = name;
    document.getElementById('txtEditActRef').value = actref;
    document.getElementById('txtEditNotes').value = notes;
    document.getElementById('divEdit').scrollIntoView();
    // just for neatness
    document.getElementById('divDelete').style.display = 'none';
}
function grabForDelete(id, name){
    document.getElementById('divDelete').style.display = 'block';
    document.getElementById('txtDeleteId').value = id;
    document.getElementById('txtDeleteName').value = name;
    document.getElementById('divDelete').scrollIntoView();
    // just for neatness
    document.getElementById('divEdit').style.display = 'none';
} 
function addClient(){
    hideDiv('divViewClients'); 
    hideDiv('divEdit');
    hideDiv('divDelete');
    showDiv('divAddNew');
}