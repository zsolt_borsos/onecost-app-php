function grabForDelete(id, name){   
        $('#addForm').toggle(false);
        document.getElementById('did').value = id;
        document.getElementById('dname').value = name;
        // just for neatness
        $('#divDelete').toggle(true);
        document.getElementById('divDelete').scrollIntoView();
}

$( document ).ready(function() {

     
    
    $('.btnAdd').click(function(e){
        $('#divDelete').toggle(false);
        $('#myTable').toggle(false);
        $('#addForm').toggle(true);
        
    });
    
    $('.manageInvoiceBtn').click(function(){
    
        var invoiceId = this.value;
        $('#invId').val(invoiceId);
        $('#manageInvoiceForm').submit();
    
    });

    
    //reset matched item
    $('.resetBtn').click(function(){
        var del = confirm("Are you sure you want to reset the matched products?");
        if (del == true){
            var pilId = this.value,
                url = "includes/scripts/matcher.php",
                posting = $.post( url, {
                                        pilId : pilId,
                                        action : 'resetMatch'
                                        });
                posting.done(function( data ){
                if (data == '"done"'){
                    alert("Matched products reseted.");
                    location.reload();
                }else{
                    alert("Failed to reset matched products.\n" + data );      
                }   
                    location.reload();
                });
                
                posting.fail(function ( error ){
                    alert("Failed to connect to Database.\n" + error);
                });   
        }
    
    });
    
    //match button
    $('.matchBtn').click(function(){
        var pid = this.value;
        $('#pilId').val(pid);
        $('#matchProductForm').submit();
    });
    
    
    //select final button
    
    $('.selectFinalBtn').click(function(){
        var pilId = this.value,
            url = "includes/scripts/matcher.php",
            posting = $.post( url, {
                                    pilId : pilId,
                                    action : 'getMatches'
                                    });
            posting.done(function( data ){
                var matches = JSON.parse(data);
                $('#finalMatch').html('');
                for (var i = 0; i < matches.length; i++){
                    //console.log(matches[i]);
                    if (matches[i].Final == 1){
                        $('#finalMatch').append('<div class="radio"><label><input type="radio" name="finalMatchId" id="finalMatchId" value="'+ matches[i].Id +'" checked><b>Supplier:</b> '+ matches[i].suppName +' <b>Description: </b> '+ matches[i].Description + ' <b>Packsize: </b> '+ matches[i].Packsize +' <b>Price: </b> '+ matches[i].Price +' </label></div>');
                    }else{
                        $('#finalMatch').append('<div class="radio"><label><input type="radio" name="finalMatchId" id="finalMatchId" value="'+ matches[i].Id +'" required><b>Supplier:</b> '+ matches[i].suppName +' <b>Description: </b> '+ matches[i].Description + ' <b>Packsize: </b> '+ matches[i].Packsize +' <b>Price: </b> '+ matches[i].Price +' </label></div>');   
                    }
                }
                $('#finalMatchModal').modal('show');
            });

            posting.fail(function ( error ){
                alert("Failed to connect to Database.\n" + error);
            });
            
        
    });
    
    $('#finalMatch').submit(function(e){
        e.preventDefault();
        var pmId = $('#finalMatchId:checked').val(),
            url = "includes/scripts/matcher.php",
            posting = $.post( url, {
                                    pmId : pmId,
                                    action : 'setFinal'
                                    });
            posting.done(function( data ){
                if (data == '"done"'){
                    alert("Final match saved.");
                    location.reload();
                }else{
                    alert("Failed to save final match.\n" + data );      
                }   
                location.reload();
            });

            posting.fail(function ( error ){
                alert("Failed to connect to Database.\n" + error);
            });
    });
    
   
});