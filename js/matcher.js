$( document ).ready(function() {
    var selectedProducts = [];
    
    // add a match for selected product
    $('.selectMatchBtn').click(function(){
        console.log("select btn pressed.");
        self = $ (this );
        var prodId = this.value,
            pilId = $('#pilId').val(),
            rawData = this.getAttribute("data-match-info"),
            product = JSON.parse(rawData),
            url = 'includes/scripts/matcher.php',
            posting = $.post( url, {    action: 'addMatch',
                                        pilId : pilId, 
                                        prodId : prodId,
                                        priceId : product.priceId
                                    });


            posting.done(function( data ){
                if (data == '"done"'){
                    //alert("Match added.");
                    location.reload();
                    self.prop('disabled', true);
                }else if (data == 'indb'){
                    alert("This product is already selected as a match.");
                    self.prop('disabled', true);
                }else{
                    alert("Failed to add match.\n" + data );      
                }

            });

            posting.fail(function ( error ){
                //err = JSON.parse(error);
                alert("Failed to connect to the DB.\n" + error);
            });  
    });
    
    $('.removeMatchBtn').click(function(){
        var sure = confirm("Are you sure you want to remove this match?");
        if (sure == true){
            var pmId = this.value,
            url = 'includes/scripts/matcher.php',
            posting = $.post( url, {    action: 'removeMatch',
                                        pmId : pmId
                                    });
        
            posting.done(function( data ){
                if (data == '"done"'){
                    alert("Match removed.");
                    location.reload();
                }else{
                    alert("Failed to remove match.\n" + data );      
                }
            });
        
            posting.fail(function ( error ){
                alert("Failed to connect to the DB.\n" + error);
            });  
        }  
    });
    
    
});