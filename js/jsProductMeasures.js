function vwDescriptive(){
    hideDiv('divAddNew');
    hideDiv('divEdit');
    hideDiv('divDelete');
    showDiv('divViewMeasures');
    hideDiv('divViewMeasuresCC');
    hideDiv('divViewMeasuresBoth');
}
function vwCcMeasures(){
    hideDiv('divAddNew');
    hideDiv('divEdit');
    hideDiv('divDelete');
    document.getElementById('divViewMeasures').style.display = 'none';
    document.getElementById('divViewMeasuresCC').style.display = 'block';
    document.getElementById('divViewMeasuresBoth').style.display = 'none';
}
function vwBoth() {
    hideDiv('divAddNew');
    hideDiv('divEdit');
    hideDiv('divDelete');
    document.getElementById('divViewMeasures').style.display = 'none';
    document.getElementById('divViewMeasuresCC').style.display = 'none';
    document.getElementById('divViewMeasuresBoth').style.display = 'block';
}

function cancelAdd() {
    hideDiv('divAddNew');
    showDiv('divViewMeasuresBoth');
}




function grabForEdit(mId, productCode, mnId, value, forCC) {
    document.getElementById('divEdit').style.display = 'block';
    console.log(forCC);
    if (forCC == 0){
        forCC = false;   
    }else{
        forCC = true;   
    }
    document.getElementById('editCode').value = productCode;
    document.getElementById('editMeasureId').value = mId;
    document.getElementById('editmnId').value = mnId;
    document.getElementById('editValue').value = value;
    document.getElementById('editForCC').checked = forCC;
    document.getElementById('divEdit').scrollIntoView();
    // just for neatness
    document.getElementById('divDelete').style.display = 'none';
}


function grabForDelete(mId, pCode) {
    document.getElementById('divDelete').style.display = 'block';
    document.getElementById('txtDeleteId').value = mId;
    document.getElementById('txtDeleteName').value = pCode;
    document.getElementById('divDelete').scrollIntoView();
    // just for neatness
    document.getElementById('divEdit').style.display = 'none';
}
function addMeasure() {
    hideDiv('divEdit');
    hideDiv('divDelete');
    hideDiv('divViewMeasures');
    hideDiv('divViewMeasuresCC');
    hideDiv('divViewMeasuresBoth');
    showDiv('divAddNew');
}


