function grabForEdit(id, code, desc, pack){
    document.getElementById('divEdit').style.display = 'block';
    document.getElementById('txtEditId').value = id;
    document.getElementById('txtEditProd').value = code;
    document.getElementById('txtEditDesc').value = desc;
    document.getElementById('txtEditPackS').value = pack;
    document.getElementById('divEdit').scrollIntoView();
    // just for neatness
    document.getElementById('divDelete').style.display = 'none';
}
function addProduct(){
    hideDiv('divViewProducts'); 
    hideDiv('divEdit');
    hideDiv('divDelete');
    showDiv('divAddNew');
}
function addMissingPrices() {
    hideDiv('divViewProducts');
    hideDiv('divEdit');
    hideDiv('divDelete');
    showDiv('divMissingPrices');
    hideDiv('divMissingMeasures');
}
function grabForPrice(id, name) {
    document.getElementById('txtPriceId').value = id;
    document.getElementById('txtPriceProdCode').value = name;
    hideDiv('divMissingPrices');
    showDiv('divAddPrice');
}
// working on this...
function addMissingMeasures() {
    hideDiv('divViewProducts');
    hideDiv('divEdit');
    hideDiv('divDelete');
    showDiv('divMissingMeasures');
    hideDiv('divMissingPrices');
}



$( document ).ready(function() {
//console.log(self.currentPos);
        //stuff to track position
        var self = this;
        self.myStopPosition = 0;
        self.currentPos = 0;
        $(window).scroll(function(e){
            self.currentPos = $(document).scrollTop().valueOf();
        });
    
    
    $('.editBtn').click(function(e){
        //e.preventDefault();
        self.myStopPosition = self.currentPos;
        //console.log(self.myStopPosition);

    });
                        
    $('.cancelBtn').click(function(e){
        $('body').scrollTop(self.myStopPosition);
        //console.log(self.myStopPosition);
    });
    
    
    $('.delBtn').click(function(e){
        
        //dont need this anymore?
        self.myStopPosition = self.currentPos;
        
        //debug
        //console.log(self.myStopPosition);
        
        var myRow = $(this).closest("tr"),
            sure = confirm("Are you sure you want to delete this product?\nDescription: " + myRow.children()[1].textContent);
        if (sure == true){
            //debug stuff
            //console.log(myRow);  
            //console.log(myRow.children()[0].textContent);
            
            //button's value is the Product ID
            var id = $(this).val(),
            //console.log(id);
            url = 'products.php',
            //post this and check for the answer
            posting = $.post(url, {
                            txtDeleteId : id
            });
            posting.done(function(result){
                //console.log(result);
                    //instead of reloading everything I only need 1 hidden element's value
                    var updatedList = $ (result),
                        //get the answer
                        answer = updatedList.find('#ajaxanswer').val();
                //console.log("answer:" + answer);
                    if (answer){
                        //change this to fancy alert?
                        //alert("Product deleted.");
                        showAlert(true, "Product deleted.");
                        //remove the row visually
                        myRow.remove();
                    }else{
                        //alert("Failed to delete product!");
                        showAlert(false, "Failed to delete product!");
                    }   
            });
            posting.fail(function (error){
                alert("Could not connect to the Database.");
            });
            
        }else{
            console.log("Product is Not deleted.");
        }
        
    });
    
    
});
