function showDiv(x) {
    document.getElementById(x).style.display = 'block';
}
function hideDiv(x) {
    document.getElementById(x).style.display = 'none';
}
function toggle(x) {
if (document.getElementById(x).style.display !== 'none') {
        document.getElementById(x).style.display = 'none';
    } else {
        document.getElementById(x).style.display = 'block';
    }
}