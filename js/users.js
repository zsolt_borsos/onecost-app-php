function grabForEdit(row){
    var a = row;
    //console.log(row);
    hideDiv('delForm');
    document.getElementById('eid').value = row['Id'];
    document.getElementById('esname').value = row['Surname'];
    document.getElementById('efname').value = row['Firstname'];
    document.getElementById('eemail').value = row['Email'];
    if (row['Enabled'] > 0){
        $('#eenabled').prop('checked', true);
    }else{
        $('#eenabled').prop('checked', false);
    }
    document.getElementById('eaccess').value = row['AccessLevel'];
    showDiv('editForm');
     document.getElementById('editForm').scrollIntoView();
    
    
}
function grabForDelete(id, name){
    hideDiv('editForm');
    document.getElementById('did').value = id;
    document.getElementById('dname').value = name;
    // just for neatness
    showDiv('delForm');
    document.getElementById('delForm').scrollIntoView();
} 

$('.btnAdd').click(function(e){
    //$('.addForm').removeClass('hidden').addClass('show');
    //$('.info').addClass('hidden');
    hideDiv('editForm');
    hideDiv('delForm');
    hideDiv('userTable');
    showDiv('addForm');
});

$('.btnReset').click(function(e){
    e.preventDefault();
    var id = this.value;
    var conf = confirm("Are you sure you want to reset this user's password?");
    if (conf == true){
        console.log(id);
        $('#resetUserId').val(id);
        //document.getElementById('resetUserId').value = id;
        $('#resetPassForm').submit();    
    }else{
        alert("Password reset cancelled.");
    }
    
});