function grabForEdit(id, name, desc){
    
    hideDiv('divDelete');
    document.getElementById('erid').value = id;
    document.getElementById('ename').value = name;
    document.getElementById('edesc').value = desc;
    showDiv('editForm');
    document.getElementById('editForm').scrollIntoView();
    
    
}
function grabForDelete(id, name){
    hideDiv('editForm');
    document.getElementById('did').value = id;
    document.getElementById('dname').value = name;
    // just for neatness
    showDiv('divDelete');
    document.getElementById('divDelete').scrollIntoView();
} 

$('.btnAdd').click(function(e){
    //$('.addForm').removeClass('hidden').addClass('show');
    //$('.info').addClass('hidden');
    hideDiv('editForm');
    hideDiv('divDelete');
    hideDiv('roleTable');
    showDiv('addForm');
});

