$( document ).ready(function() {
    
    $('.btnEdit').click(function(e){
        e.preventDefault();
        
    var r=confirm("You are about to assign this page to yourself.\nContinue?");
	if (r==true){
        
	var $button = $ ( this ),
		buttonId = $button.attr("value"),
		url = "tracker.php";
		//alert("input:" + buttonId);
	
	var posting = $.post( url, { tid : buttonId,
                                 action : 'assign' 
                               });
        
		posting.done(function( data ){
			//obj = JSON.parse(data);
			location.reload();
            alert("Page is assigned to you.");
			
		});
		
		posting.fail(function ( error ){
			//err = JSON.parse(error);
			alert("Failed to assign page to you." + error);
		});
        
    }else{
        alert("Chicken... :p");   
    }
    });
    
    
    
    $('.btnRemove').click(function(e){
        e.preventDefault();
        
    var r=confirm("Are you sure?");
	if (r==true){
        
	var $button = $ ( this ),
		buttonId = $button.attr("value"),
		url = "tracker.php";
		//alert("input:" + buttonId);
	
	var posting = $.post( url, { tid : buttonId,
                                 action : 'remove'
                               });
        
		posting.done(function( data ){
			//obj = JSON.parse(data);
			location.reload();
            alert("You have been removed.");
			
		});
		
		posting.fail(function ( error ){
			//err = JSON.parse(error);
			alert("Failed to remove you from that." + error);
		});
        
    }else{
        alert("Chicken... :p");   
    }
    });
    
    $('.btnDelete').click(function(){
        var check = confirm("Are you sure you want to remove this task?");
        if (check == true){
            var tid = this.value,
                url = "tracker.php",		
                posting = $.post( url, { tid : tid,
                                     action : 'delete'
                                   });
            
            posting.done(function( data ){
                //obj = JSON.parse(data);
                location.reload();
                alert("Task removed.");
                
            });
            
            posting.fail(function ( error ){
                //err = JSON.parse(error);
                alert("Failed to remove you from that." + error);
            });
        }
    
    });
    
    
});
