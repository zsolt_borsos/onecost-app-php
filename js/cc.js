function grabForEdit(row, users){
    //console.log(row);
    console.log(users);
    
    hideDiv('divDelete');
    // eid, ecid, estatus, eprio, enote
    document.getElementById('eid').value = row['Id'];
    document.getElementById('ecid').value = row['ClientId'];
    document.getElementById('estatus').value = row['Status'];
    document.getElementById('eprio').value = row['Priority'];
    document.getElementById('enote').value = row['Notes'];
    
    //jquery part for checkbox (from users)
    var max = users.length;
    if (users){
        var boxes = $ ('#echecks input'); 
        //console.log(boxes);
        for (var i = 0; i < boxes.length; i++){
             boxes[i].checked = false;
            for (var k = 0; k < users.length; k++){
                var x = users[k].Id;
                if (boxes[i].value == x){
                    boxes[i].checked = true;  
                }                
            }
            //$('#echecks input:checkbox[value=x]').prop('checked', true);
            
        }
    }else{
        $ ('#eassign').prop('checked', false); 
    }
    
    
    
    showDiv('editForm');
    document.getElementById('editForm').scrollIntoView();   
}

function grabForDelete(id, name){
    $('#editForm').toggle(false);
    //hideDiv('editForm');
    $('#addForm').toggle(false);
    document.getElementById('did').value = id;
    document.getElementById('dname').value = name;
    // just for neatness
    $('#divDelete').toggle(true);
    document.getElementById('divDelete').scrollIntoView();
} 

$('.btnAdd').click(function(e){
    //$('.addForm').removeClass('hidden').addClass('show');
    //$('.info').addClass('hidden');
    //hideDiv('editForm');
    //hideDiv('divDelete');
    //hideDiv('myTable');
    //showDiv('addForm');
    $('#editForm').toggle(false);
    $('#divDelete').toggle(false);
    $('#myTable').toggle(false);
    $('#addForm').toggle(true);
    
});

$('.manageBtn').click(function(b){
    //alert(this.value);
    var id = this.value;
    $('#ccId').val(id);
    $('#manageForm').submit();

});