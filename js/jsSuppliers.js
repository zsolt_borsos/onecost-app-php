function grabForEdit(id, name, desc, notes, type){
    document.getElementById('divEdit').style.display = 'block';
    document.getElementById('txtEditId').value = id;
    document.getElementById('txtEditName').value = name;
    document.getElementById('txtEditDescription').value = desc;
    document.getElementById('txtEditNotes').value = notes;
    document.getElementById('etype').value = type;
    document.getElementById('divEdit').scrollIntoView();
    // just for neatness
    document.getElementById('divDelete').style.display = 'none';
}
function grabForDelete(id, name){
    document.getElementById('divDelete').style.display = 'block';
    document.getElementById('txtDeleteId').value = id;
    document.getElementById('txtDeleteName').value = name;
    document.getElementById('divDelete').scrollIntoView();
    // just for neatness
    document.getElementById('divEdit').style.display = 'none';
} 
function addSuppliers(){
    hideDiv('divViewSuppliers'); 
    hideDiv('divEdit');
    hideDiv('divDelete');
    showDiv('divAddNew');
}