// JavaScript Document

function timedRedirect(timer) {
	setTimeout("location.href = 'login.php';", timer);
}

$.fn.scrollTo = function (target, options, callback) {
    if (typeof options == 'function' && arguments.length == 2) { callback = options; options = target; }
    var settings = $.extend({
    scrollTarget  : target,
    offsetTop     : 50,
    duration      : 500,
    easing        : 'swing'
  }, options);
  return this.each(function(){
    var scrollPane = $(this);
    var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
    var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
    scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
      if (typeof callback == 'function') { callback.call(this); }
    });
  });
}

jQuery.fn.rotate = function(degrees) {
    $(this).css({'-webkit-transform' : 'rotate('+ degrees +'deg)',
                 '-moz-transform' : 'rotate('+ degrees +'deg)',
                 '-ms-transform' : 'rotate('+ degrees +'deg)',
                 'transform' : 'rotate('+ degrees +'deg)'});
};


function showIt(){
        $('.flashAlert').animate({
        opacity: 1,
      }, 600, function() {
        // Animation complete.
          console.log("Animated in.");
      });
};

function hideIt(){
    $('.flashAlert').animate({
        opacity: 0,
    }, 2000, function() {
        // Animation complete.
        console.log("Animated out.");
    });
};

function showAlert(good, text) {
    if (good == true){
        html = '<div class="alert alert-success alert-dismissible flashAlert" role="alert"><button type="button" class="close closeAlert" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' + text + '</div>';    
    }else{
        html = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close closeAlert" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' + text + '</div>';    
    }
    $('#messages').append(html);
    showIt();
    setTimeout("hideIt();", 5000);

};

//useful scripts for later / debug stuff / other
/*

 var keys = Object.keys(invoice);
        for (var i = 0; i < keys.length; i++) {
            var val = invoice[keys[i]]();
            alert(val);
        }
        
*/



