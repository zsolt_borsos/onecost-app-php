function grabForEdit(id, name, pid){
    
    //hideDiv('divDelete');
    //hideDiv('addForm');
    $('#divDelete').toggle(false);
    $('#addForm').toggle(false);
    document.getElementById('ecid').value = id;
    document.getElementById('ename').value = name;
    if (pid > 0){
        $('#esub').prop('checked', true);
        $('#eparent').toggle(true);
        document.getElementById('epid').value = pid;
    }else{
         $('#esub').prop('checked', false);
        $('#eparent').toggle(false);
    }
    
    //showDiv('editForm');
    $('#editForm').toggle(true);
    document.getElementById('editForm').scrollIntoView();
    
    
}
function grabForDelete(id, name){
    $('#editForm').toggle(false);
    //hideDiv('editForm');
    $('#addForm').toggle(false);
    document.getElementById('did').value = id;
    document.getElementById('dname').value = name;
    // just for neatness
    $('#divDelete').toggle(true);
    document.getElementById('divDelete').scrollIntoView();
} 

$('.btnAdd').click(function(e){
    //$('.addForm').removeClass('hidden').addClass('show');
    //$('.info').addClass('hidden');
    //hideDiv('editForm');
    //hideDiv('divDelete');
    //hideDiv('myTable');
    //showDiv('addForm');
    $('#editForm').toggle(false);
    $('#divDelete').toggle(false);
    $('#myTable').toggle(false);
    $('#addForm').toggle(true);
    
});

$('#sub').click(function() {
    $('#parent').toggle(this.checked);
});

$('#esub').click(function() {
    $('#eparent').toggle(this.checked);
});