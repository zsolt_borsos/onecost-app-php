function getMsg(){ 
    $.getJSON('includes/scripts/msgboard.php', function (data) {
        var newMsgCount = data.length;
        if (newMsgCount != msgCount){
            msgCount = newMsgCount;
            $('#chatHistory').html('');
            data.forEach(function(msg){
                console.log(msg);
                var dateTime = msg.Date.split(" "),
                    dayAndM = dateTime[0].split("-"),
                    HourAndMin = dateTime[1].split(":");
                $('#chatHistory').append('[' + dayAndM[1] + '/' + dayAndM[2] + ']' + '(' + HourAndMin[0] + ':' + HourAndMin[1] + ')' + ' ' +  msg.Name + ': ' + msg.Message + '\n');
                });
            $('#chatHistory').scrollTo(10000);
        }
    });
};

$( document ).ready(function() {
    msgCount = 0;
    getMsg();
    
    setInterval(function(){
        getMsg();
    }, 10000);
    //get the messages every 10 sec?
    
    $('#msgBoard').submit(function(e){
        e.preventDefault();
        var msg = $('#chatMsg').val(),
            url = 'includes/scripts/msgboard.php',
            posting = $.post( url, { chatMsg : msg });
                posting.done(function( data ){
                    console.log(data);
                    getMsg();
                    $('#chatMsg').val('');
                });
                posting.fail(function ( error ){
                    alert("Failed to connect to Database.\n" + error);
                });
        
    }); 
});