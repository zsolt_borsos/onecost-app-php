$( document ).ready(function() {
    
    
    $('#selectCC').submit(function(e){
        e.preventDefault();
        var ccId = $('#ccId').val(),
            url = 'includes/scripts/reportsapi.php?ccId=' + ccId;
            $('#allProducts').html(''); //reset table
            $.getJSON( url, function( data ) {
                //console.log(data);
                var tableStr = '',
                    //clear sortedList
                    sortedList = [];
                
                console.log(data.length);
                while(data.length > 0){
                    var current = data.pop(),
                        newProduct = {},
                        matches = [];
                    if (current){
                        console.log(data.length);
                        console.log("current:");
                        console.log(current);
                        newProduct.name = current.p1Desc;
                        newProduct.pack = current.p1Pack;
                        newProduct.costDetails = [{ "qty" : +current.p1Qty, "price" : +current.p1Price }];
                        newProduct.measure = +current.p1ValueInGram;
                        //newProduct.Price = +current.p1Price;
                        //newProduct.Total = +current.p1Price * +current.p1Qty;
                        /*
                        if (current.p1ValueInGram != null) {
                            newProduct.ProRata = (newProduct.Price / current.p1ValueInGram) * 100; //pro rata
                        }
                        */
                        //add matched product details
                        if (current.p2Desc != null){
                            var match = {   "desc" : current.p2Desc,
                                            "pack" : current.p2Pack,
                                            "price" : current.p2Price,
                                            "supplier" : current.p2Supplier };
                            if (current.p2Final == 1){
                                match.final = 1;   
                            }
                            if (current.p2ValueInGram != null){
                                match.proRata = (match.price / current.p2ValueInGram) * 100;
                            }
                            
                            matches.push(match);
                        }
                        console.log("match from 1st: ");
                        console.log(match);
                        for (var i = 0; i < data.length; i++){
                            console.log("i :" + i);
                            if (data[i]){
                                if (current.p1Desc == data[i].p1Desc){
                                    console.log("desc is same!" + i);
                                    if (current.p1PilId == data[i].p1PilId){
                                        console.log("pil is same!" + i);
                                        // if it is the same pilId then get the match, do not add up qty
                                        var sameProduct = data[i];
                                        //sameProduct = sameProduct[0];
                                        console.log("same product: ");
                                        console.log(sameProduct);
                                        if (sameProduct.p2Desc != null){
                                            var match2 = {  "desc" : sameProduct.p2Desc,
                                                            "pack" : sameProduct.p2Pack, 
                                                            "price" : sameProduct.p2Price,
                                                            "supplier" : sameProduct.p2Supplier };
                                            if (sameProduct.p2Final == 1){
                                                match2.final = 1;   
                                            }
                                            if (sameProduct.p2ValueInGram != null){
                                                match2.proRata = (match2.price / sameProduct.p2ValueInGram) * 100;
                                            }

                                            matches.push(match2);
                                        }
                                        delete data[i];
                                        console.log("match from 2nd: ");
                                        console.log(match2);
                                    }else{
                                        console.log("pil is not the same!" + i);
                                        var removedProduct = data[i];
                                        if (removedProduct.p2Desc != null){
                                            console.log("Match found in removed product!");
                                            var match3 = {  "desc" : removedProduct.p2Desc,
                                                            "pack" : removedProduct.p2Pack, 
                                                            "price" : removedProduct.p2Price,
                                                            "supplier" : removedProduct.p2Supplier };
                                            if (removedProduct.p2Final == 1){
                                                match3.final = 1;   
                                            }
                                            if (removedProduct.p2ValueInGram != null){
                                                match3.proRata = (match3.price / removedProduct.p2ValueInGram) * 100;
                                            }
                                            matches.push(match3);
                                        }
                                        var foundPriceCounter = 0;
                                        $.each(newProduct.costDetails, function (key, value){
                                            //console.log('in each!!');
                                            //console.log(key);
                                            //console.log(value);
                                            if (value.price == removedProduct.p1Price){
                                                newProduct.costDetails[key].qty += +removedProduct.p1Qty;
                                                foundPriceCounter++;
                                                console.log("Found the same price, qty added.");
                                            }
                                        });
                                        if (foundPriceCounter == 0){
                                            newProduct.costDetails.push({"qty" : +removedProduct.p1Qty, "price" : +removedProduct.p1Price});   
                                            console.log("Price is not found, pushing new price and qty for this product.");
                                        }
                                        delete data[i];
                                    }
                                }
                            }
                        }
                        //add matches to this product
                        newProduct.Matches = matches;
                        console.log("matches at end: ");
                        console.log(matches);
                        //add this product to the sortedList
                        sortedList.push(newProduct);
                        console.log("newProduct: ");
                        console.log(newProduct);
                    }
                }
                console.log(sortedList);
                
                
                
                    /* old way
                    //$('#allProducts').append('<tr>');
                    var p1Total = data[i].p1Price * data[i].p1Qty,
                        p2Total = data[i].p1Qty * data[i].p2Price;    
                    allTable += '<tr>';
                    var k = i-1;
                    if (i == 0) k = -1;
                    if (k >= 0){
                        if (data[i].p1Desc == data[k].p1Desc){
                            allTable += '<td colspan="3"></td>';
                        }else{
                            allTable += '<td>'+ data[i].p1Desc +'</td>';  //p1 desc
                            allTable += '<td>'+ data[i].p1Pack +'</td>';  //p1 pack    
                            allTable += '<td>'+ data[i].p1Qty +'</td>';   //p1 qty
                        }                    
                    }else{
                        allTable += '<td>'+ data[i].p1Desc +'</td>';  //p1 desc
                        allTable += '<td>'+ data[i].p1Pack +'</td>';  //p1 pack
                        allTable += '<td>'+ data[i].p1Qty +'</td>';   //p1 qty
                    }
                    allTable += '<td>£'+ data[i].p1Price +'</td>'; //p1 price
                    allTable += '<td>£'+ (p1Total).toFixed(2) +'</td>';   // p1 total
                    if (data[i].p1ValueInGram != null){
                        allTable += '<td>£'+ ((data[i].p1Price / data[i].p1ValueInGram) * 100).toFixed(2) +'</td>';    //p1 pro rata?
                    }else{
                        allTable += '<td> - </td>';
                    }
                    //add product to chart
                    products1.push(data[i].p1Desc);
                    prices1.push(data[i].p1Price);
                    
                    if (data[i].p2Desc != undefined && data[i].p2Desc != null){
                        allTable += '<td>'+ data[i].p2Desc +'</td>';  //p2 desc
                        allTable += '<td>'+ data[i].p2Pack +'</td>';  //p2 pack
                        allTable += '<td>£'+ data[i].p2Price +'</td>';  //p2 price
                        allTable += '<td>£'+ (p2Total).toFixed(2) +'</td>';  //p2 total (p1.Qty x p2.price? p2 should have own qty!!)
                        if (data[i].p2ValueInGram != null){
                            allTable += '<td>£'+ ((data[i].p2Price / data[i].p2ValueInGram) * 100).toFixed(2) +'</td>';  //p2 pro rata
                        }else{
                            allTable += '<td> - </td>';
                        }
                        allTable += '<td>£'+ (p1Total - p2Total).toFixed(2) +'</td>';  //variant (p1total - p2total)
                        var saving = ((p1Total - p2Total) / p1Total);
                        allTable += '<td>'+ saving.toFixed(2) +'%</td>';  //saving %
                        
                        //add product to chart
                        products2.push(data[i].p2Desc);
                        prices2.push(data[i].p2Price);
                    
                    }else{
                        allTable += '<td colspan="7">Not matched yet</td>';  //p2 desc
                        //add an empty entry if it is not matched yet?
                        products2.push('NA');
                        prices2.push('0');
                    }
                    allTable += '</tr>';
                    if (saving){
                        save.push(saving);
                    }else{
                        
                    }
                }
                */
                    
                
                //$('#allProducts').html(allTable);
                
                
                /*
                
                //Get context with jQuery - using jQuery's .get() method.
                var ctx = $("#priceDiff").get(0).getContext("2d");
                //This will get the first returned node in the jQuery collection.
                var priceDiffChart = new Chart(ctx),
                    priceDiffData = {
                    labels : products1,
                    datasets : [
                        {
                            fillColor : "rgba(220,220,220,0.5)",
                            strokeColor : "rgba(220,220,220,1)",
                            pointColor : "rgb(201, 195, 195)",
                            pointStrokeColor : "#ffbaba",
                            data : prices1
                        },
                        {
                            fillColor : "rgba(151,187,205,0.5)",
                            strokeColor : "rgba(151,187,205,1)",
                            pointColor : "rgba(151,187,205,1)",
                            pointStrokeColor : "#fff",
                            data : prices2
                        }
                        
                    ]
                
                };
            //show 1st chart                
            priceDiffChart.Line(priceDiffData);
                
            //create saving chart
            var ctx2 = $("#saving").get(0).getContext("2d");
                //This will get the first returned node in the jQuery collection.
                var savingChart = new Chart(ctx2),
                    savingData = {
                    labels : products1,
                    datasets : [
                        {
                            fillColor : "rgba(134, 242, 159, 0.5)",
                            strokeColor : "rgb(118, 193, 143)",
                            pointColor : "rgb(0, 178, 62)",
                            pointStrokeColor : "#ff2f2f",   
                            data : save 
                        }
                    ]
                
                };    
            savingChart.Line(savingData);
            */    
            });
        });
    
});
    
