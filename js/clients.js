// JavaScript Document

//TODO Check button handler. Dont know where will this go yet...
/*
//handling the hidden form submissions
$( '.hiddenform' ).submit(function(e) {
    var $form = $( this ),
	url = $form.attr('action');
});

*/

//handling client delete button click action
$(".delbtn").click(function(e) {
	var r=confirm("Are you sure you want to delete this client?\nClick OK if you want to delete this client.\nClick Cancel if you want cancel the action.");
	if (r==true){
  		var clientId = $(this).attr("value"),
		url = "includes/scripts/clientdelete.ajax.php";
		var posting = $.post( url, { cid : clientId });	
		posting.done(function( data ){
			alert("Client deleted successful.");	
			document.location.reload(true);
		});
		posting.fail(function ( error ){
			alert("Failed to delete client.");
		});
  	}else{	
  		alert("Cancelled deletion. Pheuww...");
  	}
});
	

// handling update client form submission here
$('#clientupdate').submit(function(e) {
    e.preventDefault();
	var $form = $( this ),
	clientId = $form.find("input[name='cid']").val(),
	clientName = $form.find("input[name='cname']").val(),
	clientActRef= $form.find("input[name='cactref']").val(),
	clientNote= $form.find("input[name='cnote']").val(),
	url = $form.attr("action");
	if (clientName == " "){
		alert("Please enter a valid name.");
		return;
	}else if (clientActRef == " "){
		alert("Please enter the ACT reference.");
		return;	
	}else{	
	var posting = $.post( url, { cid : clientId,
								 cname : clientName,
								 cactref : clientActRef,
								 cnote : clientNote
	});
	}	//end of if
	
	posting.done(function( data ){
		alert("Client details updated.");
		$('#clientupdatemodal').modal('hide');
		document.location.reload(true);	
	});
		posting.fail(function ( error ){
			alert("Failed to update client details.");
		});
});	

// this function is used with the editbtn click event
function updateFields(r){
	var $form = $("form[name='clientupdate']");
	$form.find("input[name='cid']").val(r.ClientID);
	$form.find("input[name='cname']").val(r.ClientName);
	$form.find("input[name='cactref']").val(r.ClientActReference);
	$form.find("input[name='cnote']").val(r.ClientNotes);
}

//handling client update hidden form process
$(".editbtn").click(function(e) {
	var formNumber = $(this).attr("value"),
		formName = "hiddenform" + formNumber,
		$form = $('#' + formName),
		url = "includes/scripts/client.ajax.php";
		//alert("Ready to post stuff now.");
		var posting = $.post( url, { cid : formNumber });
		
		posting.done(function( data ){
			var record = JSON.parse(data);
			//alert("Result: done" + data);
			updateFields(record);	
		});
		posting.fail(function ( error ){
			alert("Failed to get client details from the database.");
		});
});
	

// handling add client form submission here
$('#clientadd').submit(function(e) {
    e.preventDefault();
	var $form = $( this ),
	clientName = $form.find("input[name='cname']").val(),
	clientActRef= $form.find("input[name='cactref']").val(),
	clientNote= $form.find("input[name='cnote']").val(),
	url = $form.attr("action");
	if (clientName == " "){
		alert("Please enter a valid name.");
		return;
	}else if (clientActRef == " "){
		alert("Please enter the ACT reference.");
		return;	
	}else{
	
	var posting = $.post( url, { cname : clientName,
								 cactref : clientActRef,
								 cnote : clientNote
	});
	}	//end of if-else
		posting.done(function( data ){
			//obj = JSON.parse(data);
			alert("Client added to database.");
			$('#clientaddmodal').modal('hide');
            document.location.reload(true);
			
		});
			posting.fail(function ( error ){
			//err = JSON.parse(error);
			alert("Failed to add new client.");
			});
});	