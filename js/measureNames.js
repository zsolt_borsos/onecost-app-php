function grabForEdit(id, name, desc){
    
    //hideDiv('divDelete');
    //hideDiv('addForm');
    $('#divDelete').toggle(false);
    $('#addForm').toggle(false);
    document.getElementById('eid').value = id;
    document.getElementById('ename').value = name;
    document.getElementById('edesc').value = desc;
    //showDiv('editForm');
    $('#editForm').toggle(true);
    document.getElementById('editForm').scrollIntoView();
    
    
}
function grabForDelete(id, name){
    $('#editForm').toggle(false);
    //hideDiv('editForm');
    $('#addForm').toggle(false);
    document.getElementById('did').value = id;
    document.getElementById('dname').value = name;
    // just for neatness
    $('#divDelete').toggle(true);
    document.getElementById('divDelete').scrollIntoView();
} 

$('.btnAdd').click(function(e){
    //$('.addForm').removeClass('hidden').addClass('show');
    //$('.info').addClass('hidden');
    //hideDiv('editForm');
    //hideDiv('divDelete');
    //hideDiv('myTable');
    //showDiv('addForm');
    $('#editForm').toggle(false);
    $('#divDelete').toggle(false);
    $('#myTable').toggle(false);
    $('#addForm').toggle(true);
    
});
