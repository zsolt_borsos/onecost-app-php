function addNewSupplier() {
    showDiv('divAddSup');
    hideDiv('divAddCat');

    hideDiv('divDeleteCategory');
    hideDiv('divDeleteSupplier');
}

function addNewCategory() {
    showDiv('divAddCat');
    hideDiv('divAddSup');

    hideDiv('divDeleteCategory');
    hideDiv('divDeleteSupplier');
}


function grabForDelete_Sup(id, name) {
    document.getElementById('txtDeleteId_Sup').value = id;
    document.getElementById('txtDeleteSupplier').value = name;
    //hideDiv('divMissingPrices');
    showDiv('divDeleteSupplier');

    hideDiv('divDeleteCategory');
    hideDiv('divAddSup');
    hideDiv('divAddCat');
}

function grabForDelete_Cat(id, name) {
    document.getElementById('txtDeleteId_Cat').value = id;
    document.getElementById('txtDeleteCategory').value = name;
    //hideDiv('divMissingPrices');
    showDiv('divDeleteCategory');

    hideDiv('divDeleteSupplier');
    hideDiv('divAddSup');
    hideDiv('divAddCat');
}

function goBack() {
    window.location = 'products.php';
}