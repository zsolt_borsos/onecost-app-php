$( document ).ready(function() {
    
    //hide the footer for now
    $('#footer').toggle(false);
    
    //lets check if supplier is set before we start typing 
    $('#addProdForm > *').keydown(function(e){
        if ($('#suppSel').val() == undefined){
            alert("Please select a supplier first."); 
        }
    });
      
    var angle = 0, 
        img = document.getElementById('image');
    $('.rotate').click(function() {
        angle = (angle+90)%360;
        img.className = "rotate"+angle;
    });
    
    
    
    //handle add supplier modal
    
    $('#addSupp').submit(function(e) {
    e.preventDefault();
        var $form = $( this ),
        name= $form.find("input[name='suppName']").val(),
        desc = $form.find("input[name='suppDesc']").val(),
        notes = $form.find("input[name='suppNotes']").val(),
        type = $form.find("select[name='suppType']").val(),
        url = $form.attr("action");
        var posting = $.post( url, { suppName : name,
                                     suppDesc : desc,
                                     suppNotes : notes,
                                     suppType : type
                                   });
            
            posting.done(function( data ){
                //obj = JSON.parse(data);
                //alert(data);
                if (data == '"done"'){
                    alert("Supplier Added.");
                    $('#suppModal').modal('hide');
                    location.reload();
                }else if (data == '"exist"'){
                    alert("Supplier already in Database.");
                }else{
                    alert('Failed to add Supplier.');   
                }
                
                
            });
                posting.fail(function ( error ){
                //err = JSON.parse(error);
                alert(error);
                alert("Failed to add Supplier.");
                });
    
});

    //product selected from DB
    $('.prodSelectBtn').click(function(e){
        e.preventDefault();
        var pid = this.value;
        //console.log("pid: " + pid);
        //set inDb for pid
        $('#inDb').prop('value', pid);
        //console.log(this);
        
        //get the table row where the button is
        var btn = $ (this),
            rowObj = btn.closest('tr'),
            row = rowObj[0];
            //console.log(row);
            //var x = $ ( row ).eq(0).text();
            //console.log(x);
            var code = row.childNodes.item(0).innerHTML,
                name = row.childNodes.item(1).innerHTML,
                pack = row.childNodes.item(2).innerHTML;
            //console.log("code: " + code);
            //console.log("name: " + name);
            //console.log("pack: " + pack);
            //console.log(row.childNodes.item[0]);
            //console.log(row.childNodes.length);
        
        //set the values
        $('#prodCode').val(code);
        $('#prodDesc').val(name);
        $('#prodPack').val(pack);
        $('#prodModal').modal('hide');
    });
    
    //check if supplier set 1st
    $('#addProdForm').submit(function(e){
        if ($('#suppSel').val() == undefined){
            e.preventDefault();
            alert("Please select a supplier first.");    
        }
    });
    
    
    //Finish button handler
    
    $('.finishBtn').click(function(e){
        var status = this.value;
        //console.log(status);   
        var setTo = 0,
            invId = this.getAttribute("data-invid");
        //console.log(invId);
        if (status == 0){
            setTo = 1;
        }else{
            setTo = 0;   
        } 
            var url = "includes/scripts/invoiceHandler.php",
                posting = $.post( url, {    finishStatus : setTo,
                                            invId : invId
                                       });
                posting.done(function( data ){
                    //obj = JSON.parse(data);
                    //alert(data);
                    if (data == '"done"'){
                        alert("Invoice status updated.");
                        if (setTo == 1){
                            $('.finishBtn').html("Unlock Invoice");
                            
                        }else{
                             $('.finishBtn').html("Finish Invoice");
                        }
                    }else{
                        alert('Failed to lock invoice. Please try again.');   
                    }
                    location.reload();
                });
                
                posting.fail(function ( error ){
                //err = JSON.parse(error);
                alert("Failed to connect to Database." + error);
                });
    
    
    });
    
    //handle edit item button
    
    $('.editBtn').click(function(e){
        $('#divDelete').toggle(false);
        var d = this.getAttribute("data-edit-info");
        console.log(d);
        var details = JSON.parse(d);
        console.log(details);
        //eprodCode, eprodDesc, eprodPack, eqty, etotal
        $('#eprodCode').val(details.ProductCode);
        $('#eprodDesc').val(details.Description);
        $('#eprodPack').val(details.Packsize);
        $('#eqty').val(details.Qty);
        $('#etotal').val(details.PriceLeft);
        $('#prodLeftId').val(details.ProductIdLeft);
        $('#priceLeftId').val(details.PriceIdLeft);
        $('#pilId').val(details.pilId);
        $('#editItem').toggle(true);
        $('body').scrollTo('#editItem', {duration:'slow', offsetTop : '300'});
    });
    
    $('.delBtn').click(function(){
         $('#editItem').toggle(false);
        var name = this.getAttribute("data-remove-info"),
            id = this.value;
        $('#dname').val(name);
        $('#did').val(id);
        $('#divDelete').toggle(true);
        $('body').scrollTo('#divDelete', {duration:'slow', offsetTop : '300'});
    });
   
    
    //search for products
    $('#prodSearch').keyup(function() {
    var $rows = $('#prodBody tr');
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
    
    $rows.show().filter(function() {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
    });
    
    //reset search
    
    $('.resetBtn').click(function(){
        $('#prodSearch').val('');
        $('#prodSearch').keyup();
    });
    
    
    
    //disable reset details button if products are added
    if ($('#recordsTable tr').length > 1){
        $('.resetDetailsBtn').prop('disabled', true);
    }
    
    
    
    //reset invoice details
    //only allow user to reset if no products were added to the invoice
    $('.resetDetailsBtn').click(function(e){
        var invId = $('#invId').val(),
            url = "includes/scripts/invoiceHandler.php",
                posting = $.post( url, {    action : 'resetDetails',
                                            invId : invId
                                       });
                posting.done(function( data ){
                    //obj = JSON.parse(data);
                    //alert(data);
                    if (data == '"done"'){
                        alert("Reset successful.");
                    }else{
                        alert('Reset failed.');   
                    }
                    location.reload();
                });
                
                posting.fail(function ( error ){
                //err = JSON.parse(error);
                alert("Failed to connect to Database." + error);
                });
        
    });
    
    //product code checkbox
    
    $('#prodCheck').change(function(){
        if (this.checked){
            $('#prodCode').prop('readonly', false);
            $('#prodCode').prop('required', true);
            $('#prodCode').prop('placeholder', 'Enter product code');
            $('#prodCode').val('');
        }else{
            $('#prodCode').prop('readonly', true);
            $('#prodCode').prop('required', false);
            $('#prodCode').val('N/A');
        }
    
    });
   
    if ($('.msg').length > 0){
        $('#messages').css({"margin-top" : "60px", "margin-left" : "160px"});
    }
    
    
    //invoice notes popover
    $('.notesBtn').popover();
    $('.notesBtn').on('shown.bs.popover', function(e) {
        var currentTxt = $('.notesTxt').html();
        
        if (currentTxt.length > 0){
            //console.log(currentTxt);
            
            var buttons = "<p><br><button type='button' class='btn btn-danger btn-sm delNoteBtn'>Delete</button><button type='button' class='btn btn-primary btn-sm pull-right editNoteBtn'>Edit</button><br></p>";
            
            $('.notesTxt').append( buttons );
            
            $('.editNoteBtn').click(function(){
                var updateBtn = "<p><button type='button' class='btn btn-primary btn-sm pull-right updateNoteBtn'>Update</button></p><br><br>";
                $('.notesTxt').html("<input type='text' class='form-control note' value='" + currentTxt + "'><br>" + updateBtn ); 
                
                    //handle update notes
                    $('.updateNoteBtn').click(function(){
                        console.log('updating');
                        var invId = $('#invId').val(),
                            note = $('.note').val(),
                            url = "includes/scripts/invoiceHandler.php",
                            posting = $.post( url, {    action : 'updateNote',
                                                        invId : invId,
                                                        note : note
                                       });
                        posting.done(function( data ){
                            if (data == '"done"'){
                                alert("Notes updated.");
                                location.reload();
                            }else{
                                alert('Failed to update notes.');   
                            }
                            
                        });
                
                        posting.fail(function ( error ){
                            alert("Failed to connect to Database." + error);
                        });
                    });
                

            }); 
        }else{
            var addBtn = "<button type='button' class='btn btn-primary addNoteBtn'>Add notes</button><br>";
            $('.notesTxt').html(addBtn);
            
        }
       
        //handle add notes
        
        $('.addNoteBtn').click(function(){
            var addBtn = "<button type='button' class='btn btn-primary btn-sm pull-right addNote'>Add notes</button><br><br>";
            $('.notesTxt').html("<input type='text' class='form-control note'><br>" + addBtn );
            
            $('.addNote').click(function(){
                var note = $('.note').val();
                if (note.length > 0){
                    var invId = $('#invId').val(),
                        url = "includes/scripts/invoiceHandler.php",
                        posting = $.post( url, {    action : 'addNote',
                                                    invId : invId,
                                                    note : note
                                                });
                        posting.done(function( data ){
                            //obj = JSON.parse(data);
                            //alert(data);
                            if (data == '"done"'){
                                alert("Notes added.");
                                location.reload();
                            }else{
                                alert('Failed to add notes.');   
                            }
                                
                        });
                    
                        posting.fail(function ( error ){
                            //err = JSON.parse(error);
                            alert("Failed to connect to Database." + error);
                        });
                }else{
                    alert('Please enter a note.');   
                }
            
            });
            
        });
        
        //handle delete note
                    $('.delNoteBtn').click(function(){
                        var sure = confirm("Are you sure you want to delete this note?");
                        if (sure == true){
                            
                            var invId = $('#invId').val(),
                                url = "includes/scripts/invoiceHandler.php",
                                posting = $.post( url, {    action : 'delNote',
                                                            invId : invId               
                                           });
                            posting.done(function( data ){
                                if (data == '"done"'){
                                    alert("Notes deleted.");
                                    location.reload();
                                }else{
                                    alert('Failed to delete notes.');   
                                }
                                
                            });
                    
                            posting.fail(function ( error ){
                                alert("Failed to connect to Database." + error);
                            });
                        }
                    }); 
        
        
    });
    
    
    //handle: keep the position after adding a product
    $('#addProdForm').submit(function(e){
        e.preventDefault();
        var prodCode = $("#prodCode").val(),
            prodDesc = $("#prodDesc").val(),
            prodPack = $("#prodPack").val(),
            qty = $("#qty").val(),
            total = $("#total").val(),
            inDb = $("#inDb").val(),
            ccId = $("#ccId").val(),
            invId = $("#invId").val(),
            suppId = $("#suppId").val();
        url = "includes/scripts/invoiceHandler.php",
                        posting = $.post( url, {    
                                                prodCode : prodCode ,
                                                prodDesc : prodDesc,
                                                prodPack : prodPack,
                                                qty : qty,
                                                total : total,
                                                inDb : inDb,
                                                ccId : ccId,
                                                invId : invId ,
                                                suppId : suppId 
                                                });
                        posting.done(function( data ){
                            //obj = JSON.parse(data);
                            //alert(data);
                            //console.log(data);
                            var updatedList = $ (data);
                            $('#recordsTable').html(updatedList.find('#recordsTable').html());
                            alert("Product added to invoice.");
                            
                            //resetting form 
                            $("#prodCode").val('');
                            $("#prodDesc").val('');
                            $("#prodPack").val('');
                            $("#qty").val('');
                            $("#total").val('');
                            $("#inDb").val('0');
                        });
                    
                        posting.fail(function ( error ){
                            //err = JSON.parse(error);
                            alert("Failed to connect to Database." + error);
                        });
    });
    
    
    
});