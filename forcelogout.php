<?php
    session_start();
    $_SESSION = array(); // Clear the variables.
	session_destroy(); // Destroy the session itself.
	setcookie ('PHPSESSID', '', time()-3600, '/', '', 0, 0); // Destroy the cookie.
	
?>
<script type="text/javascript">
location.href = "login.php";
</script>