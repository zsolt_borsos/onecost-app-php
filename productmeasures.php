<?php
$page_title = "Product Measurement";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}
include ('includes/templates/header.html');


if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //print_r($_POST); //DEBUG INFO 
    
    if (isset($_POST["txtProdId"])) // Checks if the submit is from ADD NEW...
    {
        if (isset($_POST["cbForCC"])) 
            $x = 1;
        else
            $x=0;
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "insert into measure (ProductId, MeasureNameId, Value, ForCC) select :prodId, :measureNameId, :value, :forCc";    
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':prodId', $_POST["txtProdId"], PDO::PARAM_INT);
        $stmt->bindParam(':measureNameId', $_POST["mnId"], PDO::PARAM_INT);
        $stmt->bindParam(':value', $_POST["txtValue"], PDO::PARAM_STR);
        $stmt->bindParam(':forCc', $x, PDO::PARAM_INT); 
        $stmt->Execute();
        $db = null;
    }
    
    if (isset($_POST["editCode"])){
        
        //debug
        //print_r($_POST);
        
        if (isset($_POST["editForCC"])){
            $x = 1;
        }else{
            $x = 0;
        }
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "UPDATE measure SET ProductId = :prodId, MeasureNameId = :mnId, Value = :value, ForCC = :forCC WHERE Id = :mId";    
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':prodId', $_POST["editProdId"], PDO::PARAM_INT);
        $stmt->bindParam(':mnId', $_POST["editmnId"], PDO::PARAM_INT);
        $stmt->bindParam(':value', $_POST["editValue"], PDO::PARAM_STR);
        $stmt->bindParam(':forCC', $x, PDO::PARAM_INT);
        $stmt->bindParam(':mId', $_POST["editMeasureId"], PDO::PARAM_INT);
        $stmt->Execute();
        $db = null;
        
    }
    
    if (isset($_POST["txtDeleteId"]))
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "delete from measure where Id = :id";
        $stmt = $db->prepare($sql);  
        $stmt->bindParam(':id', $_POST["txtDeleteId"], PDO::PARAM_INT);
        try{
            $stmt->Execute();
        }catch (PDOException $e){
            setErrorMsg("Could not delete measure! Please report this error to an admin.");
            $url = '?prodid=' . $_GET["prodid"];
            redirect_user(url);
        }
    }
    
}

?>

<div id="page-header"><h2>Product Measures ::</h2>
    <p>Product measures for product 
        <b id="productname"> 
            <?php 
            if (isset($_GET["prodid"]))
            {
                $id = $_GET["prodid"]; 
                $db = connectDb(); 
                $sql = "SELECT Description, Packsize, ProductCode from product where  Id = {$id}";
                $result = $db->query($sql);
                $row = $result->fetch();
                echo "<a href=\"products.php\">" . $row[0] . " (" . $row[1] . ") </a>";
                $productDesc = $row[0];
                $productCode = $row[2];
                $productId = $id;
            }
            ?> 
        </b> listed below :</p> 
</div>

<p><input type="button" value="Add New Measure" class="btn btn-primary" onclick="addMeasure();" /></p>

<div class="row">
    <div class="col-md-4" style="text-align:center;"> <input type="button" value="View Descriptive Measures" class="btn btn-primary" onclick="vwDescriptive();" /> </div>
    <div class="col-md-4" style="text-align:center;"> <input type="button" value="View CC Measures (for calculation)" class="btn btn-primary" onclick="vwCcMeasures();" /> </div>
    <div class="col-md-4" style="text-align:center;"> <input type="button" value="View Both" class="btn btn-primary" onclick="vwBoth();" /> </div>
</div>

<div class="table-responsive" id="divViewMeasures" style="display: none;">
    <p>Below are the descriptive measures given to the selected product :</p>
    <table class="table table-bordered table-hover text-center">
        <tr>
           <!-- <th>M.Id</th> -->
            <th>Product Code</th>
            <th>Description</th>
            <th>Price</th>
            <th>MeasureType</th>
            <th>MeasureValue</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        <?php
        
        //connect to the database
        if (isset($_GET["prodid"])){ // only attempt to load page if has prodid in querystring
            $id = $_GET["prodid"]; 
            $db = connectDb(); 
            $sql = "SELECT m.Id, product.ProductCode, product.Description, price.Price, mn.Name, mn.Id as MeasureNameId, m.Value
            FROM product inner join price on product.Id = price.ProductId 
            inner join measure as m on product.Id = m.ProductId
            inner join measurename as mn on m.MeasureNameId = mn.Id
            where product.Id = {$id} and price.DateTo is null and ForCC=0
            order by m.Id";
            
            $result = $db->query($sql);
            $db = null;  
            
            $i = 0;
            while ($row = $result->fetch())
            {
                echo '<tr>';
                //echo '<td>' .  $row[0] .  '</td>';
                echo '<td>' .  $row['ProductCode'] .  '</td>';
                echo '<td>' .  $row['Description'] .  '</td>';
                echo '<td>' .  $row['Price'] .  '</td>';
                echo '<td>' .  $row['Name'] .  '</td>';
                echo '<td>' .  $row['Value'] .  '</td>';
                echo '<td> <input type="button" value="edit" class="btn btn-info" onclick="grabForEdit(\''.$row[0].'\',\''.$row['ProductCode'].'\',\''.$row['MeasureNameId'].'\',\''.$row['Value'].'\', 0)"  /></td>';
                echo '<td> <input type="button" value="delete" class="btn btn-danger" onclick="grabForDelete(\''.$row[0].'\',\''.$row['ProductCode'].'\');" /></td>';
                echo '</tr>';
                $i++;
            }
            if ($i == 0) // no results..
                echo '<td colspan ="8"> No Measurement info added to this product yet... </td>';
        }
        
        /*
        
        print_r($result); echo '<br />';
        
        print_r( $result->fetch());
          */
        ?>
    </table>
    
</div>

<div class="table-responsive" id="divViewMeasuresCC" style="display:none;">
    <p>Below are the measures used only for CC purposes (cost per g, Kg etc) :</p>
    <table class="table table-bordered table-hover text-center">
        <tr>
            <!-- <th>M.Id</th> -->
            <th>Product Code</th>
            <th>Description</th>
            <th>Price</th>
            <th>MeasureType</th>
            <th>MeasureValue</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        <?php
        
        //connect to the database
        if (isset($_GET["prodid"])){ // only attempt to load page if has prodid in querystring
            $id = $_GET["prodid"]; 
            $db = connectDb(); 
            $sql = "SELECT m.Id, product.ProductCode, product.Description, price.Price, mn.Name, mn.Id as MeasureNameId, m.Value
            FROM product inner join price on product.Id = price.ProductId 
            inner join measure as m on product.Id = m.ProductId
            inner join measurename as mn on m.MeasureNameId = mn.Id
            where product.Id = {$id} and price.DateTo is null and ForCC=1
            order by m.Id";
            
            $result = $db->query($sql);
            $db = null;  
            
            $i = 0;
            while ($row = $result->fetch())
            {
                echo '<tr>';
                //echo '<td>' .  $row[0] .  '</td>';
                echo '<td>' .  $row['ProductCode'] .  '</td>';
                echo '<td>' .  $row['Description'] .  '</td>';
                echo '<td>' .  $row['Price'] .  '</td>';
                echo '<td>' .  $row['Name'] .  '</td>';
                echo '<td>' .  $row['Value'] .  '</td>';
                echo '<td> <input type="button" value="edit" class="btn btn-info" onclick="grabForEdit(\''.$row[0].'\',\''.$row['ProductCode'].'\',\''.$row['MeasureNameId'].'\',\''.$row['Value'].'\', 1)"  /></td>';
                echo '<td> <input type="button" value="delete" class="btn btn-danger" onclick="grabForDelete(\''.$row[0].'\',\''.$row['ProductCode'].'\');" /></td>';
                echo '</tr>';
                $i++;
            }
            if ($i == 0) // no results..
                echo '<td colspan ="8"> No Measurement info added to this product yet... </td>';
        }
        
        /*
        print_r($result); echo '<br />';
        
        print_r( $result->fetch());
          */
        ?>
    </table>
</div>

<div class="table-responsive" id="divViewMeasuresBoth">
    <p>Below are ALL the measures for the selected product :</p>
    <table class="table table-bordered table-hover text-center">
        <tr>
            <!-- <th>M.Id</th> -->
            <th>Product Code</th>
            <th>Description</th>
            <th>Price</th>
            <th>MeasureType</th>
            <th>MeasureValue</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        <?php
        
        //connect to the database
        if (isset($_GET["prodid"])){ // only attempt to load page if has prodid in querystring
            $id = $_GET["prodid"]; 
            $db = connectDb(); 
            $sql = "SELECT m.Id, product.ProductCode, product.Description, price.Price, mn.Name, mn.Id as MeasureNameId, m.Value, m.ForCC
            FROM product inner join price on product.Id = price.ProductId 
            inner join measure as m on product.Id = m.ProductId
            inner join measurename as mn on m.MeasureNameId = mn.Id
            where product.Id = {$id} and price.DateTo is null
            order by m.Id";
            
            $result = $db->query($sql);
            $db = null;  
            
            $i = 0;
            while ($row = $result->fetch())
            {
                echo '<tr>';
                //echo '<td>' .  $row[0] .  '</td>';
                echo '<td>' .  $row['ProductCode'] .  '</td>';
                echo '<td>' .  $row['Description'] .  '</td>';
                echo '<td>' .  $row['Price'] .  '</td>';
                echo '<td>' .  $row['Name'] .  '</td>';
                echo '<td>' .  $row['Value'] .  '</td>';
                echo '<td> <input type="button" value="edit" class="btn btn-info" onclick="grabForEdit(\''.$row[0].'\',\''.$row['ProductCode'].'\',\''.$row['MeasureNameId'].'\',\''.$row['Value'].'\', \''.$row['ForCC'].'\')"  /></td>';
                echo '<td> <input type="button" value="delete" class="btn btn-danger" onclick="grabForDelete(\''.$row[0].'\',\''.$row['ProductCode'].'\');" /></td>';
                echo '</tr>';
                $i++;
            }
            if ($i == 0) // no results..
                echo '<td colspan ="8"> No Measurement info added to this product yet... </td>';
        }
        
        /*
        print_r($result); echo '<br />';
        
        print_r( $result->fetch());
          */
        ?>
    </table>
</div>



<div id="divAddNew" style="display:none;">
    <p>Add New Measure :</p>
    <form id="formAddNew" action="productmeasures.php?prodid=<?php echo $_GET["prodid"]; ?>" class="form-inline" method="post"> 
        <div class="form-group">
        <input type="text" name="txtProdCode" id="txtProdCode" readonly="true" class="form-control" value="<?php echo $productCode; ?>"/>   
        </div>
        <input type="hidden" name="txtProdId" id="txtProdId" value="<?php echo $productId; ?>">
        ||
        <div class="form-group">
            <?php
                //details: [sid] = id/name, [table] = table name, [col] = column to show, [valCol] = columns for Value,
                $details = array();
                $details['sid'] = "mnId";
                $details['table'] = "measurename";
                $details['col'] = "Name";
                $details['valCol'] = "Id";
                createSelect($details);
            ?>
        </div> 
        || 
        <div class="form-group">
        <input type="text" name="txtValue" id="txtValue" placeholder="Enter Measure Value" required class="form-control" />
        </div> 
        || 
        <div class="form-group"> Is this for CC (Calculation)?
        <input type="checkbox" name="cbForCC" id="cbForCC" class="form-control" />
        </div> 

        <div class="form-group">
        <input type="button" value="Cancel" class="btn btn-default" onclick="cancelAdd();">
        <input type ="submit" value="Add New" class="btn btn-primary" />
        </div> 
    </form> 
</div>

<div id="divEdit" style="display:none;">
    <p>Edit Measure Info :</p>
    <form id="formEdit" action="productmeasures.php?prodid=<?php echo $_GET["prodid"]; ?>" class="form-inline" method="post"> 
        <div class="form-group">
            <input type ="text" name="editCode" id="editCode" class="form-control" readonly="true" />
        </div>
        <input type="hidden" name="editProdId" id="editProdId" value="<?php echo $productId; ?>">
        <input type="hidden" id="editMeasureId" name="editMeasureId">
        || 
        <div class="form-group">
            <?php
                //details: [sid] = id/name, [table] = table name, [col] = column to show, [valCol] = columns for Value,
                $details = array();
                $details['sid'] = "editmnId";
                $details['table'] = "measurename";
                $details['col'] = "Name";
                $details['valCol'] = "Id";
                createSelect($details);
            ?>
        </div>
        ||
        <div class="form-group">
            <input type="text" name="editValue" id="editValue" placeholder="Enter Measure Value"  required class="form-control" />
        </div> 
        ||
        <div class="form-group"> Is this for CC (Calculation)?
            <input type="checkbox" name="editForCC" id="editForCC" class="form-control" />
        </div>
        ||
        <div class="form-group">
            <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divEdit');">
            <input type ="submit" value="Edit Entry" class="btn btn-primary" />
        </div>
    </form> 
</div>

<div id="divDelete" style="display:none;">
    <p>Delete Measure :</p>
    <form id="formDelete" action="productmeasures.php?prodid=<?php echo $_GET["prodid"]; ?>" class="form-inline" method="post"> 
        <p><span class="bg-danger">Warning you are about to delete this record, this cannot be un-done!</span>
        Are you sure you want to remove :</p>
        <div class="form-group">
        <input type="text" name="txtDeleteName" id="txtDeleteName" readonly="true" class="form-control"> </div> 
        || Id :  
        <div class="form-group">
        <input type ="text" name="txtDeleteId" id="txtDeleteId" class="form-control" readonly="true"></div>  ?
        <div class="form-group">
        <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divDelete');">
        <input type ="submit" value="Confirm Removal" class="btn btn-danger" />
        </div> 
    </form> 
</div>


<script src="js/jsProductMeasures.js"></script>


<?php
include ('includes/templates/footer.html');
?>
