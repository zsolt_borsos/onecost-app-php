<?php
$page_title = "Measures";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

include ('includes/templates/header.html');

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //print_r($_POST);
    if (isset($_POST['name'])){
        $details = array();
        $details['name'] = $_POST['name'];
        $details['desc'] = $_POST['desc'];
        if (addMeasureName($details)){
            setMsg("Measure Name added.");   
            reloadMe();
        }else{
            setErrorMsg("Measure Name could not be added.");
            reloadMe();
        }    
        
    }
    if (isset($_POST['eid'])){
        $details = array();
        $details['name'] = $_POST['ename'];
        $details['id'] = $_POST['eid'];
        $details['desc'] = $_POST['edesc'];
        if (updateMeasureName($details)){
            setMsg("Measure Name updated.");
            reloadMe();
        }else{
            setErrorMsg("Measure Name could not be updated.");
            reloadMe();
        }
    }
    
    if (isset($_POST['did'])){
        if (deleteMeasureName($_POST['did'])){
            setMsg("Measure Name deleted.");   
            reloadMe();
        }else{
            setErrorMsg("Measure Name could not be deleted.");
            reloadMe();
        }   
        
    }
    
}

?>


<div class="page-header">
    <h2>Measurements</h2>
</div>

<div>
    <p>
        <button type="button" class="btn btn-primary btnAdd">Add Measure Name</button>
        <input type="button" value="Toggle Search" class="btn btn-primary" onclick=" $('#divSearch').toggle();">
        <button type="button" class="btn btn-default" onclick="location.reload();">Reset Search</button>
    </p>
</div>    

<div id="divSearch" style="display:none;">
    <form id="search" action="measures.php" class="form-inline" method="post"> 
    <div class="form-group">
        <input type="text" name="txtSearch" id="txtSearch" placeholder="Search measures..." class="form-control"> </div>
        ||
        <div class="form-group">
        <input type ="submit" value="Search" class="btn btn-primary">
        </div> 
    </form>
</div>

<div class="table-responsive info" id="myTable">
<p>List of measurements:</p>
    <table class="table table-bordered table-hover text-center">
		<tr>
		<!-- <th>Measurement Id</th> -->
		<th>Measurement</th>
        <th>Description</th>
        <th>Edit</th>
        <th>Delete</th>
		</tr>
        <?php 
        if (isset($_POST['txtSearch'])){
            $results = getMeasureNameByName($_POST['txtSearch']);
        }else{
            $results = getAllMeasureName();
        }
        //print_r($results);
            while ($row = $results->fetch()){
                //print_r($row);
                echo '<tr>';
                    //echo '<td>' . $row['Id'] . '</td>';
                    echo '<td>' . $row['Name'] . '</td>';
                    echo '<td>' . $row['Description'] . '</td>';
                    echo '<td> <input type="button" value="edit" class="btn btn-info" onclick="grabForEdit(\''.$row['Id'].'\', \''.$row['Name'].'\', \''.$row['Description'].'\');"></td>';
                    echo '<td> <input type="button" value="delete" class="btn btn-danger"              onclick="grabForDelete(\''.$row['Id'].'\',\''.$row['Name'].'\');"></td>';
                    echo '</tr>';
            }
        ?>
        
    </table>
</div>

<div id="addForm" class="addForm" style="display:none;">
    <form class="form-inline" role="form" action="measures.php" method="post">
        <div class="form-group">
            <label class="sr-only" for="name">Measure Name</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Measure name" required>
        </div>
        ||
        <div class="form-group">
            <label class="sr-only" for="desc">Measure Description</label>
            <input type="text" class="form-control" name="desc" id="desc" placeholder="Measure description" required>
        </div>
        ||
    <button type="button" class="btn btn-default" onclick="hideDiv('addForm'); showDiv('myTable');">Cancel</button>
    <button type="submit" class="btn btn-primary">Add Measure</button>
</form>
</div>


<div id="editForm" class="editForm" style="display:none;">
    <form class="form-inline" role="form" action="measures.php" method="post">
  
        <div class="form-group">
            <label class="sr-only" for="eid">Measure Id</label>
            <input type="text" class="form-control" name="eid" id="eid" placeholder="Measure Id" readonly>
        </div>
        ||
        <div class="form-group">
            <label class="sr-only" for="ename">Measure Name</label>
            <input type="text" class="form-control" name="ename" id="ename" placeholder="Measure name">
        </div>
        ||
        <div class="form-group">
            <label class="sr-only" for="desc">Measure Description</label>
            <input type="text" class="form-control" name="edesc" id="edesc" placeholder="Measure description" required>
        </div>
        ||
    <button type="button" class="btn btn-default" onclick="$('#editForm').toggle(false);">Cancel</button>
    <button type="submit" class="btn btn-primary">Update Measure</button>
</form>
</div>


<div id="divDelete" style="display:none;">
    <form id="formDelete" action="measures.php" class="form-inline" method="post" role="form"> 
        <p>Delete Measure:</p>    
        <p>
            <span class="bg-danger">Warning you are about to delete this record, this cannot be un-done!</span>
            Are you sure you want to remove :
        </p>
        <p>
            <div class="form-group">
            <input type="text" name="dname" id="dname" readonly="true" class="form-control"> </div> 
            Id :  
            <div class="form-group">
            <input type ="text" name="did" id="did" class="form-control" readonly="true"> </div>  ?
            <div class="form-group">
            <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divDelete');">
            <input type ="submit" value="Confirm Removal" class="btn btn-danger">
            </div> 
        </p>     
    </form> 
</div>

<script type="text/javascript" src="js/measureNames.js"></script>
<?php
include ('includes/templates/footer.html');
?>
