<?php # login_page.inc.php
// This page creates the login form.
// Display the form:
?>
<form action="login.php" method="post" enctype="application/x-www-form-urlencoded" class="well form-horizontal" id="loginform" accept-charset="UTF-8" role="form">

<div class="page-header">
   		<h1>Login</h1>
        </div>

	<div class="form-group" id="emailgrp">
    <label for="email" class="col-md-3 col-sm-3 col-xs-3 control-label">Email</label>
    <div class="col-lg-6 col-md-8 col-sm-8 col-xs-8">
      <input name="email" type="email" required class="form-control" id="email" placeholder="Enter your email">
    </div>
  </div>
  
	<div class="form-group" id="passgrp">
    <label for="pass" class="col-md-3 col-sm-3 col-xs-3 control-label">Password</label>
    <div class="col-lg-6 col-md-8 col-sm-8 col-xs-8">
      <input name="pass" type="password" required class="form-control" id="pass" placeholder="Enter your password" maxlength="25">
    </div>
  </div>


<div class="form-group">
     <div class="col-xs-3 visible-xs"></div>
     <div class="col-lg-offset-6 col-lg-3 col-md-offset-7 col-md-4 col-sm-offset-3 col-sm-8 col-xs-8">
  		<button value="login" type="submit" class="btn btn-primary btn-block" id="login">Login</button>
        
  	 </div>
      <div class="col-xs-1 visible-xs"></div>
    </div>
     
</form>