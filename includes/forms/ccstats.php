<?php
    //this widget expects ccId in GET to work
    $db = connectDb();
    $myccId = $_GET['ccId'];
    //
    $sql = "SELECT COUNT(i.Id) FROM invoice AS i
            WHERE i.CcId = $myccId";
	$stmt = $db->query($sql);
    $invoices = $stmt->fetchColumn();
    //
    $sql = "SELECT COUNT(i.Id) FROM invoice AS i
            WHERE i.CcId = $myccId
            AND i.Status = 0";
    $stmt = $db->query($sql);
    $invFinish = $stmt->fetchColumn();
    $sql = "SELECT COUNT(DISTINCT p.Description) FROM product AS p
            INNER JOIN productinvoicelink AS pil ON pil.ProductIdLeft = p.Id
            INNER JOIN invoice AS i ON i.Id = pil.InvoiceId
            WHERE i.CcId = $myccId";
    $stmt = $db->query($sql);
    $products = $stmt->fetchColumn();
    $sql = "SELECT COUNT(DISTINCT ProductIdLeft) FROM productinvoicelink AS pil
            INNER JOIN productmatch AS pm ON pil.Id = pm.InvoiceLinkId
            INNER JOIN invoice AS i ON i.Id = pil.InvoiceId
            WHERE i.CcId = $myccId
            ";
    $stmt = $db->query($sql);
    $matched = $stmt->fetchColumn();
    $toMatch = $products - $matched;
    $sql = "SELECT COUNT(DISTINCT ProductIdLeft) FROM productinvoicelink AS pil
            INNER JOIN productmatch AS pm ON pil.Id = pm.InvoiceLinkId
            INNER JOIN invoice AS i ON i.Id = pil.InvoiceId
            WHERE i.CcId = $myccId
            AND pm.Final = 1";     
    $stmt = $db->query($sql);
    $finals = $stmt->fetchColumn();
    $toFinal = $matched - $finals;
?>

<div class="row">
    <div class="page-header">
        <h4>CC stats</h4>
    </div>
    <div class="well">
        <p><b>Invoices: </b><span><?php echo $invoices; ?></span></p>
        <p><b>Invoices to finish: </b><span><?php echo $invFinish; ?></span></p>
        <p><b>Products: </b><span><?php echo $products; ?></span></p>
        <p><b>Products to Match: </b><span><?php echo $toMatch; ?></span></p>
        <p><b>Products to Final Match: </b><span><?php echo $toFinal; ?></span></p>
    </div>
</div>