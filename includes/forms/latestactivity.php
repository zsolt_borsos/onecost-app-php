<?php
    $db = connectDb();
    $sql = "SELECT DateFrom FROM price WHERE DateTo IS NULL AND Notes LIKE '%Inserted with%' ORDER BY DateFrom DESC LIMIT 1";
	$stmt = $db->query($sql);
    $price = $stmt->fetchColumn();
    $sql = "SELECT Name FROM client ORDER BY Id DESC LIMIT 1";
    $stmt = $db->query($sql);
    $client = $stmt->fetchColumn();
    $sql = "SELECT Name FROM supplier ORDER BY Id Desc LIMIT 1";
    $stmt = $db->query($sql);
    $supplier = $stmt->fetchColumn();
    $sql = "SELECT CreatedAt, u.Firstname AS fn, u.Surname AS sn FROM cc 
            INNER JOIN user AS u ON cc.CreatedBy = u.Id
            ORDER BY CreatedAt DESC LIMIT 1";
    $stmt = $db->query($sql);
    $ccInfo = $stmt->fetch(PDO::FETCH_ASSOC);
    $ccAt = $ccInfo['CreatedAt'];
    $ccBy = $ccInfo['fn'] . ' ' . $ccInfo['sn'];
?>

<div class="row">
    <div class="page-header">
        <h4>Latest activities</h4>
    </div>
    <div class="well">
        <p><b>Prices updated at: </b><span><?php echo $price; ?></span></p>
        <p><b>Latest Client: </b><span><?php echo $client; ?></span></p>
        <p><b>Latest Supplier: </b><span><?php echo $supplier; ?></span></p>
        <p><b>Latest CC created at: </b><span><?php echo $ccAt; ?></span></p>
        <p><b>Latest CC created by: </b><span><?php echo $ccBy; ?></span></p>
    </div>
</div>