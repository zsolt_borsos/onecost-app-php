<form method="post" class="form-horizontal" action="includes/scripts/msgboard.php" id="msgBoard" name="msgBoard">
    <div class="form-group">
        <textarea class="form-control" rows="10" name="chatHistory" id="chatHistory" readonly></textarea>
    </div>
    <div class="row">
        <div class="input-group">
            <input type="text" name="chatMsg" id="chatMsg" class="form-control" placeholder="Enter your message" required>
            <span class="input-group-btn">
                <button type="submit" class="btn btn-primary" name="sendMsg" id="sendMsg">Send</button>
            </span>
        </div>
    </div>
</form>