<?php

    $db = connectDb();
    $sql = "SELECT COUNT(Id) FROM product";
	$stmt = $db->query($sql);
    $products = $stmt->fetchColumn();
    $sql = "SELECT COUNT(Id) FROM client";
    $stmt = $db->query($sql);
    $clients = $stmt->fetchColumn();
    $sql = "SELECT COUNT(Id) FROM supplier";
    $stmt = $db->query($sql);
    $suppliers = $stmt->fetchColumn();
    $sql = "SELECT COUNT(Id) FROM cc";
    $stmt = $db->query($sql);
    $cc = $stmt->fetchColumn();
    $sql = "SELECT COUNT(Id) FROM invoice";
    $stmt = $db->query($sql);
    $invoices = $stmt->fetchColumn();
?>
<div class="row">
    <div class="page-header">
        <h4>App stats</h4>
    </div>
    <div class="well">
        <p><b>Products: </b><span><?php echo $products; ?></span></p>
        <p><b>Clients: </b><span><?php echo $clients; ?></span></p>
        <p><b>Suppliers: </b><span><?php echo $suppliers; ?></span></p>
        <p><b>CC's: </b><span><?php echo $cc; ?></span></p>
        <p><b>Invoices: </b><span><?php echo $invoices; ?></span></p>
    </div>
</div>