<div class="row well">
    <div class="page-header">
        <h1>User profile</h1>
    </div>
    <div id="uDetails">
        <form action="includes/scripts/userupdate.php" method="post" enctype="application/x-www-form-urlencoded" class="form-horizontal" id="userupdateform" accept-charset="UTF-8" role="form">
    
    
        <div class="form-group">
            <label for="fname" class="col-md-3 col-sm-3 col-xs-3 control-label">First name</label>
            <div class="col-lg-6 col-md-8 col-sm-8 col-xs-8">
                <input name="fname" type="text" required class="form-control" id="fname" placeholder="Enter your first name" value="<?php echo $fname;?>">
            </div>
        </div>
    
        <div class="form-group">
            <label for="sname" class="col-md-3 col-sm-3 col-xs-3 control-label">Surname</label>
            <div class="col-lg-6 col-md-8 col-sm-8 col-xs-8">
                <input name="sname" type="text" required class="form-control" id="sname" placeholder="Enter your surname" value="<?php echo $sname;?>">
            </div>
        </div>


        <div class="form-group">
            <label for="email" class="col-md-3 col-sm-3 col-xs-3 control-label">Email</label>
            <div class="col-lg-6 col-md-8 col-sm-8 col-xs-8">
              <input name="email" type="email" required class="form-control" id="email" placeholder="Enter your email" value="<?php echo $email;?>">
            </div>
        </div>
  
        <div class="form-group">
            <label for="pass" class="col-md-3 col-sm-3 col-xs-3 control-label">Password</label>
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-8">
                 <button type="button" class="btn btn-info passChange">Change password</button> 
        </div>
      </div>

        <div class="form-group">
             <div class="col-xs-3 visible-xs"></div>
                <div class="col-lg-offset-6 col-lg-3 col-md-offset-7 col-md-4 col-sm-offset-3 col-sm-8 col-xs-8">
                    <button value="submit" type="submit" class="btn btn-primary btn-block" id="submit">Update</button>  
                </div>
              <div class="col-xs-1 visible-xs"></div>
        </div>        
            <input type="hidden" id="action" name="action" value="update">
    </form>
 </div>

<div id="uPass" style="display:none;">
     <form action="includes/scripts/userupdate.php" method="post" enctype="application/x-www-form-urlencoded" id="changePass" name="changePass" accept-charset="UTF-8" role="form">
         <div class="col-md-offset-2 col-md-6 well">
        <div class="form-group">
            <label for="oldPass">Current password</label>
            <input name="oldPass" id="oldPass" type="password" required class="form-control"  placeholder="Enter current password">
        </div>
        <div class="form-group">
            <label for="newPass">New password</label>
              <input name="newPass" id="newPass" type="password" required class="form-control"  placeholder="Enter new password">
        </div>
        
        <div class="form-group pull-right">
             <button type="button" class="btn btn-default cancelBtn">Cancel</button>
            <button value="submit" type="submit" class="btn btn-primary" id="submit" form="changePass">Change password</button>
           
        </div>
           <input type="hidden" id="action" name="action" value="changePass">
        </div>
    </form>
</div>

</div>
<script type="text/javascript" src="js/userupdate.js"></script>
