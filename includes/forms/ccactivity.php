<?php
    //this widget expects ccId in GET to work
    $db = connectDb();
    $myccId = $_GET['ccId'];
    //get latest invoice's supplier name
    $sql = "SELECT i.Id, s.Name FROM invoice AS i
            INNER JOIN supplier AS s ON i.SupplierId = s.Id
            WHERE i.CcId = $myccId
            ORDER BY i.Id DESC LIMIT 1";
	$stmt = $db->query($sql);
    $invoice = $stmt->fetchColumn(1);
    //get latest product added to any of these invoices? hmm...
    $sql = "SELECT p.Description FROM product AS p
            INNER JOIN productinvoicelink AS pil ON pil.ProductIdLeft = p.Id
            INNER JOIN invoice AS i ON i.Id = pil.InvoiceId
            WHERE i.CcId = $myccId
            ORDER BY p.Id DESC LIMIT 1";
    $stmt = $db->query($sql);
    $product = $stmt->fetchColumn();
    $sql = "SELECT p.Description FROM product AS p
            INNER JOIN productmatch AS pm ON pm.ProductId = p.Id
            INNER JOIN productinvoicelink AS pil ON pil.Id = pm.InvoiceLinkId
            INNER JOIN invoice AS i ON i.Id = pil.InvoiceId
            WHERE i.CcId = $myccId
            ORDER BY pm.Id DESC LIMIT 1";
    $stmt = $db->query($sql);
    $match = $stmt->fetchColumn();
    $sql = "SELECT p.Description FROM product AS p
            INNER JOIN productmatch AS pm ON pm.ProductId = p.Id
            INNER JOIN productinvoicelink AS pil ON pil.Id = pm.InvoiceLinkId
            INNER JOIN invoice AS i ON i.Id = pil.InvoiceId
            WHERE i.CcId = $myccId
            AND pm.Final = 1
            ORDER BY pm.Id DESC LIMIT 1";
    $stmt = $db->query($sql);
    $final = $stmt->fetchColumn();
    
?>

<div class="row">
    <div class="page-header">
        <h4>Latest activities</h4>
    </div>
    <div class="well">
        <p><b>Latest Invoice From: </b><span><?php echo $invoice; ?></span></p>
        <p><b>Latest Product: </b><span><?php echo $product; ?></span></p>
        <p><b>Latest Match: </b><span><?php echo $match; ?></span></p>
        <p><b>Latest Final Match: </b><span><?php echo $final; ?></span></p>
    </div>
</div>