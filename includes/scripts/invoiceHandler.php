<?php
include ('appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}



if ($_SERVER['REQUEST_METHOD'] == 'POST'){
 
    //handle add supplier requests
    //expects $detail array with parameter: sname, sdesc, snote, stype
    if (isset($_POST['suppName'])){
        $details = array();
        $details['sname'] = $_POST['suppName'];
        $details['sdesc'] = $_POST['suppDesc'];
        $details['snote'] = $_POST['suppNotes'];
        $details['stype'] = $_POST['suppType'];
        
        //check if supplier already in DB
    
        $checker = getSuppliersByName($details['sname']);
        $supplier = $checker->fetch();
        if (empty($supplier)){
            if (addSupplier($details)){
                $msg = "done";
            }else{
                $msg = "fail";
            }      
        }else{
            $msg = "exist";   
        }
        echo json_encode($msg);
    }
    
    //handle add product to invoice requests
    if (isset($_POST['prodCode'])){
        //add the product
        //steps: add product(if not selected), add price, add product-supplier link, add product-invoice link
        $good = true;
        $ccId = $_POST['ccId'];
        //if product is not in db
        if ($_POST['inDb'] == 0){
            //then add it (new product)
            $details = array();
            $details['code'] = $_POST['prodCode'];
            $details['desc'] = $_POST['prodDesc'];
            $details['pack'] = $_POST['prodPack'];
            //add product
            list($go, $answer) = addProduct($details);
            if ($go){   
                //then next step is to add the price
                $pid = $answer;
               
                $msg = "Product added to Db.";
                // addPrice() expects in details: pid, price, notes
                $pDetails = array();
                $pDetails['pid'] = $pid;
                $total = $_POST['total'];
                $qty = $_POST['qty'];
                $pDetails['price'] = (floatval($total) / floatval($qty));
                $pDetails['notes'] = "From invoice";
                // if the price is added
                if ($priceId = addPrice($pDetails)){
                    //then the next step is to add the supplier link
                    $msg .= "Price added.";
                    $sid = $_POST['suppId'];
                    //if it is assigned
                    if (assignSupplierToProduct($sid, $pid)){
                        //then add it to the invoice(productLeft)
                        $msg .= "Supplier selected.";
                        //invoiceId, productLeftId, qty
                        $invId = $_POST['invId'];
                        if (assignProductToInvoiceLeft($ccId, $invId, $pid, $qty, $priceId)){
                            $msg .= "Product added to Invoice.";
                        }else{
                            //invoice-link fail
                            $msg .= "Failed to link Product to Invoice.";
                            $good = false;
                        }
                    }else{
                        //supplier fail
                        $msg .= "Failed to link Supplier to Product!";   
                        $good = false;
                    }
                }else{
                    // price add fail
                    $msg .= "Failed to add Price for Product!";   
                    $good = false;
                }
                
            }else{
                //if product failed to add then exit
                setErrorMsg($answer);
            }
        //if product is in the db then:
        //steps: add new price for THIS product, link product to invoice
        }else{
            $pid = $_POST['inDb'];
            $total = $_POST['total'];
            $qty = $_POST['qty'];
            $details = array();
            $details['pid'] = $pid;
            $details['price'] = (floatval($total) / floatval($qty));
            $details['notes'] = "Update from invoice";
            //details expects: pid, price, notes
            if ($priceId = updatePrice($details)){
                //if price is updated then link it to the invoice
                $msg = "Price updated.";
                $invId = $_POST['invId'];
                if (assignProductToInvoiceLeft($ccId, $invId, $pid, $qty, $priceId)){
                    $msg .= "Product added to Invoice.";
                }else{
                    //invoice-link fail
                    $msg .= "Failed to link Product to Invoice.";
                    $good = false;
                }   
            }else{
                //price update failed
                $msg .= "Failed to update Price for this product.";
                $good = false;
            }        
        }
        
        
            
        if ($good == true){
            setMsg($msg);
        }else{
            setErrorMsg($msg);    
        }
        $response = array();
        $response['good'] = $good;
        $response['msg'] = $msg;
        
        echo json_encode($response);
        
        
        //redirecting for direct POSTs
        redirect_user('../../ccinvoice.php?ccId=' . $_POST['ccId'] . '&invId=' . $_POST['invId']);
    }
    
    
    //handle invoice finish requests
    
    if (isset($_POST['finishStatus'])){
        $status = $_POST['finishStatus'];
        $invId = $_POST['invId'];     
        if (setInvoiceStatus($invId, $status)){
            echo json_encode("done");    
        }else{
            echo json_encode("fail");   
        }
        
    }
    
    //handle product details update requests
    if (isset($_POST['eprodCode'])){
        //expected submitted info:  prodLeftId, priceLeftId, pilId, etotal, eprodPack, eprodDesc,eprodCode, eqty
        $details = array();
        $details['pid'] = $_POST['prodLeftId'];
        $priceId = $_POST['priceLeftId'];
        $pilId = $_POST['pilId'];
        $total = $_POST['etotal'];
        $qty = $_POST['eqty'];
        $details['pack'] = $_POST['eprodPack'];
        $details['desc'] = $_POST['eprodDesc'];
        $details['code'] = $_POST['eprodCode'];
        //expected by updateProduct: pid, code, desc, pack
        list($updated, $msg) = updateProduct($details);
        //if the product updated
        if ($updated){
            $endMsg = $msg;
            // editPrice expects: $pid, $price, $note
            $price = (floatval($total)) / (floatval($qty));
            $note = "Edited from Invoice Manager.";
            list($edited, $msg) = editPrice($priceId, $price, $note);
            if ($edited){
                $endMsg .= $msg;
                list($success, $msg) = updateProductInvoiceLink($pilId, $qty);
                if ($success){
                    $endMsg .= $msg;
                }else{
                    //if pil not updated
                    setErrorMsg($msg);
                }
            }else{
                //if price could not be updated
                setErrorMsg($msg);
            }          
        }else{
            //product could not be updated
            setErrorMsg($msg);
        }
        setMsg($endMsg);
        redirect_user('../../ccinvoice.php?invId=' . $_POST['invId'] . '&ccId=' . $_POST['ccId']);
        
    }
    
    
    // handle product removal from invoice (delete record in productinvoicelink)
    if (isset($_POST['did'])){
        if (deleteProductInvoiceLink($_POST['did'])){
            setMsg('Product removed from invoice.');   
        }else{
            setErrorMsg('Failed to remove product from invoice.');
        }
        redirect_user('../../ccinvoice.php?invId=' . $_POST['invId'] . '&ccId=' . $_POST['ccId']);
        
    }
    
    //handle setDetails for invoice
    if (isset($_POST['action'])){
        $action = $_POST['action'];
        if ($action == 'setDetails'){
            //expects in details: date, suppId, invId
            $details = array();
            $details['invId'] = $_POST['invId'];
            $details['suppId'] = $_POST['suppId'];
            $details['date'] = $_POST['date'];
            if (setInvoiceDetails($details)){
                setMsg('Invoice details are set.');
            }else{
                setErrorMsg('Failed to set invoice details.');   
            }
            redirect_user('../../ccinvoice.php?invId=' . $_POST['invId'] . '&ccId=' . $_POST['ccId']);
         }
        
        if ($action == 'resetDetails'){
            $id = $_POST['invId'];
            if (resetInvoiceDetails($id)){
                 echo json_encode("done");   
            }else{
                 echo json_encode("fail");  
            }
        }
        
        if ($action == 'updateNote' || $action == 'addNote'){
            $id = $_POST['invId'];
            $note = $_POST['note'];
            if (updateInvoiceNote($id, $note)){
                echo json_encode("done");
            }else{
                echo json_encode("fail");   
            }
            
        }
        
        if ($action == 'delNote'){
            $id = $_POST['invId'];
            if (resetInvoiceNote($id)){
                echo json_encode("done");
            }else{
                echo json_encode("fail");   
            }
        }
        
        
        
         
     }
    
         
}




?>