<?php //core app functions
include(dirname(__FILE__) . '/dbconnect.php');
// this file contains classes and functions for the OneCost webapp


//functions for the OneCost app


// html shortcuts for dynamic stuff

// USAGE: call it in your DIV. Add Label if needed in html
// details: [sid] = id/name, [table] = table name, [col] = column to show, [valCol] = value for columns,
function createSelect($details){
    echo '<select class="form-control" name="'. $details['sid'] . '" id="'. $details['sid'] . '">';
	$results = getColFromTable($details['table'], $details['col'], $details['valCol']);
   // print_r($results);
	if ($results != false){
		while ($res = $results->fetch())
		{
			//echo 'in foreach';
			echo '<option value="'. $res[1] .'">';
			echo $res[0];
			echo '</option>';
		}
		echo '</select>';
	}else{
		echo 'No results were found.';
		echo '</select>';	
	}
    
}

function createSmallSelect($details){
    echo '<select class="form-control input-sm" name="'. $details['sid'] . '" id="'. $details['sid'] . '">';
	$results = getColFromTable($details['table'], $details['col'], $details['valCol']);
   // print_r($results);
	if ($results != false){
		foreach ($results as $res)
		{
			//echo 'in foreach';
			echo '<option value="'. $res[1] .'">';
			echo $res[0];
			echo '</option>';
		}
		echo '</select>';
	}else{
		echo 'No results were found.';
		echo '</select>';	
	}
    
}
    
    
//whole site usage functions


function reloadMe(){
echo '<script type="text/javascript">
                            location.reload();
                            </script>';
}

// redirect function for general use
function redirect_user($page = 'index.php') {

	// Start defining the URL...
	// URL is http:// plus the host name plus the current directory:
	$url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
	
	// Remove any trailing slashes:
	$url = rtrim($url, '/\\');
	
	// Add the page:
	$url .= '/' . $page;
	
	// Redirect the user:
	header("Location: $url");
	exit(); // Quit the script.

} // End of redirect_user() function.


//message/error message (alert) setters and reset
function setErrorMsg($msg){
	$_SESSION['errormsg'] = $msg;
}

function setMsg($msg){
	$_SESSION['msg'] = $msg;
}
function resetMsg(){
	unset($_SESSION['errormsg']);
	unset($_SESSION['msg']);	
}

//whole site usage functions end





//general DB functions

//the function returns 2 columns from the table. It is useful when we want to use 1 column for naming, and another for the value
function getColFromTable($table, $col, $valueCol, $where = ''){
	$db = connectDb();
	$sql = "SELECT $col, $valueCol FROM $table $where";
	$result = $db->query($sql);
	$db = null;
	return $result;
}

// returns true if the table empty, false if the table has records in
//!!!!!!!rowCount does not seem to work properly!!!!!!
function isTableEmpty($table){
	$db = connectDb();
	$sql = "SELECT * FROM $table";
	$result = $db->query($sql);
	$counter = $result->rowCount();
	$db = null;
	if ($counter > 0){
		return false;	
	}else{
		return true;	
	}
	
}




//general DB functions end






//registration and login functions


/* This function validates the data (the email address and password).
 * If both are present, the database is queried.
 * The function requires a database connection.
 * The function returns an array of information, including:
 * - a TRUE/FALSE variable indicating success
 * - an array of either errors or the database result
 */

function check_login($email = '', $pass = '') {
	$db = connectDb();
	
	$errors = array(); // Initialize error array.

	// Validate the email address:
	if (empty($email)) {
		$errors[] = 'You forgot to enter your email address.';
	} else {
		$e = $email;
	}

	// Validate the password:
	if (empty($pass)) {
		$errors[] = 'You forgot to enter your password.';
	} else {
		$p = sha1($pass);
	}

	if (empty($errors)) { // If everything's OK.

		// Retrieve the UserID and UserName for that email/password combination:
		$sqlString = "SELECT Id, Firstname, Surname, Enabled, AccessLevel FROM user 
		WHERE Email = :e AND Passwd = :p";
		$stmt = $db->prepare($sqlString);
		$stmt->bindParam(':e', $e, PDO::PARAM_STR);
		$stmt->bindParam(':p', $p, PDO::PARAM_STR);			
		if ($stmt->Execute()){
			$stmt->setFetchMode(PDO::FETCH_ASSOC);		
		}else{
			exit("Login failed.");	
		}
		
		// Check the result:
		if ($row = $stmt->fetch()){
			// close db	
			$db = null;
			
            //!!!!!!!!!!!!!new!!!!!!!!!!!!!
            //check for enabled status on user record
            if ($row['Enabled'] > 0){
			//print_r($row); //debug
			// Return true and the record:
            $db = null;
			return array(true, $row);
            }else{
                $errors[] =  "Your account is not activated. Please contact an admin.";
                return array(false, $errors);
            }
            
		} else { // Not a match!
			$errors[] = "The email address and password entered do not match those on file.";
		}
		
	} // End of empty($errors) IF.
	
	// close db	
	$db = null;
   
	// Return false and the errors:
	return array(false, $errors);

} // End of check_login() function.


// registration helper function to check if email is already in user table
function isEmailInDb($email){
	$db = connectDb();
	$sqlString = "SELECT COUNT(*) FROM user WHERE Email = :e";
	$stmt = $db->prepare($sqlString);
	$stmt->bindParam(':e', $email, PDO::PARAM_STR);		
	$stmt->Execute();
	$row = $stmt->fetchColumn();
	$db = null;
	if ($row){
		return true;
	}else{
		return false;
	}
}


//registration and login end




//roles functions

//returns all the roles from the DB
function getAllRoles(){
    $db = connectDb();
    $sql = "SELECT * FROM roles";
	$stmt = $db->query($sql);	
	return $stmt;
}
//get role by id
function getRole($id){
    $db = connectDb();
    $sql = "SELECT * FROM roles WHERE Id = $id";
	$stmt = $db->query($sql);
    $db = null;
	return $stmt;    
}

function getRolesByName($name){
    $db = connectDb();
    $n = '%' .$name . '%';
    $sql = "SELECT * FROM roles WHERE Name LIKE '$n'";
	$stmt = $db->query($sql);	
    $db = null;
	return $stmt;    
}

//add role
function addRole($details){
    $db = connectDb();
    $sql = "INSERT INTO roles (Name, Description) VALUES (:name, :desc)";
	$stmt = $db->prepare($sql);
	$stmt->bindParam(':name', $details['name'], PDO::PARAM_STR);
    $stmt->bindParam(':desc', $details['desc'], PDO::PARAM_STR);
	if ($stmt->Execute()){
        return true;   
    }else{
        return false;   
    }   
}

function deleteRole($id){
    $db = connectDb();
    $sql = "DELETE FROM roles WHERE Id = $id";
	$stmt = $db->query($sql);
    if ($stmt->rowCount() > 0){
        return true;   
    }else{
        return false;   
    }
}
//expects: name, desc, id
function updateRole($details){
    $db = connectDb();
    $stmt = $db->prepare("UPDATE roles SET Name = :name, Description = :desc WHERE Id = :id");
	//bind paramaters for security
    $stmt->bindParam(':id', $details['id'], PDO::PARAM_INT);
	$stmt->bindParam(':name', $details['name'], PDO::PARAM_STR);
	$stmt->bindParam(':desc', $details['desc'], PDO::PARAM_STR);
	if ($stmt->execute()){
		$db = null;
        //return true if it is done
        return true;
	}else{
	//close db connection
	$db = null;
	//return false if failed
	return false;
	}    
}


// roles end








//invoice related functions


//add invoice

// details: ccId, img, date
// status should be 0 on creation

function addInvoice($details){
    $db = connectDb();
    $sqlString = "INSERT INTO invoice (CcId, ImageName, Status) VALUES (:ccid, :img, 0)";
    $stmt = $db->prepare($sqlString);
	$stmt->bindParam(':ccid', $details['ccId'], PDO::PARAM_INT);
	$stmt->bindParam(':img', $details['img'], PDO::PARAM_STR);			
	if ($stmt->Execute()){
        return true;    
    }else{
        return false;   
    }
	//return $stmt;
}

//get all invoice id in CC
function getAllInvoiceId($ccId){
    $db = connectDb();
	$sql = "SELECT Id FROM invoice WHERE CcId = $ccId";
	$result = $db->query($sql);
	$db = null;
	return $result;	

}

//get all invoice in CC
function getAllInvoice($ccId){
    $db = connectDb();
	$sql = "SELECT * FROM invoice WHERE CcId = $ccId";
	$result = $db->query($sql);
	$db = null;
	return $result;	
}

//get all invoice in CC with Supplier name
function getAllInvoiceWithSupplier($ccId){
    $db = connectDb();
	$sql = "SELECT i.*, s.Name FROM invoice AS i
            LEFT JOIN supplier AS s ON s.Id = i.SupplierId
            WHERE CcId = $ccId";
	$result = $db->query($sql);
	$db = null;
	return $result;	
}


function getInvoice($id){
    $db = connectDb();
	$sql = "SELECT i.*, s.Name FROM invoice AS i
            LEFT JOIN supplier AS s ON s.Id = i.SupplierId
            WHERE i.Id = $id";
	$result = $db->query($sql);
	$db = null;
	return $result;	
}

// dont think we need this
// details: invId, ccId, img, date, status
function updateInvoice($details){
    $db = connectDb();
    $sqlString = "UPDATE invoice SET CcId = :ccid, ImageName = :img, InvoiceDate = :date, Status = :status WHERE Id = :id";
    $stmt = $db->prepare($sqlString);
    $stmt->bindParam(':id', $details['invId'], PDO::PARAM_INT);
	$stmt->bindParam(':ccid', $details['ccId'], PDO::PARAM_INT);
	$stmt->bindParam(':date', $details['date'], PDO::PARAM_STR);			
	$stmt->bindParam(':img', $details['img'], PDO::PARAM_STR);
    $stmt->bindParam(':status', $details['status'], PDO::PARAM_INT);			
	if ($stmt->Execute()){
        return true;    
    }else{
        return false;   
    }  
}

function updateInvoiceNote($id, $note){
    $db = connectDb();
    $sqlString = "UPDATE invoice SET Notes = :note WHERE Id = :id";
    $stmt = $db->prepare($sqlString);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
	$stmt->bindParam(':note', $note, PDO::PARAM_STR);
	if ($stmt->Execute()){
        return true;    
    }else{
        return false;   
    }
}

function resetInvoiceNote($id){
    $db = connectDb();
    $sqlString = "UPDATE invoice SET Notes = NULL WHERE Id = :id";
    $stmt = $db->prepare($sqlString);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
	if ($stmt->Execute()){
        return true;    
    }else{
        return false;   
    }

}

//set details (used on Invoice page)
//expects in details: date, suppId, invId
function setInvoiceDetails($details){
    $db = connectDb();
    $sqlString = "UPDATE invoice SET InvoiceDate = :date, SupplierId = :sid WHERE Id = :id";
    $stmt = $db->prepare($sqlString);
    $stmt->bindParam(':id', $details['invId'], PDO::PARAM_INT);
	$stmt->bindParam(':sid', $details['suppId'], PDO::PARAM_INT);
	$stmt->bindParam(':date', $details['date'], PDO::PARAM_STR);			
	if ($stmt->Execute()){
        return true;    
    }else{
        return false;   
    }  
}

//reset invoice details to NULL
function resetInvoiceDetails($id){
    $db = connectDb();
    $sqlString = "UPDATE invoice SET InvoiceDate = NULL, SupplierId = NULL WHERE Id = :id";
    $stmt = $db->prepare($sqlString);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
	if ($stmt->Execute()){
        return true;    
    }else{
        return false;   
    }  
}

//sets the status of the invoice identified by Id
function setInvoiceStatus($id, $status){
    $db = connectDb();
    $sqlString = "UPDATE invoice SET Status = :status WHERE Id = :id";
    $stmt = $db->prepare($sqlString);
	$stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->bindParam(':status', $status, PDO::PARAM_INT);			
	if ($stmt->Execute()){
        return true;    
    }else{
        return false;   
    }

}

//delete an invoice from DB and also attempts to delete the file
//on failure it sets an error msg, but still deletes the record from DB!
function deleteInvoice($id){
    $db = connectDb();
    $sql = "SELECT ImageName, CcId FROM invoice WHERE Id = $id";
    $result = $db->query($sql);
    $imgDetails = $result->fetch();
    if (!deleteInvoiceImage($imgDetails)){
        setErrorMsg('Failed to delete image file!');
    }
	$sql = "DELETE FROM invoice WHERE Id = $id";
    $result = $db->query($sql);
    $db = null;
    return $result;   
}

// invoice image functions

function deleteInvoiceImage($details){
    //$myPath = realpath(null);
    $ccId = $details['CcId'];
    $imgName = $details['ImageName'];
    $imgPath = './images/invoices/' . $ccId . '/' .$imgName;
    $path = realpath($imgPath);
    //print_r($path);
    return unlink($path);
}

//invoice related functions end



//cc related functions


//update CC
// in details: (id), cname, status, prio, notes, createdAt, createdBy, modAt, modBy
function updateCC($details){
	$db = connectDb();
    $myId = $_SESSION['myId'];
    $sql = "UPDATE cc SET ClientId = :cid, Status = :status, Priority = :prio, Notes = :note, ModifiedAt = Now(), ModifiedBy = :me WHERE Id = :id";
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $details['id'], PDO::PARAM_INT);
	$stmt->bindParam(':cid', $details['cid'], PDO::PARAM_INT);
    $stmt->bindParam(':status', $details['status'], PDO::PARAM_INT);			
    $stmt->bindParam(':prio', $details['prio'], PDO::PARAM_INT);
    $stmt->bindParam(':note', $details['note'], PDO::PARAM_STR);
    $stmt->bindParam(':me', $myId, PDO::PARAM_INT);			
    if ($stmt->Execute()){
		$db = null;
		return true;
	}else{
		$db = null;
		return false;	
	}
}


//this function deletes a quote/CC from the DB.
//the function only need a ccId passed in. No security check yet!!!
//currently used through the handler -> possible security risk? needs more validation?
//working
function deleteCC($ccId){
    //also delete records from usercclink table!
    resetAssignedUsers($ccId);
	$db = connectDb();
	$sql = "DELETE FROM cc WHERE Id = $ccId";
	$result = $db->query($sql);
	$counter = $result->rowCount();
	$db = null;
	if ($counter > 0){
		return true;	
	}else{
		return false;	
	}
}


//the function adds a new record in the table (CC)
//the function uses session variables for some parameters for security? need confirming/ maybe default values instead
//so it is still possible to pass in these values too
// in details: (id), cname, status, prio, notes, createdAt, createdBy, modAt, modBy
//!!!!!the function returns the added CC ID!!!!
function addCC($details){
	$db = connectDb();
	$myId = $_SESSION['myId'];  // for security this should not be passed in, it should be already in session
		$sql = "INSERT INTO cc (ClientId, Status, Priority, Notes, CreatedAt, CreatedBy) VALUES (:cid, :status, :prio, :note, Now(), :me)";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(':cid', $details['cid'], PDO::PARAM_INT);
		$stmt->bindParam(':status', $details['status'], PDO::PARAM_INT);			
		$stmt->bindParam(':prio', $details['prio'], PDO::PARAM_INT);
		$stmt->bindParam(':note', $details['note'], PDO::PARAM_STR);
        $stmt->bindParam(':me', $myId, PDO::PARAM_INT);			
		if ($stmt->execute()){
            $currentId = $db->lastInsertId();
            $db = null;
        //return true if it is done
            if (createInvoiceDir($currentId)){
                return $currentId;    
            }else{
                setErrorMsg("Directory could not be created.");
                return false;   
            }
            
	}else{
	//close db connection
	$db = null;
	//return false if failed
	return false;
	}
}

function createInvoiceDir($id){
     return mkdir("images/invoices/$id");
}

//this function returns all the CC's from the db
function getAllCC(){
	$db = connectDb();
	$sql = "SELECT cc.*, c.Name, u.Firstname AS fn, u.Surname AS sn, u2.Firstname AS fn2, u2.Surname AS sn2 FROM cc 
    INNER JOIN client AS c ON c.Id = cc.ClientId 
    INNER JOIN user AS u ON u.Id = cc.CreatedBy
    LEFT JOIN user AS u2 ON cc.ModifiedBy = u2.Id";
	$result = $db->query($sql);
    $result->setFetchMode(PDO::FETCH_ASSOC);
	$db = null;
	return $result;	
}


//get cc by ccId
function getCCbyId($ccId){
	$db = connectDb();
	$sqlString = "SELECT cc.*, c.Name, u.Firstname AS fn, u.Surname AS sn, u2.Firstname AS fn2, u2.Surname AS sn2 FROM cc 
    INNER JOIN client AS c ON c.Id = cc.ClientId 
    INNER JOIN user AS u ON u.Id = cc.CreatedBy
    LEFT JOIN user AS u2 ON cc.ModifiedBy = u2.Id
    WHERE cc.Id = :ccid";
	$stmt = $db->prepare($sqlString);
	$stmt->bindParam(':ccId', $ccId, PDO::PARAM_INT);		
	$stmt->Execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$db = null;
	return $stmt;
}


//returns the filtered CC's. Filter so far: client name
// TODO : add the rest of the filters as well
function getCC($search){
	$db = connectDb();
	$prep = "%" . $search . "%";
	$sqlString = "SELECT cc.*, c.Name, u.Firstname AS fn, u.Surname AS sn, u2.Firstname AS fn2, u2.Surname AS sn2 FROM cc 
    INNER JOIN client AS c ON c.Id = cc.ClientId 
    INNER JOIN user AS u ON u.Id = cc.CreatedBy
    LEFT JOIN user AS u2 ON cc.ModifiedBy = u2.Id
    WHERE c.Name LIKE :cname";
	$stmt = $db->prepare($sqlString);
	$stmt->bindParam(':cname', $prep, PDO::PARAM_STR);		
	$stmt->Execute();
	$db = null;
	return $stmt;
}


//cc related functions end





//usercclink functions

function isUserAssigned($ccid, $userId){
    $db = connectDb();
    $sql = "SELECT Id FROM usercclink WHERE CcId = $ccId AND UserId = $userId";
    if ($result = $db->query($sql)){
        $db = null;
        return true;
    }
    $db = null;
    return false;

}

function getAssignedUsers($ccid){
    $db = connectDb();
    $sql = "SELECT u.Id, u.Firstname AS fname, u.Surname AS sname FROM user AS u INNER JOIN usercclink AS cc ON cc.UserId = u.Id WHERE cc.CcId = $ccid";
    $result = $db->query($sql);
    $db = null;
    return $result;
}

//remove all assigned users from the CC
function resetAssignedUsers($ccId){
    if ($ccId){
        $db = connectDb();
        $sql = "DELETE FROM usercclink WHERE CcId = $ccId";
        $result = $db->query($sql);
        $db = null;
        //return $result;
    }
}


//assign Users to CC. This function should check for already existing records and delete/update accordingly
//if details are passed in it should get rid of the existing links and add the new ones. 
// (or check/update only when needed?) maybe for later TODO!!
//expect: user Ids array, ccid
function assignUsersToCC($details = false){
    //print_r($details);
    if ($details != false){
        resetAssignedUsers($details['ccId']);
        $db = connectDb();
        $sql = "INSERT INTO usercclink (UserId, CcId) VALUES (:uid,:ccid)";
        $stmt = $db->prepare($sql);          
        foreach($details['userId'] as $id){
            $stmt->bindParam(':uid', $id, PDO::PARAM_INT);
            $stmt->bindParam(':ccid', $details['ccId'], PDO::PARAM_INT);
            $stmt->execute();
        }
        return true;
    }else{
        
        return false;
    }
}



// usercclink functions end




//user related functions



//add user
function addUser($details){
    $db = connectDb();
    $sqlString = "INSERT INTO user (Firstname, Surname, Email, Passwd, Enabled, AccessLevel) VALUES (:fn,:sn,:e,:p,:en,:ac)";
    $stmt = $db->prepare($sqlString);
    $stmt->bindParam(':e', $details['email'], PDO::PARAM_STR);
    $stmt->bindParam(':p', $details['pass'], PDO::PARAM_STR);			
	$stmt->bindParam(':fn', $details['fname'], PDO::PARAM_STR);
    $stmt->bindParam(':sn', $details['sname'], PDO::PARAM_STR);
    $stmt->bindParam(':en', $details['enabled'], PDO::PARAM_INT);
    $stmt->bindParam(':ac', $details['access'], PDO::PARAM_INT);
   if ($stmt->execute()){
		$db = null;
        //return true if it is done
        return true;
	}else{
	//close db connection
	$db = null;
	//return false if failed
	return false;
	}
}


// register user
function registerUser($fname, $sname, $email, $pass){
		$db = connectDb();
		$sqlString = "INSERT INTO user (Firstname, Surname, Email, Passwd) VALUES (:fn,:sn,:e,:p)";
		$stmt = $db->prepare($sqlString);
		$stmt->bindParam(':e', $email, PDO::PARAM_STR);
		$stmt->bindParam(':p', $pass, PDO::PARAM_STR);			
		$stmt->bindParam(':fn', $fname, PDO::PARAM_STR);	
        $stmt->bindParam(':sn', $sname, PDO::PARAM_STR);
		if ($stmt->execute()){
		$db = null;
        //return true if it is done
        return true;
	}else{
	//close db connection
	$db = null;
	//return false if failed
	return false;
	}
}


//this function returns the staff name /for internal use only?/
function getUserName($userId){
    $db = connectDb();
	$sql = "SELECT Surname, Firstname FROM user WHERE Id = $userId";
	$result = $db->query($sql);
	$db = null;
	return $result;	
    
}

// read user profile based on userId. Returns an array of details.
function getUser($userId){
	$db = connectDb();
	$sql = "SELECT * FROM user WHERE Id = $userId";
	$result = $db->query($sql);
	$db = null;
	return $result;
}

function getUsersByName($name){
    $db = connectDb();
    $name = '%' . $name . '%';
	$sql = "SELECT * FROM user WHERE Surname LIKE '$name' OR Email LIKE '$name' OR Firstname LIKE '$name'";
	$result = $db->query($sql);
	$db = null;
	return $result;
}

// delete a user based on userId. Returns true if successful, false if no deletion.
function deleteUser($userId){
	$db = connectDb();
	$sql = "DELETE FROM user WHERE Id = $userId";
	$result = $db->query($sql);
	$counter = $result->rowCount();
	$db = null;
	if ($counter > 0){
		return true;	
	}else{
		return false;	
	}
	
}


//This function is from the tutorial.
function getAllUser(){
    //connect to the database
    $db = connectDb();    
    //sql query as string
    $sql = "SELECT * FROM user";    
    $result = $db->query($sql);
    $db = null;    
    return $result;
}


// update user details
//this function does not change the user's password.
//expects: id, fname, sname, email, enabled, access
//!!this function should be used everywhere apart from when the user updates own settings. For that use the one below. !!
function updateUser($details){
    $db = connectDb();
    $sqlString = "UPDATE user SET Firstname = :fn, Surname = :sn, Email = :e, Enabled = :en, AccessLevel = :ac WHERE Id = :id";
    $stmt = $db->prepare($sqlString);
    $stmt->bindParam(':id', $details['id'], PDO::PARAM_INT);
    $stmt->bindParam(':e', $details['email'], PDO::PARAM_STR);
	$stmt->bindParam(':fn', $details['fname'], PDO::PARAM_STR);
    $stmt->bindParam(':sn', $details['sname'], PDO::PARAM_STR);
    $stmt->bindParam(':en', $details['enabled'], PDO::PARAM_INT);
    $stmt->bindParam(':ac', $details['access'], PDO::PARAM_INT);
    if ($stmt->execute()){
		$db = null;
        //return true if it is done
        return true;
	}else{
	//close db connection
	$db = null;
	//return false if failed
	return false;
	}  
}


//update user details. Details array: fname, sname, email, pass, sql.
//!! This function is specially for the user for own profile editing. Limited properties... !!
function updateUserDetails($details, $userId){

    //get db
    $db = connectDb();
    // in details we have created the 1st part of the query
    $sql = $details['sql'];
    //add WHERE part to the query. userId is from session or specified (for admin use only).
    $sql .= " WHERE Id = $userId";
    $stmt = $db->prepare($sql);
    
    //do the bindings based on what is in $details
    
    //for fname
    if (isset($details['fname'])){
        $stmt->bindParam(':fn', $details['fname'], PDO::PARAM_STR);	
    }
    //for sname
    if (isset($details['sname'])){
        $stmt->bindParam(':sn', $details['sname'], PDO::PARAM_STR);	
    }
    
    //for email
    if (isset($details['email'])){
        $stmt->bindParam(':em', $details['email'], PDO::PARAM_STR);
    }
    
    
    if ($stmt->Execute()){
        //update session variables
        if (isset($details['fname'])){
            $_SESSION['fname'] = $details['fname'];
        }
        if (isset($details['sname'])){
            $_SESSION['sname'] = $details['sname'];
        }
        return true;
            
    }else{
        return false;
    }
       
}
//change the password for a user
function setUserPass($id, $pass){
    $myPass = sha1($pass);
    $db = connectDb();
	$sql = "UPDATE user SET Passwd = :pass WHERE Id = :id";
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->bindParam(':pass', $myPass, PDO::PARAM_STR);
	if ($stmt->Execute()){
        $db = null;
        return array(true, "Password changed.");
    }else{
        $db = null;
        return array(false, "Failed to change password.");
    }
	
	
}

//generates a new random password and sends it to the user's email address.
function resetUserPass($id){
    $db = connectDb();
    $sql = "SELECT Email FROM user WHERE Id = :id";
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->Execute();
    $email = $stmt->fetch();
    $email = $email[0];
    $db = null;
    $randomPass = substr(md5(rand()), 0, 5);
    list($done, $msg) = setUserPass($id, $randomPass);
    if ($done == true){
        if (sendPassEmail($email, $randomPass)){
            $details['done'] = $done;
            $details['msg'] = $msg . " Email sent to the user.";
            //$details['msg'] = $msg . " Email sent to the user.Email:" . $email . " Pass:" . $randomPass . " id:" . $id;           
        }else{
            $details['done'] = false;
            $details['msg'] = "Password changed but Email could not be sent!";
            //$details['msg'] = $msg . " No email? .Enail:" . $email . " Pass:" . $randomPass . " id:" . $id;           
        }
    }else{
        $details['done'] = $done;
        $details['msg'] = $msg;           
        //$details['msg'] = $msg . " Error! Email:" . $email . " Pass:" . $randomPass . " id:" . $id;           
     
    }
    return $details;
}


function sendPassEmail($email, $pass){
    $to      = $email;
    $subject = 'Password reset';
    $message = "Dear User\r\nYour password is now set to a random password.\r\nYour new password is: ". $pass ."\r\nYou can log in and change this password anytime in the future.\r\nHave a nice day!\r\n\r\nOne Cost development team";
    $headers = 'From: noreply@onecostdev.com' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
    //send the mail
    $done = mail($to, $subject, $message, $headers);
    return $done;
}


function getUserPass($id){
    $db = connectDb();
    $sql = "SELECT Passwd FROM user WHERE Id = $id";
    $result = $db->query($sql);
    $db = null;
    return $result;
}

// user related end





//supplier related functions


//get all suppliers
function getAllSupplier(){
	$db = connectDb();
	$sql = "Select * from supplier";
	$result = $db->query($sql);
	$db = null;
	return $result;
}

// get suppliers based on search criteria, from name
function getSuppliersByName($searchFor){
	$db = connectDb();
	$prepSearchFor = "%" . $searchFor . "%";
	$stmt = $db->prepare("SELECT * FROM supplier WHERE Name LIKE :suppName");
	$stmt->bindParam(':suppName', $prepSearchFor, PDO::PARAM_STR);
	$stmt->Execute();
	//$result = $db->query($sql);
	$db = null;
	return $stmt;	
}

function getSupplier($id){
    $db = connectDb();
	$sql = "SELECT * FROM supplier WHERE Id = $id";
	$result = $db->query($sql);
	$db = null;
	return $result;
}


//add new supplier 
//expects $detail array with parameter: sname, sdesc, snote, stype
function addSupplier($details){

    //check for alredy existing supplier before you call this, as because of the true/false return it is not obvious to do here
    //use the commented out code below for example
    
    /*
    //check if supplier already in DB
    
    $checker = getSuppliersByName($details['sname']);
    if (!empty($supplier = $checker->fetch())){
        setErrorMsg('Supplier is in Db already!');
        return false;   
    }
    */
    
    //connect to the db
	$db = connectDb();
	//prepare the sql statement
	$stmt = $db->prepare("INSERT INTO supplier (Name, Description, Notes, Type) VALUES (:sname, :sdesc, :snote, :stype)");
	//bind paramaters for security
	$stmt->bindParam(':sname', $details['sname'], PDO::PARAM_STR);
	$stmt->bindParam(':sdesc', $details['sdesc'], PDO::PARAM_STR);
	$stmt->bindParam(':snote', $details['snote'], PDO::PARAM_STR);
    $stmt->bindParam(':stype', $details['stype'], PDO::PARAM_INT);
	if ($stmt->execute()){
		$db = null;
        //return true if it is done
        return true;
	}else{
	//close db connection
	$db = null;
	//return false if failed
	return false;
	}
}

//deletes a supplier, expects supplier Id
function deleteSupplier($id){
    $db = connectDb();
	$stmt = $db->prepare("DELETE FROM supplier WHERE Id = :id");
	$stmt->bindParam(':id', $id, PDO::PARAM_INT);
	if ($stmt->Execute()){
		$db = null;
		return true;
	}else{
		$db = null;
		return false;	
	}
}

//update supplier details. Expects sid, sname, sdesc, snote and stype in details array
function updateSupplier($details){
    $db = connectDb();
	//prepare the sql statement
	$stmt = $db->prepare("UPDATE supplier SET Name = :sname, Description = :sdesc, Notes = :snote, Type = :stype WHERE Id = :sid");
	//bind paramaters for security
    $stmt->bindParam(':sid', $details['sid'], PDO::PARAM_INT);
	$stmt->bindParam(':sname', $details['sname'], PDO::PARAM_STR);
	$stmt->bindParam(':sdesc', $details['sdesc'], PDO::PARAM_STR);
	$stmt->bindParam(':snote', $details['snote'], PDO::PARAM_STR);
    $stmt->bindParam(':stype', $details['stype'], PDO::PARAM_INT);
	if ($stmt->execute()){
		$db = null;
        //return true if it is done
        return true;
	}else{
	//close db connection
	$db = null;
	//return false if failed
	return false;
	}
}
//supplier related end



//productsupplier-link functions

// assign supplier to product
//expects: supplier id, product id
function assignSupplierToProduct($sid, $pid){
    $db = connectDb();    
    //sql query as string
    $sql = "INSERT INTO productsupplierlink (ProductId, SupplierId) SELECT :pid, :sid";
    $stmt = $db->prepare($sql);  
    $stmt->bindParam(':pid', $pid, PDO::PARAM_INT);
    $stmt->bindParam(':sid', $sid, PDO::PARAM_INT);
    if ($stmt->Execute()){
        return true;   
    }else{
        return false;   
    }
}


//productsupplier-link functions END



//productinvoicelink functions


// expects: ccId, InvoiceId, ProductLeft (id), Qty, priceId
function assignProductToInvoiceLeft($ccId, $invId, $pid, $qty, $priceId){
    $db = connectDb();    
    //check if this product is already in this invoice
    //if it is in the invoice with the same price then just add the qty to the existing record
    $sql = "SELECT pil.Id, pil.Qty FROM productinvoicelink AS pil
            INNER JOIN invoice AS i ON i.Id = pil.InvoiceId
            WHERE i.Id = $invId
            AND pil.ProductIdLeft = $pid
            AND pil.PriceIdLeft = $priceId";
    $result = $db->query($sql);
    $alreadyIn = $result->fetch();
    if (isset($alreadyIn['Id'])){
        $qty += $alreadyIn['Qty'];
        list($answer, $msg) = updateProductInvoiceLink($alreadyIn['Id'], $qty);
        if ($answer == true){
            $db = null;
            return true;   
        }else{
            $db = null;
            return false;   
        }
    }
    // if it is not in the invoice or with a different price then add a new record
    $sql = "INSERT INTO productinvoicelink (InvoiceId, ProductIdLeft, Qty, PriceIdLeft) SELECT :invId, :pid, :qty, :price";
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':invId', $invId, PDO::PARAM_INT);
    $stmt->bindParam(':pid', $pid, PDO::PARAM_INT);
    $stmt->bindParam(':qty', $qty, PDO::PARAM_STR);
    $stmt->bindParam(':price', $priceId, PDO::PARAM_INT);
    if ($stmt->Execute()){
        return $db->lastInsertId(); 
    }else{
        $db = null;
        return false;   
    }
}


//update qty (the only property that can be changed, the others are a big web, easeier to delete record and add a new for that)
function updateProductInvoiceLink($pilId, $qty){
    $db = connectDb();
	//prepare the sql statement
    $sql = "UPDATE productinvoicelink SET Qty = :qty WHERE Id = :id";
	$stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $pilId, PDO::PARAM_INT);
	$stmt->bindParam(':qty', $qty, PDO::PARAM_INT);
	if ($stmt->execute()){
        $db = null;
        return array(true, "Invoice link updated.");   
    }else{
        $db = null;
        return array(false, "Failed to update invoice link.");   
    }
}

//delete an InvoiceLink
function deleteProductInvoiceLink($pilId){
    $db = connectDb();
	$sql = "DELETE FROM productinvoicelink WHERE Id = $pilId";
	if ($result = $db->query($sql)){
        if ($result->rowCount() == 1){
            $db = null;
            return true;   
        }
    }else{
        $db = null;
        return false;   
    }
    $db = null;
    return false;   
}

//get product Id from pil

function getProductIdFromPil($pilId){
    $db = connectDb();
	$sql = "SELECT ProductIdLeft FROM productinvoicelink WHERE Id = $pilId";
	$result = $db->query($sql);
    $db = null;
    return $result; 
}

// pil end



//productmatch functions

function checkMatch($pid, $pilId){
    $db = connectDb();
	$sql = "SELECT COUNT(Id) FROM productmatch WHERE InvoiceLinkId = $pilId AND ProductId = $pid";
	$result = $db->query($sql);
    $inDb = $result->fetch();
	$db = null;
    //return the actual count
	return $inDb;
}

function getProductMatch($pilId){
    $db = connectDb();
	$sql = "SELECT pm.*, pm.Id AS pmId, p.Description, p.Packsize, s.Name AS suppName, price.Price
            FROM productmatch AS pm
            INNER JOIN product AS p ON pm.ProductId = p.Id
            INNER JOIN productsupplierlink AS psl ON psl.ProductId = p.Id
            INNER JOIN supplier AS s ON psl.SupplierId = s.Id
            INNER JOIN price ON price.Id = pm.PriceId 
            WHERE pm.InvoiceLinkId = $pilId";
	$result = $db->query($sql);
	$db = null;
	return $result;
}

//expects: InvoiceLinkId, productId, priceId, discount (final?)
function setProductMatch($details){
    $db = connectDb();
    $sql = "INSERT INTO productmatch (InvoiceLinkId, ProductId, PriceId) VALUES (:pilId, :prodId, :priceId)";
	$stmt = $db->prepare($sql);
	$stmt->bindParam(':pilId', $details['pilId'], PDO::PARAM_INT);
	$stmt->bindParam(':prodId', $details['prodId'], PDO::PARAM_INT);
    $stmt->bindParam(':priceId', $details['priceId'], PDO::PARAM_INT);
	if ($stmt->execute()){
		$db = null;
        //return true if it is done
        return true;
	}else{
        //close db connection
        $db = null;
        //return false if failed
        return false;
	}
    
}

//expects: ProductInvoiceLinkId
function resetAllProductMatch($pilId){
    $db = connectDb();
    $sql = "DELETE FROM productmatch WHERE InvoiceLinkId = $pilId";
	if ($stmt = $db->query($sql)){
        return true;       
    }else{
        return false;      
    }
}

//remove a match
function removeProductMatch($pmId){
    $db = connectDb();
    $sql = "DELETE FROM productmatch WHERE Id = $pmId";
	$stmt = $db->query($sql);
    if ($stmt->rowCount() > 0){
        return true;   
    }else{
        return false;   
    }  
}


//expects: pmId
//resets Final to 0 on each matched products then set the new Final
//only 1 should be Final
function setMatchFinal($pmId){
    $db = connectDb();
    //1st reset the final on these matches
    $sql = "SELECT InvoiceLinkId FROM productmatch WHERE Id = $pmId";
    $res = $db->query($sql);
    $res = $res->fetch();
    $pilId = $res[0];
    $sql = "UPDATE productmatch SET Final = 0 WHERE InvoiceLinkId = $pilId";
    $db->query($sql);
    //then set the new final
    $sql = "UPDATE productmatch SET Final = 1 WHERE Id = $pmId";    
    if ($db->query($sql)){
        $db = null;
        return true;
    }else{
        $db = null;
        return false;
    }
}


function getSuggestedProducts($pid){
    $db = connectDb();
	$sql = "
        SELECT pm.Id AS pmId, p.Id AS prodId, p.Description, p.Packsize, p.ProductCode, s.Name, price.Price, price.DateFrom, price.Id AS priceId 
        FROM productmatch AS pm
        INNER JOIN productinvoicelink as pil ON pil.Id = pm.InvoiceLinkId
        INNER JOIN product AS p ON pm.ProductId = p.Id
        INNER JOIN productsupplierlink AS psl ON p.Id = psl.ProductId
        INNER JOIN supplier AS s ON psl.SupplierId = s.Id
        INNER JOIN price ON price.ProductId = p.Id
        WHERE s.Type NOT IN (1) 
        AND price.DateTo is null 
        AND p.Deleted = 0
        AND pil.ProductIdLeft = $pid
        GROUP BY p.Id
        ";
	$result = $db->query($sql);
	$db = null;
	return $result;


}

//productmatch functions end







//product related functions




//get products by ccId
function getProductIdInCC($ccId){
    //get all invoice in this cc
    $myProducts = array();
    $db = connectDb();
	$sql = "SELECT Id FROM invoice WHERE CcId = $ccId";
	$invoices = $db->query($sql);
    while($invoice = $invoices->fetch()){
        //get all product ID's from link table
        $sql = "SELECT ProductIdLeft FROM productinvoicelink WHERE InvoiceId = :invId";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':invId', $invoice['Id'], PDO::PARAM_INT);
        $stmt->Execute();
        while($product = $stmt->fetch(PDO::FETCH_NUM)){
            //get all products
            $myProducts[] = $product[0];
        }
    }
    return $myProducts;   
}

// get all product from an invoice
function getProductInInvoice($invId){
    $db = connectDb();
	$sql = "SELECT pil.*, pil.Id AS pilId, p.*, s.Name, pr1.Price AS PriceLeft 
            FROM productinvoicelink AS pil
            INNER JOIN product AS p ON pil.ProductIdLeft = p.Id
            INNER JOIN productsupplierlink AS psl ON psl.ProductId = pil.ProductIdLeft
            INNER JOIN supplier AS s ON psl.SupplierId = s.Id
            INNER JOIN price AS pr1 ON pil.PriceIdLeft = pr1.Id
            WHERE pil.InvoiceId = $invId
            ";
	$result = $db->query($sql);
    return $result;
}

// get all product from an invoice
function getProductInInvoiceByCC($ccId){
    $db = connectDb();
	$sql = "SELECT pil.*, pil.Id AS pilId, p.*, s.Name, pr1.Price AS PriceLeft 
            FROM productinvoicelink AS pil
            INNER JOIN product AS p ON pil.ProductIdLeft = p.Id
            INNER JOIN productsupplierlink AS psl ON psl.ProductId = pil.ProductIdLeft
            INNER JOIN supplier AS s ON psl.SupplierId = s.Id
            INNER JOIN price AS pr1 ON pil.PriceIdLeft = pr1.Id
            INNER JOIN invoice ON invoice.Id = pil.InvoiceId
            WHERE invoice.CcId = $ccId
            GROUP BY p.Id
            ";
	$result = $db->query($sql);
    return $result;
}

//get a product by Id
function getProduct($id){
	$db = connectDb();
	$sql =   "SELECT p.*, p.Id AS prodId, s.Name, price.Price FROM product AS p 
                    INNER JOIN productsupplierlink AS psl ON p.Id = psl.ProductId
                    INNER JOIN supplier AS s ON psl.SupplierId = s.Id
                    INNER JOIN price ON price.ProductId = p.Id
                    WHERE p.Id = $id";
    $result = $db->query($sql);
	$db = null;
	return $result;
}

// type : 0:(0,2:onecost), 1:client
function getAllProductByType($type){
    $db = connectDb();
    if ($type == 0){
        $sqlString =   
            "SELECT p.Id AS prodId, p.Description, p.Packsize, s.Name, price.Price, price.DateFrom, price.Id AS priceId 
            FROM product AS p 
            INNER JOIN productsupplierlink AS psl ON p.Id = psl.ProductId
            INNER JOIN supplier AS s ON psl.SupplierId = s.Id
            INNER JOIN price ON price.ProductId = p.Id
            WHERE s.Type NOT IN (1) AND price.DateTo is null";
    }else{
        $sqlString =   
            "SELECT p.Id AS prodId, p.Description, p.Packsize, s.Name, price.Price, price.DateFrom, price.Id AS priceId 
            FROM product AS p 
            INNER JOIN productsupplierlink AS psl ON p.Id = psl.ProductId
            INNER JOIN supplier AS s ON psl.SupplierId = s.Id
            INNER JOIN price ON price.ProductId = p.Id
            WHERE s.Type = 1 AND price.DateTo is null";
    }
	$stmt = $db->prepare($sqlString);
	$stmt->Execute();
	$db = null;
	return $stmt;

}

//get OneCost products by description
function getAllOcProductByDesc($desc, $pilId){
    $db = connectDb();
    $desc = '%' . $desc . '%';
    $sqlString =   
        "SELECT p.Id AS prodId, p.Description, p.Packsize, p.ProductCode, s.Name, price.Price, price.DateFrom, price.Id AS priceId 
        FROM product AS p 
        INNER JOIN productsupplierlink AS psl ON p.Id = psl.ProductId
        INNER JOIN supplier AS s ON psl.SupplierId = s.Id
        INNER JOIN price ON price.ProductId = p.Id
        WHERE s.Type NOT IN (1) 
        AND price.DateTo is null 
        AND p.Deleted = 0 
        AND p.Id NOT IN (SELECT ProductId FROM productmatch WHERE InvoiceLinkId = :pilId)
        AND p.Description LIKE :desc
        ";
    $stmt = $db->prepare($sqlString);
    $stmt->bindParam(':desc', $desc, PDO::PARAM_STR);
    $stmt->bindParam(':pilId', $pilId, PDO::PARAM_INT);
	if ($stmt->Execute()){
        $db = null;
        return $stmt;
    }else{
        $db = null;
        return false;   
    }
}


//get OneCost products by ProductCode
function getAllOcProductByCode($code, $pilId){
    $db = connectDb();
    $code = '%' . $code . '%';
    $sqlString =   
        "SELECT p.Id AS prodId, p.Description, p.Packsize, p.ProductCode, s.Name, price.Price, price.DateFrom, price.Id AS priceId 
        FROM product AS p 
        INNER JOIN productsupplierlink AS psl ON p.Id = psl.ProductId
        INNER JOIN supplier AS s ON psl.SupplierId = s.Id
        INNER JOIN price ON price.ProductId = p.Id
        WHERE s.Type NOT IN (1) 
        AND price.DateTo is null 
        AND p.Deleted = 0 
        AND p.Id NOT IN (SELECT ProductId FROM productmatch WHERE InvoiceLinkId = :pilId)
        AND p.ProductCode LIKE :code";
    $stmt = $db->prepare($sqlString);
    $stmt->bindParam(':code', $code, PDO::PARAM_STR);
    $stmt->bindParam(':pilId', $pilId, PDO::PARAM_INT);
	if ($stmt->Execute()){
        $db = null;
        return $stmt;
    }else{
        $db = null;
        return false;   
    }
}

// get all the products
function getAllProducts(){
	$db = connectDb();
	$sqlString =   "SELECT p.*, p.Id AS prodId, s.Name, price.Price FROM product AS p 
                    INNER JOIN productsupplierlink AS psl ON p.Id = psl.ProductId
                    INNER JOIN supplier AS s ON psl.SupplierId = s.Id
                    INNER JOIN price ON price.ProductId = p.Id WHERE price.DateTo is null";
	$stmt = $db->prepare($sqlString);
	$stmt->Execute();
	$db = null;
	return $stmt;
}

function getProductsBySupp($id){
    $db = connectDb();
	$sqlString =   "SELECT p.*, s.Name FROM product AS p 
                    INNER JOIN productsupplierlink AS psl ON p.Id = psl.ProductId
                    INNER JOIN supplier AS s ON psl.SupplierId = s.Id WHERE s.Id = :id";
	$stmt = $db->prepare($sqlString);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
	$stmt->Execute();
	$db = null;
	return $stmt;
}

// adds a new product
//expects in details: code, desc, pack
function addProduct($details){
    $db = connectDb();    
    //check if record is in DB already
    $sql = "SELECT Id FROM product WHERE ProductCode LIKE :pc AND Description LIKE :desc AND Packsize LIKE :ps";
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':pc', $details['code'], PDO::PARAM_STR);
    $stmt->bindParam(':desc', $details['desc'], PDO::PARAM_STR);
    $stmt->bindParam(':ps', $details['pack'], PDO::PARAM_STR);
    if ($stmt->Execute()){
        if ($row = $stmt->fetch()){
            $db = null;
            return array(false, "Product is alredy in DB.");
        }
    }else{
        $db = null;
        return array(false, "Could not execute query.");   
    }
    //sql query as string
    $sql = "insert into product (ProductCode, Description, Packsize) select :prodcode, :desc, :packsize ";    
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':prodcode', $details['code'], PDO::PARAM_STR);
    $stmt->bindParam(':desc', $details['desc'], PDO::PARAM_STR);
    $stmt->bindParam(':packsize', $details['pack'], PDO::PARAM_STR);
    if ($stmt->Execute()){
        return array(true, $db->lastInsertId()); 
    }else{
        $db = null;
        return array(false, "Could not insert record.");
    }
}

//update a product with details
//expects: pid, code, desc, pack
function updateProduct($details){
    $db = connectDb();
    
    // !!!!!!!!!!!TODO !!!!!!!!!!!check if we need an update at all !!!!!!!!!!!!!!! TODO!!!!!!!!!!!!!!!!!!!!!!!
    
    $sql = "UPDATE product SET ProductCode = :pc, Description = :desc, Packsize = :ps WHERE Id = :id";    
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $details['pid'], PDO::PARAM_INT);
    $stmt->bindParam(':pc', $details['code'], PDO::PARAM_STR);
    $stmt->bindParam(':desc', $details['desc'], PDO::PARAM_STR);
    $stmt->bindParam(':ps', $details['pack'], PDO::PARAM_STR);
    if ($stmt->Execute()){
        $db = null;
        return array(true, "Product details updated.");
    }else{
        $db = null;
        return array(false, "Failed to update product details.");   
    }
    

}

//product related end



// price related functions


//expects in details: pid, price, notes
function addPrice($details){
//connect to the database
    $db = connectDb();    
    //sql query as string
    $sql = "insert into price (ProductId, Price, Notes) select :prodid, :price, :notes";
    $stmt = $db->prepare($sql);  
    $stmt->bindParam(':prodid', $details['pid'], PDO::PARAM_INT);
    $stmt->bindParam(':price', $details['price'], PDO::PARAM_STR);
    $stmt->bindParam(':notes', $details['notes'], PDO::PARAM_STR);
    if ($stmt->Execute()){
        return $db->lastInsertId();   
    }else{
        $db = null;
        return false;   
    }
}


//details expects: pid, price, notes
function updatePrice($details){
    //connect to the database
    $db = connectDb();    
    //check if the price is the same as it is stored currently
    $price = $details['price'];
    $pid = $details['pid'];
    $sql = "SELECT Id FROM price WHERE Price = $price AND ProductId = $pid";
    $result = $db->query($sql);
    $same = $result->fetch();
    if (isset($same['Id'])){
        return $same['Id'];
    }
    //sql query as string
    $sql = "update price set DateTo = now() where DateTo is null and ProductId = :id ";    
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $details['pid'], PDO::PARAM_INT);
    $stmt->Execute();
    
    $sql = "insert into price (ProductId, Price, DateFrom, Notes) select :id, :price, now(), :notes";    
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $details['pid'], PDO::PARAM_INT);
    $stmt->bindParam(':price', $details['price'], PDO::PARAM_STR);
    $stmt->bindParam(':notes', $details['notes'], PDO::PARAM_STR);
    if ($stmt->Execute()){
        return $db->lastInsertId();   
    }else{
        $db = null;
        return false;
    }
}


function editPrice($pid, $price, $note){
    $db = connectDb();    
    //sql query as string
    $sql = "UPDATE price SET Price = :price, Notes = :note WHERE Id = :id";    
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $pid, PDO::PARAM_INT);
    $stmt->bindParam(':price', $price, PDO::PARAM_STR);
    $stmt->bindParam(':note', $note, PDO::PARAM_STR);
    if ($stmt->Execute()){
        $db = null;
        return array(true, "Price updated.");
    }else{
        $db = null;
        return array(false, "Failed to update price.");
    }

}

// price related end




// category related functions


function addCategory($details){
    $db = connectDb();
	//prepare the sql statement
    $sql = "INSERT INTO category (Name, ParentId) VALUES (:name, :pid)";
	$stmt = $db->prepare($sql);
	//bind paramaters for security
	$stmt->bindParam(':name', $details['name'], PDO::PARAM_STR);
	$stmt->bindParam(':pid', $details['pid'], PDO::PARAM_INT);
	if ($stmt->execute()){
		$db = null;
        //return true if it is done
        return true;
	}else{
	//close db connection
	$db = null;
	//return false if failed
	return false;
	}
}

function getAllCategory(){
    $db = connectDb();
	$sql = "SELECT cat.Name, cat.Id, cat.ParentId, parent.Name AS pName FROM category AS cat LEFT JOIN category AS parent ON parent.Id = cat.ParentId";
	$result = $db->query($sql);
	$db = null;
	return $result;
}

function getCategoryByName($search){
    $db = connectDb();
    $prep = "%" . $search . "%";
	$sql = "SELECT cat.Name, cat.Id, cat.ParentId, parent.Name AS pName FROM category AS cat LEFT JOIN category AS parent ON parent.Id = cat.ParentId WHERE cat.Name LIKE '$prep'";
	$result = $db->query($sql);
	$db = null;
	return $result;
}

function updateCategory($details){
    $db = connectDb();
    
	//prepare the sql statement
	$sql = "UPDATE category SET Name = :name, ParentId = :pid WHERE Id = :cid";
    $stmt = $db->prepare($sql);
	//bind paramaters for security
    $stmt->bindParam(':cid', $details['cid'], PDO::PARAM_INT);
	$stmt->bindParam(':name', $details['name'], PDO::PARAM_STR);
	$stmt->bindParam(':pid', $details['pid'], PDO::PARAM_INT);
	if ($stmt->execute()){
		$db = null;
        //return true if it is done
        return true;
	}else{
	//close db connection
	$db = null;
	//return false if failed
	return false;
	}

}

function deleteCategory($id){
    $db = connectDb();
    $sql = "DELETE FROM category WHERE Id = $id";
	$stmt = $db->query($sql);
    if ($stmt->rowCount() > 0){
        return true;   
    }else{
        return false;   
    }

}



//category related end



// measurement name functions

function getAllMeasureName(){
    $db = connectDb();
	$sql = "SELECT * FROM measurename";
	$result = $db->query($sql);
	$db = null;
	return $result;

}

function getMeasureNameByName($search){
    $db = connectDb();
    $prep = "%". $search . "%";
	$sql = "SELECT * FROM measurename WHERE Name LIKE '$prep'";
	$result = $db->query($sql);
	$db = null;
	return $result;

}

function addMeasureName($details){
    $db = connectDb();
	//prepare the sql statement
    $sql = "INSERT INTO measurename (Name, Description) VALUES (:name, :desc)";
	$stmt = $db->prepare($sql);
	//bind paramaters for security
	$stmt->bindParam(':name', $details['name'], PDO::PARAM_STR);
	$stmt->bindParam(':desc', $details['desc'], PDO::PARAM_STR);
	if ($stmt->execute()){
		$db = null;
        //return true if it is done
        return true;
	}else{
	//close db connection
	$db = null;
	//return false if failed
	return false;
	}
}

function updateMeasureName($details){
    $db = connectDb();
	//prepare the sql statement
	$sql = "UPDATE measurename SET Name = :name, Description = :desc WHERE Id = :id";
    $stmt = $db->prepare($sql);
	//bind paramaters for security
    $stmt->bindParam(':id', $details['id'], PDO::PARAM_INT);
	$stmt->bindParam(':name', $details['name'], PDO::PARAM_STR);
	$stmt->bindParam(':desc', $details['desc'], PDO::PARAM_STR);
	if ($stmt->execute()){
		$db = null;
        //return true if it is done
        return true;
	}else{
	//close db connection
	$db = null;
	//return false if failed
	return false;
	}
}

function deleteMeasureName($id){
    $db = connectDb();
    $sql = "DELETE FROM measurename WHERE Id = $id";
	$stmt = $db->query($sql);
    if ($stmt->rowCount() > 0){
        return true;   
    }else{
        return false;   
    }
}


// measurement name functions end



//measurement functions


function getMeasures($pid){

     $db = connectDb(); 
        $sql = "SELECT m.*, mn.* FROM measure AS m 
                INNER JOIN measurename AS mn ON mn.Id = m.MeasureNameId
                WHERE m.ProductId = $pid";
        $result = $db->query($sql);
        return $result;
}


//measurement functions end



//client related functions


//add client
function addClient($details){
//connect to the db
	$db = connectDb();
	//prepare the sql statement
	$stmt = $db->prepare("INSERT INTO tblClient (ClientName, ClientActReference, ClientNotes) VALUES (:cname, :cactref, :cnote)");
	//bind paramaters for security
	$stmt->bindParam(':cname', $details['cname']);
	$stmt->bindParam(':cactref', $details['cactref']);
	$stmt->bindParam(':cnote', $details['cnote']);
	if ($stmt->execute()){
		$db = null;
        //return true if it is done
        return true;
	}else{
	//close db connection
	$db = null;
	//return false if failed
	return false;
	}
}

//get all the clients
function getAllClients(){
	$db = connectDb();
	$sql = "SELECT * FROM tblClient";
	$result = $db->query($sql);
	$db = null;
	return $result;
}

//get clients based on search criteria/ name
function getClients($searchFor){	
	$db = connectDb();
	$prepSearchFor = "%" . $searchFor . "%";
	$stmt = $db->prepare("SELECT * FROM tblClient WHERE ClientName LIKE :clientName");
	$stmt->bindParam(':clientName', $prepSearchFor, PDO::PARAM_STR);
	$stmt->Execute();
	$db = null;
	return $stmt;
}

//get client based on search criteria/ client id
function getClientById($searchFor){	
	$db = connectDb();
	$stmt = $db->prepare("SELECT * FROM tblClient WHERE ClientID = :clientId");
	$stmt->bindParam(':clientId', $searchFor, PDO::PARAM_INT);
	$stmt->Execute();
	$db = null;
	return $stmt;
}

// update client details from the array
function updateClientDetails(array $details){
	$db = connectDb();
	$stmt = $db->prepare("UPDATE tblClient SET ClientName = :cname, ClientActReference = :cactref, ClientNotes = :cnote WHERE ClientID = :clientId");
	$stmt->bindParam(':clientId', $details['cid'], PDO::PARAM_INT);
	$stmt->bindParam(':cname', $details['cname'], PDO::PARAM_STR);
	$stmt->bindParam(':cactref', $details['cactref'], PDO::PARAM_STR);
	$stmt->bindParam(':cnote', $details['cnote'], PDO::PARAM_STR);
	if ($stmt->Execute()){
		$db = null;
		return true;
	}else{
		$db = null;
		return false;	
	}
}

//delete client based on clientId
function deleteClient($id){
	$db = connectDb();
	$stmt = $db->prepare("DELETE FROM tblClient WHERE ClientID = :clientId");
	$stmt->bindParam(':clientId', $id, PDO::PARAM_INT);
	if ($stmt->Execute()){
		$db = null;
		return true;
	}else{
		$db = null;
		return false;	
	}
}

//client related end




//extra classes 

class Options
// this class creates a select option (dropdown), populated from the db
{

private $id, $labeltxt, $table, $col, $value; 


public function __construct($id, $labeltxt, $table, $col, $value)
{
	$this->id = $id;
	$this->labeltxt = $labeltxt;
	$this->table = $table;
	$this->col = $col;
	$this->value = $value;
}



public function create()	//create option from database(default)
{
	echo '<label for="'. $this->id . '">'. $this->labeltxt .'</label>
	<select class="form-control" id="'. $this->id . '" name="'. $this->id . '">';
	//get the db
	$results = getColFromTable($this->table, $this->col, $this->value);	
	if ($results != false)
	{
		while($row = $results->fetch())
		{
		//	echo 'in foreach';
			echo '<option value="'.$row[$this->value].'">';
			echo $row[$this->col];
			echo '</option>';
		}
		echo '</select>';
	}else{
		echo 'No results were found.';
		echo '</select>';	
	}
	
}
} // end of class