<?php
include ('appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}


if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    
    if (isset($_GET['ccId'])){
        $ccId = $_GET['ccId'];
            $db = connectDb();
            $sql = "SELECT pil.Qty AS p1Qty, pil.Id AS p1PilId,
            p1.ProductCode AS p1Code, p1.Description AS p1Desc, p1.Packsize AS p1Pack, 
            p2.ProductCode AS p2Code, p2.Description AS p2Desc, p2.Packsize AS p2Pack,
            s1.Name AS p1Supplier,
            s2.Name AS p2Supplier,
            pr1.Price AS p1Price, pr1.DateFrom AS p1Date,
            pr2.Price AS p2Price, pr2.DateFrom AS p2Date,
            m1.Value AS p1ValueInGram,
            m2.Value AS p2ValueInGram,
            i.InvoiceDate,
            pm.Final AS p2Final, pm.Id AS p2PmId
            FROM productinvoicelink AS pil
            INNER JOIN product AS p1 ON pil.ProductIdLeft = p1.Id
            INNER JOIN productsupplierlink AS psl1 ON psl1.ProductId = pil.ProductIdLeft
            INNER JOIN supplier AS s1 ON psl1.SupplierId = s1.Id
            INNER JOIN price AS pr1 ON pil.PriceIdLeft = pr1.Id
            INNER JOIN invoice AS i ON i.Id = pil.InvoiceId
            LEFT JOIN measure AS m1 ON m1.ProductId = p1.Id
            LEFT JOIN productmatch AS pm ON pm.InvoiceLinkId = pil.Id
            LEFT JOIN price AS pr2 ON pm.PriceId = pr2.Id
            LEFT JOIN product AS p2 ON p2.Id = pm.ProductId
            LEFT JOIN productsupplierlink AS psl2 ON psl2.ProductId = p2.Id
            LEFT JOIN supplier AS s2 ON psl2.SupplierId = s2.Id
            LEFT JOIN measure AS m2 ON m2.ProductId = p2.Id
            WHERE i.CcId = $ccId
            ORDER BY p1Desc
            ";
            //$sql = "SELECT * FROM user";
            $result = $db->query($sql);
    }
    $productsInCC = array();
    $productsInCC = $result->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($productsInCC);
    $addrStr = '../../reportsgraphs.php?ccId=' . $ccId;
    //redirect_user($addrStr);
}


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
   
    
}