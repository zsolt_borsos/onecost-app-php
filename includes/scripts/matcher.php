<?php
include ('appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

if ($_SERVER['REQUEST_METHOD'] == 'POST'){

    $answer = array();
    
    if (isset($_POST['action'])){
        $action = $_POST['action'];
        
        
        if ($action == 'addMatch'){
            $pilId = $_POST['pilId'];
            $details = array();
            $details['pilId'] = $pilId;
            $details['prodId'] = $_POST['prodId'];
            $details['priceId'] = $_POST['priceId'];
            //check if this match is already added
            $inDb = checkMatch($details['prodId'], $pilId);
            //print_r($inDb);        
            if ( $inDb[0] > 0){
                die("indb");   
            }
            if (setProductMatch($details)){
                echo json_encode("done");
            }else{
                echo json_encode("fail"); 
            }
            
        }
        
        if ($action == 'removeMatch'){
            $pmId = $_POST['pmId'];
            if (removeProductMatch($pmId)){
                echo json_encode("done");
            }else{
                echo json_encode("fail"); 
            }    
        }
    
        if ($action == 'resetMatch'){
            $pilId = $_POST['pilId'];
            if (resetAllProductMatch($pilId)){
                echo json_encode("done");
            }else{
                echo json_encode("fail"); 
            } 
        }
       
        if ($action == 'setFinal'){
            $pmId = $_POST['pmId'];
            if (setMatchFinal($pmId)){
                echo json_encode("done");
            }else{
                echo json_encode("fail"); 
            }        
            
        }
        
        if ($action == 'getMatches'){
            $pilId = $_POST['pilId'];
            $matches = getProductMatch($pilId);
            $answer = $matches->fetchAll(PDO::FETCH_ASSOC);
            echo json_encode($answer);
        }

    } //end of ACTION
    
    
} // end of POST




?>