 <?php //the script updates the user details in the user table, this script using functions from appfunctions.php

include ("appfunctions.php");

//security check
session_start(); // get current session if exists

// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {
	redirect_user('../../index.php');	
}


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (isset($_POST['action'])){
        $action = $_POST['action'];
        
        if ($action == 'update'){
            $details = array();
    
            $sql ="UPDATE user SET ";
            
            //check firstname	
            if (isset($_POST['fname'])){
                $fname = $_POST['fname'];
                $sql .="Firstname = :fn";
                $details['fname'] = $fname;
            }
            //check surname
            if (isset($_POST['sname'])){
                $sname = $_POST['sname'];
                $sql .= ", Surname = :sn";
                $details['sname'] = $sname;
            }
                
            //check email
            if (isset($_POST['email'])){
                $email = $_POST['email'];
                $sql .= ", Email = :em";
                $details['email'] = $email;
            }
            
                
            $details['sql'] = $sql;
            
                //debug
                //echo "sql string is: " . $sql;
            $userId = $_SESSION['myId'];
            //call the update function from appfunctions.php    
            if (updateUserDetails($details, $userId)){
                setMsg("Profile update successful.");
            }else{
                setErrorMsg("Profile update failed.");
            }
                
            redirect_user("../../userupdate.php");
            
            
        }// update action end
    
        if ($action == 'changePass'){
         
            $oldPass = $_POST['oldPass'];
            $newPass = $_POST['newPass'];
            $pass = getUserPass($_SESSION['myId']);
            $myPass = $pass->fetch();
            
            if ( $myPass[0] == sha1($oldPass)){
                list($done, $answer) = setUserPass($_SESSION['myId'], sha1($newPass));
                if ($done){
                    setMsg($answer);
                }else{
                    setErrorMsg($answer);   
                }
                redirect_user("../../userupdate.php");
            }else{
                //wrong password!
                setErrorMsg("Wrong password. Please try it again.");
                redirect_user("../../userupdate.php");
            }
        }
    
    
      

    } //if no action set then let it fall back to the default redirect
} // post check end
//redirect by default
redirect_user("../../index.php");    

?>