<?php
include ('appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
    if (isset($_POST['ccId'])){
        $db = connectDb();
        $msg = $_POST['chatMsg'];
        $ccId = $_POST['ccId'];
        $name = $_SESSION['fname'] . " " . $_SESSION['sname'];
        $sql = "INSERT INTO msgboard (Name, Message, CcId) VALUES (:name, :msg, :ccId)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':msg', $msg, PDO::PARAM_STR);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':ccId', $ccId, PDO::PARAM_INT);
        $redStr = '../../ccmanager.php?ccId=' . $ccId;
            if ($stmt->Execute()){
                //redirect_user($redStr);
                $db = null;
            }else{
                $db = null;
                setErrorMsg('Failed to send message.');
                redirect_user($redStr);
            }
    }else{
    if (isset($_POST['chatMsg'])){
        $db = connectDb();
        $msg = $_POST['chatMsg'];
        $name = $_SESSION['fname'] . " " . $_SESSION['sname'];
        $sql = "INSERT INTO msgboard (Name, Message) VALUES (:name, :msg)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':msg', $msg, PDO::PARAM_STR);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
            if ($stmt->Execute()){
                $db = null;
                redirect_user('../../index.php');   
            }else{
                setErrorMsg('Failed to send message.');
                $db = null;
                redirect_user('../../index.php');
            }

    }else{
        redirect_user('../../index.php');
        }
    }
    
    
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    
    if (isset($_GET['ccId'])){
        $ccId = $_GET['ccId'];
        $db = connectDb();
        $sql = "SELECT Name, Message, Date FROM msgboard WHERE CcId = $ccId ORDER BY Id ASC LIMIT 100";
        $result = $db->query($sql);
        $db = null;
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($rows);
    }else{
        //get the last ?? 50 ?? messages
        $db = connectDb();
        $sql = "SELECT Name, Message, Date FROM msgboard WHERE CcId IS NULL ORDER BY Id ASC LIMIT 100";
        $result = $db->query($sql);
        $db = null;
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($rows);
    }
}
