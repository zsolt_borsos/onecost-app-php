<?php //CC starting page
$page_title = "Cost Comparison";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

include ('includes/templates/header.html');

//check for POST and use search box to filter results
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	//print_r($_POST);
	
     if (isset($_POST['cid'])){
        $currentId = false;
        $details = array();
        $details['cid'] = $_POST['cid'];
        $details['status'] = $_POST['status'];
        $details['prio'] = $_POST['prio'];
        $details['note'] = $_POST['note'];
        if ($currentId = addCC($details)){
            setMsg("CC created.");
        }else{
            setErrorMsg("CC could not be added.");
            //reloadMe();
        }
        // adding assignments to DB
        if (isset($_POST['assign'])){
            $assDet = array();
            $assDet['userId'] = $_POST['assign'];
            $assDet['ccId'] = $currentId;
            if (assignUsersToCC($assDet)){
                setMsg("CC created. Users assigned to CC successful."); 
            }else{
                 setErrorMsg("Failed to assign users to CC.");
            }
        }else{
            resetAssignedUsers($currentId);
            //clean records (usercclink) for this CC as it is just been added without assigned info   
        }
        reloadMe();
    }
    
    if (isset($_POST['ecid'])){
        $details = array();
        $details['id'] = $_POST['eid'];
        $details['cid'] = $_POST['ecid'];
        $details['status'] = $_POST['estatus'];
        $details['prio'] = $_POST['eprio'];
        $details['note'] = $_POST['enote']; 
        if (updateCC($details)){
            setMsg("CC updated.");        
        }else{
            setErrorMsg("CC could not be updated.");
            reloadMe();
        }
         if (isset($_POST['eassign'])){
            $assDet = array();
            $assDet['userId'] = $_POST['eassign'];
            $assDet['ccId'] = $_POST['eid'];
            if (assignUsersToCC($assDet)){
                setMsg("CC Updated. Users assigned to CC successful."); 
            }else{
                 setErrorMsg("Failed to assign users to CC.");
            }
        }else{
            resetAssignedUsers($_POST['eid']);
            //delete assigned users from this cc!!   
        }
        reloadMe();
    }
    
    if (isset($_POST['did'])){
        if (deleteCC($_POST['did'])){
            setMsg("CC deleted.");   
            reloadMe();
        }else{
            setErrorMsg("CC could not be deleted.");
            reloadMe();
        }   
        
    }
    
}
?>

<div class="page-header">
    <h2>Cost Comparisons Overview</h2>
</div>

<div>
    <p>
        <button type="button" class="btn btn-primary btnAdd">Add CC</button>
        <input type="button" value="Toggle Search" class="btn btn-primary" onclick=" $('#divSearch').toggle();">
        <button type="button" class="btn btn-default" onclick="location.reload();">Reset Search</button>
    </p>
</div>    

<div id="divSearch" style="display:none;">
    <form id="search" action="cc.php" class="form-inline" method="post"> 
    <div class="form-group">
        <input type="text" name="txtSearch" id="txtSearch" placeholder="Search CC's..." class="form-control"> </div>
        ||
        <div class="form-group">
        <input type ="submit" value="Search" class="btn btn-primary">
        </div> 
    </form>
</div>

<div class="table-responsive info col-md-12" id="myTable">
<p>List of CC's:</p>
    <form class="hidden" id="manageForm" name="manageForm" role="form" action="ccmanager.php" method="get">
        <input type="hidden" id="ccId" name="ccId">
    </form>
    <table class="table table-bordered table-hover text-center">
		<tr>
		<!-- <th>CC Id</th> -->
		<th>Client Name</th>
        <th>Status</th>
        <th>Priority</th>
        <th>Notes</th>
        <th>Created At</th>
        <th>Created By</th>
        <th>Modified At</th>
        <th>Modified By</th>
        <th>Assigned To</th>
        <th>Delete</th>
        <th>Edit</th>
        <th>Manage</th>
		</tr>
        <?php 
        if (isset($_POST['txtSearch'])){
            $results = getCC($_POST['txtSearch']);
        }else{
            $results = getAllCC();
        }
        //print_r($results);
            
            while ($row = $results->fetch()){
                //print_r($row);
                echo '<tr>';
                    //echo '<td>' . $row['Id'] . '</td>';
                    echo '<td>' . $row['Name'] . '</td>';
                    echo '<td>';
                        if ($row['Status'] == 1){
                            echo "Invoices in progress";   
                        }else if ($row['Status'] == 2){
                            echo "Invoices done";   
                        }else if ($row['Status'] == 3){
                            echo "Products are matched";
                        }else{
                            echo 'Ready for client <span class="glyphicon glyphicon-ok"></span>';
                        }
                    echo '</td>';
                    echo '<td>';
                        if ($row['Priority'] == 1){
                            echo 'Low';   
                        }else if ($row['Priority'] == 2){
                            echo 'Normal';
                        }else{
                            echo 'High';   
                        }
                    echo '</td>';
                    echo '<td>' . $row['Notes'] . '</td>';
                    echo '<td>' . $row['CreatedAt'] . '</td>';
                    echo '<td>' . $row['fn'] . " " .  $row['sn'] . '</td>';
                    echo '<td>' . $row['ModifiedAt'] . '</td>';
                    echo '<td>' . $row['fn2'] . " " . $row['sn2'] . '</td>';
                    $assignedUsers = getAssignedUsers($row['Id']);
                    //print_r($assignedUsers);
                    echo '<td>';
                    $counter = 0;
                    $assignedUsersStore = array();
                    while ($user = $assignedUsers->fetch()){
                        //print_r($user);
                        if ($counter > 0){
                            echo ", ";
                        }
                        echo $user['fname'] . " " . $user['sname'];
                        $assignedUsersStore[] = $user;
                        $counter++;
                    }
                    echo '</td>';
                    echo '<td> <input type="button" value="Delete" class="btn btn-danger"              onclick="grabForDelete(\''.$row['Id'].'\',\''.$row['Name'].'\');"></td>';
                    echo '<td> <input type="button" value="Edit" class="btn btn-info" onclick="grabForEdit( '. htmlspecialchars(json_encode($row)) .', '. htmlspecialchars(json_encode($assignedUsersStore)) .');"></td>';
                    echo '<td> <button type="submit" value="'. $row['Id'] .'" class="btn btn-primary manageBtn" for="manageForm">Manage</button> </td>';
                echo '</tr>';
            }        
        ?>
    </table>
</div>


<div id="addForm" class="addForm col-md-12" style="display:none;">
    
    <form role="form" action="cc.php" method="post" class="form-horizontal"> 
        <div class="col-md-5">
            <div class="page-header">
                <h3>Add CC</h3>
            </div>
            
                <div class="form-group">
                    <label for="cid">Select Client:</label>
                      <?php
                        //details: [sid] = id/name, [table] = table name, [col] = column to show, [valCol] = columns for Value,
                        $details = array();
                        $details['sid'] = "cid";
                        $details['table'] = "client";
                        $details['col'] = "Name";
                        $details['valCol'] = "Id";
                        createSelect($details);
                      ?>
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                        <select class="form-control" id="status" name="status">
                            <option value="1">Invoices in progress</option>
                            <option value="2">Invoices done</option>
                            <option value="3">Products are matched</option>
                            <option value="4">Ready for client</option>
                        </select>
                </div>
              <div class="form-group">
                <label for="prio">Priority</label>
                    <select class="form-control" id="prio" name="prio">
                      <option value="1">Low</option>
                      <option value="2">Normal</option>
                      <option value="3">High</option>
                    </select>
              </div>
              <div class="form-group">
                <label for="note">Notes</label>
                <input type="text" class="form-control" id="note" name="note" placeholder="Notes">
              </div>
        </div>
        <div class="col-md-4 col-md-offset-1">
            
            <div class="page-header">
                <h3>Assign Users</h3>
            </div>
            
            <?php
                //get all users
                $users = getAllUser();
                while ($row = $users->fetch()){
                    echo '<div class="checkbox">';
                    echo '<label>';
                    echo '<input type="checkbox" value="'. $row['Id'] .'" name="assign[]">';
                    echo $row['Firstname'] . " " . $row['Surname'];
                    echo '</label>';
                    echo '</div>';
                }
            ?>
      
        </div>
        <div class="col-md-10">
            <div class="form-group pull-right">
                
                <button type="button" class="btn btn-default" onclick="hideDiv('addForm'); showDiv('myTable');">Cancel</button>
                <button type="submit" class="btn btn-primary">Create CC</button>
                
            </div>  
        </div>
    </form>
</div>



<div id="editForm" class="editForm" style="display:none;">
     <form role="form" action="cc.php" method="post" class="form-horizontal"> 
        <div class="col-md-5">
            <div class="page-header">
                <h3>Update CC</h3>
            </div>
       
            <div class="form-group">
                <label for="eid">CC Id</label>
                <input type="number" class="form-control" id="eid" name="eid" placeholder="Id" readonly>
            </div>
            <div class="form-group">
                <label for="ecid">Select Client:</label>
                  <?php
                    //details: [sid] = id/name, [table] = table name, [col] = column to show, [valCol] = columns for Value,
                    $details = array();
                    $details['sid'] = "ecid";
                    $details['table'] = "client";
                    $details['col'] = "Name";
                    $details['valCol'] = "Id";
                    createSelect($details);
                  ?>
            </div>
            <div class="form-group">
                <label for="estatus">Status</label>
                    <select class="form-control" id="estatus" name="estatus">
                        <option value="1">Invoices in progress</option>
                        <option value="2">Invoices done</option>
                        <option value="3">Products are matched</option>
                        <option value="4">Ready for client</option>
                    </select>
            </div>
          <div class="form-group">
            <label for="eprio">Priority</label>
                <select class="form-control" id="eprio" name="eprio">
                  <option value="1">Low</option>
                  <option value="2">Normal</option>
                  <option value="3">High</option>
                </select>
          </div>
          <div class="form-group">
            <label for="enote">Notes</label>
            <input type="text" class="form-control" id="enote" name="enote" placeholder="Notes">
          </div>
    </div>
    
    <div class="col-md-4 col-md-offset-1" id="echecks">
    
        <div class="page-header">
            <h3>Assign Users</h3>
        </div >            
            <?php
               
                $users = getAllUser();
                while ($row = $users->fetch()){
                    echo '<div class="checkbox">';
                    echo '<label>'; 
                    echo '<input type="checkbox" value="'. $row['Id'] .'" name="eassign[]">';
                    echo $row['Firstname'] . " " . $row['Surname'];
                    echo '</label>';
                    echo '</div>';
                }
            ?>
      
    </div>
    <div class="col-md-10">
        <div class="form-group pull-right">
           <button type="button" class="btn btn-default" onclick="hideDiv('editForm'); showDiv('myTable');">Cancel</button>
           <button type="submit" class="btn btn-primary">Update CC</button>
        </div>
    </div>
</form>
</div>


<div id="divDelete" style="display:none;">
    <form id="formDelete" action="cc.php" class="form-inline" method="post" role="form"> 
        <p>Delete CC:</p>    
        <p>
            <span class="bg-danger">Warning you are about to delete this record, this cannot be un-done!</span>
            Are you sure you want to remove :
        </p>
        <p>
            <div class="form-group">
            <input type="text" name="dname" id="dname" readonly="true" class="form-control"> </div> 
            Id :  
            <div class="form-group">
            <input type ="text" name="did" id="did" class="form-control" readonly="true"> </div>  ?
            <div class="form-group">
            <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divDelete');">
            <input type ="submit" value="Confirm Removal" class="btn btn-danger">
            </div> 
        </p>     
    </form> 
</div>



<script type="text/javascript" src="js/cc.js"></script>



<?
include ('includes/templates/footer.html');
?>