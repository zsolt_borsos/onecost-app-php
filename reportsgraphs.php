<?php
$page_title = "Reports";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

include ('includes/templates/header.html');

?>
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load('visualization', '1.1', {packages: ['controls']});
    </script>
    <script type="text/javascript">
      function drawVisualization() {
        // Prepare the data
        
        //var data = google.visualization.arrayToDataTable(myData);
        
        var data = google.visualization.arrayToDataTable([
            ['Product', 'Packsize', 'Qty', 'Price', 'Total', 'Pro Rata', 'Supplier', 'Product', 'Packsize', 'Price', 'Total', 'Pro Rata', 'Variant', 'Saving'],
          ['Flour' , '1kg', 2, 3.10, 6.20, 0.83, 'Brakes', 'Plain flour', '1kg', 2.60, 5.20, 0.46, -1.00, '25%'],
          ['Beans' , '2kg', 1, 4.20, 4.20, 0.68, 'Alliance', 'Beans', '5kg', 6.50, 6.50, 0.26, 2.30, '40%']
        ]);
      
        
      
        // Define a category picker control for the Gender column
        var supplierPicker = new google.visualization.ControlWrapper({
          'controlType': 'CategoryFilter',
          'containerId': 'control1',
          'options': {
            'filterColumnLabel': 'Supplier',
            'ui': {
            'labelStacking': 'vertical',
              'allowTyping': false,
              'allowMultiple': true
            }
          }
        });
      
        
      
        // Define a table
        var table = new google.visualization.ChartWrapper({
          'chartType': 'Table',
          'containerId': 'chart1',
          'options': {
            'width': '1000px'
          }
        });
      
        // Create a dashboard
        new google.visualization.Dashboard(document.getElementById('dashboard')).
            // Establish bindings, declaring the both the slider and the category
            // picker will drive both charts.
            bind([supplierPicker], [table]).
            // Draw the entire dashboard.
            draw(data);
      }
      

      //google.setOnLoadCallback(drawVisualization);
    </script>

<div class="row">
    <div class="page-header">
        <h2>Graphs for Reports</h2>
    </div>
</div> 
<div class="row">
    <form class="form-inline" action="includes/scripts/reportsapi.php" method="get" id="selectCC" name="selectCC">
        <div class="form-group">
            <?php 
                    //details: [sid] = id/name, [table] = table name, [col] = column to show, [valCol] = columns for Value,
                    $details = array();
                    $details['sid'] = "ccId";
                    $details['table'] = "vwrpt_ccsummaryddl";
                    $details['col'] = "display";
                    $details['valCol'] = "Id";
                    createSelect($details);
                    ?>
        </div>
        
        <div class="form-group">
            <select name="type" id="type">
                <option value="1">Line</option>
                <option value="2">Bar</option>
                <option value="3">Pie</option>
            </select>
        </div>
        <button class="btn btn-primary" type="submit" form="selectCC">Click me</button>
    </form>
</div>
    

<div id="dashboard">
      <table>
        <tr style='vertical-align: top'>
          <td style='width: 200px; font-size: 0.9em;'>
            <div id="control1"></div>
          </td>
        </tr>
          <tr>
          <td style='width: 1200px'>
            <div style="float: left;" id="chart1"></div>
          </td>
        </tr>
          <tr>
            <td>
                <div style="float: left;" id="chart2"></div>
            </td>
        
        </tr>
      </table>
</div>



    
    
    
<!-- <script src="js/Chart.min.js"></script> -->
<script type="text/javascript" src="js/reportsgraphs.js"></script>
    
<?php
include ('includes/templates/footer.html');
?>