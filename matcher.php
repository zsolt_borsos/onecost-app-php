<?php
$page_title = "Product Matcher";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

function getProductInfo($ccId){
    $db = connectDb();
	$sql = "SELECT p.Id, p.Description, pil.Id AS pilId 
            FROM product AS p
            INNER JOIN productinvoicelink AS pil ON pil.ProductIdLeft = p.Id
            INNER JOIN invoice AS i ON i.Id = pil.InvoiceId
            WHERE i.CcId = $ccId
            GROUP BY p.Id
            ";
	$result = $db->query($sql);
	$products = $result->fetchAll(PDO::FETCH_ASSOC);
	$db = null;
	return $products;
}

if ($_SERVER['REQUEST_METHOD'] == 'GET'){
  
    if (isset($_GET['ccId'])){
        $ccId = $_GET['ccId']; 
    }else{
        redirect_user('cc.php');
    }
    
    if (isset($_GET['pilId'])){
        $pilId = $_GET['pilId'];
        $res = getProductIdFromPil($pilId);
        $res = $res->fetch(PDO::FETCH_ASSOC);
        $pid = $res['ProductIdLeft'];
        $selectedProduct = getProduct($pid);
        $selectedProduct = $selectedProduct->fetch(PDO::FETCH_ASSOC);
        $suggested = getSuggestedProducts($pid);
        
    }
    //this should have: product Id, product description, productinvoicelink Id
    $products = getProductInfo($ccId);
    
    if (isset($_GET['searchIn'])){
        //if searching for products
        $searchIn = $_GET['searchIn'];
        $searchFor = $_GET['searchFor'];
        if ($searchIn == 'desc'){
            //if searching by description    
            $foundProducts = getAllOcProductByDesc($searchFor, $pilId);
        }
        if ($searchIn == 'code'){
            $foundProducts = getAllOcProductByCode($searchFor, $pilId);
        }
        
    }
    
}

include ('includes/templates/header.html');
?>

<div class="row">
    <a href="ccmanager.php?ccId=<?php echo $ccId?>" class="btn btn-default">Back to CC Manager</a>
    <div class="page-header">
        <h2>Product Matcher</h2>
    </div>
</div>
    <!-- top part: product selection and matched products -->
    
<div class="row">
    <!-- left side: product selection and info -->
    <div class="col-md-5 col-lg-5">
        <form id="selectProduct" name="selectProduct" action="matcher.php" method="get" class="form-inline">
            <input type="hidden" name="ccId" id="ccId" value="<?php echo $ccId;?>">
            <div class="form-group">  
                <label for="product">Product:</label>
                <select name="pilId" id="pilId" class="form-control">
                <?php
                    //create a dropdown for products in this CC
                    //print_r($products);
                    foreach ($products as $product){
                        if ((isset($selectedProduct)) && ($selectedProduct['Id'] == $product['Id'])){
                             echo '<option value="'. $product['pilId'] .'" selected>'; 
                        }else{
                             echo '<option value="'. $product['pilId'] .'">';
                        }
                        echo $product['Description'];
                        echo '</option>';
                    }
                ?>
                </select>  
            </div>
            <div class="form-group">
                <button type="submit" form="selectProduct" class="btn btn-primary">Select</button>
            </div>  
        </form>
        <div class="well well-sm">
            <h4>Current product:</h4>
            <p><b>Description:<span><i><?php if (isset($selectedProduct)) echo " ".$selectedProduct['Description']; ?></i></span></b></p>
            <p><b>Packsize:<span><i><?php if (isset($selectedProduct)) echo " ".$selectedProduct['Packsize']; ?></i></span></b></p>
            <p><b>Price:<span><i><?php if (isset($selectedProduct)) echo " £".$selectedProduct['Price']; ?></i></span></b></p>
        </div>
    </div> <!-- left side: product selection and info END -->
    
    <!-- right side: matched products list -->
    <div class="col-md-7 col-lg-7">
        
        <div class="table-responsive" id="matchedProductTable">
            <table class="table table-bordered table-hover text-center">
                <tr>
                    <th scope="col">Supplier</th>
                    <th scope="col">Description</th>
                    <th scope="col">Packsize</th>
                    <th scope="col">Measures</th>         
                    <th scope="col">Price</th>
                    <th scope="col">Remove</th>
                </tr> 
                <tbody id="matchedProductBody">
                    <?php
                        if (isset($pilId)){
                            $matchedProducts = array();
                            $matches = getProductMatch($pilId);
                            $matchCounter = 0;
                            while ($match = $matches->fetch(PDO::FETCH_ASSOC)){
                                //print_r($match);
                                echo '<tr>';
                                    echo '<td>' . $match['suppName'] . '</td>';
                                    echo '<td>' . $match['Description'] . '</td>';
                                    echo '<td>' . $match['Packsize'] . '</td>';
                                    //measure here 
                                    echo '<td>measure</td>';
                                    echo '<td>' . $match['Price'] . '</td>';
                                    echo '<td> <button type="button" value="'. $match['pmId'] .'" class="btn btn-danger removeMatchBtn">Remove</button></td>';
                                    
                                echo '</tr>';
                                $matchCounter++;
                                $matchedProducts[] = $match['ProductId'];
                            }
                            if ($matchCounter == 0){
                                echo '<tr><td colspan="6">There are no matched products, yet.</td></tr>';      
                            }
                        }else{
                            echo '<tr><td colspan="6">Select a product.</td></tr>';   
                        }
                                
                        
                     
                    ?>            
                </tbody>
            </table>
        </div>
        
        
    
    </div> <!-- right side: matched products list END -->
    
</div> <!-- top part: product selection and matched products END -->
 
<!-- suggested products table -->
<div class="row">
    <div class="page-header">
        <h4>Suggested Products</h4>
    </div>
    

        <div class="table-responsive" id="suggestedProducts">
            <table class="table table-bordered table-hover text-center">
                <tr>
                    <th scope="col">Supplier</th>
                    <th scope="col">Code</th>
                    <th scope="col">Description</th>
                    <th scope="col">Packsize</th>      
                    <th scope="col">Updated</th>
                    <th scope="col">Price</th>
                    <th scope="col">Select</th>
                </tr> 
                <tbody id="suggestedBody">
                    
        <?php
            if (isset($suggested)){
                $suggCounter = 0;
                while($row = $suggested->fetch(PDO::FETCH_ASSOC)){
                    //print_r($row);
                    echo '<tr>';
                        echo '<td>' . $row['Name'] . '</td>';
                        echo '<td>' . $row['ProductCode'] . '</td>';
                        echo '<td>' . $row['Description'] . '</td>';
                        echo '<td>' . $row['Packsize'] . '</td>';
                        echo '<td>' . $row['DateFrom'] . '</td>';
                        echo '<td>' . $row['Price'] . '</td>';
                        if (isset($selectedProduct)){
                            echo '<td> <button type="button" data-match-info="'. htmlspecialchars(json_encode($row)) .'" value="'. $row['prodId'] .'" class="btn btn-primary selectMatchBtn">Select</button></td>';
                        }else{
                            echo '<td> <button type="button" data-match-info="'. htmlspecialchars(json_encode($row)) .'" value="'. $row['prodId'] .'" class="btn btn-primary selectMatchBtn" disabled>Select</button></td>';
                        }
                    
                    echo '</tr>';
                    $suggCounter++;
                }
                if ($suggCounter == 0){
                    echo '<tr><td colspan="7">No suggestion for this product.</td></tr>';
                }
            }
        
        ?> 
         
                </tbody>
            </table>
        </div>
</div><!-- suggested products table END-->

<!-- search in DB -->
    <div class="row">
        <div class="page-header">
            <h4>Search for Products</h4>
        </div>
    </div>
   
        <form method="get" action="matcher.php" class="form-inline" name="searchProductForm" id="searchProductForm" role="form">
            <input type="hidden" name="ccId" id="ccId" value="<?php echo $ccId;?>">
            <?php
                if (isset($pilId)){
                    echo '<input type="hidden" name="pilId" id="pilId" value="'. $pilId .'">';
                }
            ?>
            <div class="form-group">
                <div class="radio">
                    <label class="radio-inline">
                        <input type="radio" name="searchIn" id="searchIn" value="code" <?php if ((isset($searchIn)) && ($searchIn == 'code')) echo ' checked'; ?>>Product Code
                    </label>
                    </div>
                    <div class="radio">
                    <label class="radio-inline">
                        <input type="radio" name="searchIn" id="searchIn" value="desc" <?php if ((isset($searchIn)) && ($searchIn == 'desc')) echo ' checked '; ?> required>Description
                    </label>
                </div>
            </div>
            
            <div class="form-group">
                <input type="search" id="searchFor" name="searchFor" class="form-control" placeholder="Searching for..." required value="<?php if(isset($searchFor)) echo $searchFor;?>">
            </div>
            
            <div class="form-group">
                <button type="submit" class="btn btn-primary searchBtn" form="searchProductForm">Search</button>
            </div>
      
           
        </form>
 
        <div class="table-responsive" id="productsTable">
            <table class="table table-bordered table-hover text-center">
                <tr>
                    <th scope="col">Supplier</th>
                    <th scope="col">Code</th>
                    <th scope="col">Description</th>
                    <th scope="col">Packsize</th>         
                    <th scope="col">Updated</th>
                    <th scope="col">Price</th>
                    <th scope="col">Select</th>
                </tr> 
                <tbody id="productsBody">
        <?php
            if (isset($foundProducts)){
                $foundCounter = 0;
                while($row = $foundProducts->fetch(PDO::FETCH_ASSOC)){
                    //print_r($row);
                    echo '<tr>';
                        echo '<td>' . $row['Name'] . '</td>';
                        echo '<td>' . $row['ProductCode'] . '</td>';
                        echo '<td>' . $row['Description'] . '</td>';
                        echo '<td>' . $row['Packsize'] . '</td>';
                        echo '<td>' . $row['DateFrom'] . '</td>';
                        echo '<td>' . $row['Price'] . '</td>';
                        if (isset($selectedProduct)){
                            echo '<td> <button type="button" data-match-info="'. htmlspecialchars(json_encode($row)) .'" value="'. $row['prodId'] .'" class="btn btn-primary selectMatchBtn">Select</button></td>';
                        }else{
                            echo '<td> <button type="button" data-match-info="'. htmlspecialchars(json_encode($row)) .'" value="'. $row['prodId'] .'" class="btn btn-primary selectMatchBtn" disabled>Select</button></td>';
                        }
                        
                     
                    echo '</tr>';
                    $foundCounter++;
                }
                if ($foundCounter == 0){
                    echo '<tr><td colspan="7">No match found.</td></tr>';
                }
            }
        
        ?> 
                </tbody>
            </table>
        </div>
    <!-- search in DB END -->

<script type="text/javascript" src="js/matcher.js"></script>

<?php
include ('includes/templates/footer.html');
?>
