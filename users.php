<?php //users.php
$page_title = "Users";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

if ($_SESSION['Access'] < 2){
    setErrorMsg("You have no permission to see that page.");
    redirect_user('index.php');
}


if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if (isset($_POST['email'])){
        $details = array();
        $details['fname'] = $_POST['fname'];
        $details['sname'] = $_POST['sname'];
        $details['email'] = $_POST['email'];
        $details['pass'] = sha1($_POST['pass']);
        if (isset($_POST['enabled'])){
            $details['enabled'] = 1;       
        }else{
            $details['enabled'] = 0;   
        }
        $details['access'] = $_POST['access'];
        if (addUser($details)){
            setMsg("User added.");   
        }else{
            setErrorMsg("User could not be added.");
        }
    }
    if (isset($_POST['eid'])){
        $details = array();
        $details['id'] = $_POST['eid'];
        $details['fname'] = $_POST['efname'];
        $details['sname'] = $_POST['esname'];
        $details['email'] = $_POST['eemail'];
        if (isset($_POST['eenabled'])){
            $details['enabled'] = 1;       
        }else{
            $details['enabled'] = 0;   
        }
        $details['access'] = $_POST['eaccess'];
        
        if (updateUser($details)){
            setMsg("User updated.");   
        }else{
            setErrorMsg("User could not be updated.");
        }
    }
    //deleting user
    if (isset($_POST['did'])){
        if (deleteUser($_POST['did'])){
            setMsg("User deleted.");   
        }else{
            setErrorMsg("User could not be deleted.");
        }   
        
    }
    
    //resetting password
    if (isset($_POST['resetPass'])){
        if (isset($_POST['resetUserId'])){
            $ruId = $_POST['resetUserId'];
            //echo $ruId;
            $answer = resetUserPass($ruId);
            if ($answer['done'] == true){
                setMsg($answer['msg']);   
            }else{
                setErrorMsg($answer['msg']);
            }
        }else{
            setErrorMsg('Error! No User Id.');   
        }
    }
}

include ('includes/templates/header.html');
?>

<div class="page-header">
    <h2>Manage Users</h2>
</div>

<div>
    <p>
        <button type="button" class="btn btn-primary btnAdd">Add User</button>
        <input type="button" value="Toggle Search" class="btn btn-primary" onclick="toggle('divSearch');">
        <button type="button" class="btn btn-default" onclick="location.reload();">Reset Search</button>
    </p>
</div>    

<div id="divSearch" style="display:none;">
    <form id="search" action="users.php" class="form-inline" method="post"> 
    <div class="form-group">
        <input type="text" name="txtSearch" id="txtSearch" placeholder="Enter User name or Email" class="form-control"> </div>
        ||
        <div class="form-group">
        <input type ="submit" value="Search" class="btn btn-primary">
        </div> 
    </form>
</div>

<form action="users.php" method="post" name="resetPassForm" id="resetPassForm">
    <input type="hidden" name="resetUserId" id="resetUserId">
    <input type="hidden" name="resetPass" id="resetPass" value="1">
</form>

<div class="table-responsive" id="userTable">
<p>List of users:</p>
    <table class="table table-bordered table-hover text-center">
        <tr>
		<th scope="col">Id</th>
		<th scope="col">Firstname</th>
        <th scope="col">Surname</th>
        <th scope="col">Email</th>
        <th scope="col">Enabled</th>
        <th scope="col">Access level</th>
        <th scope="col">Edit</th>
        <th scope="col">Reset Password</th>
        <th scope="col">Delete</th>
        </tr>

<?php


if (isset($_POST['txtSearch'])){
            $result = getUsersByName($_POST['txtSearch']);
        }else{
            $result = getAllUser();
        }

    while ($row = $result->fetch()){    
     echo "<tr>";
        echo "<td>" . $row['Id'] . "</td>";
        echo "<td>" . $row['Firstname'] . "</td>";
        echo "<td>" . $row['Surname'] . "</td>";
        echo "<td>" . $row['Email'] . "</td>";
        if ($row['Enabled'] == 0 ){
            echo "<td>No</td>";
        }else{
            echo "<td>Yes</td>";
        }
         echo '<td>';
            switch ($row['AccessLevel']){
                case 1 : echo 'Regular user';
                    break;
                case 2 : echo 'Admin';
                    break;
                case 3 : echo 'Superuser';
                    break;
            }
            echo '</td>';
        
        echo '<td> <input type="button" value="Edit" class="btn btn-info" onclick="grabForEdit( '. htmlspecialchars(json_encode($row)) .');"></td>';
        echo '<td> <button type="submit" form="resetPassForm" class="btn btn-warning btnReset" value="'. $row['Id'] .'">Reset Password</button></td>';
        echo '<td> <input type="button" value="Delete" class="btn btn-danger" onclick="grabForDelete(\''.$row['Id'].'\',\''.$row['Surname'].'\');"></td>';
     echo "</tr>";
    }

?>

</table>
</div>
<div id="addForm" class="addForm col-md-5" style="display:none;">
    
    <form role="form" action="users.php" method="post" class="form-horizontal"> 
        
        <label><h3>Add User</h3></label>
        
        <div class="form-group">
            <label for="fname">User Firstname</label>
            <input type="text" class="form-control" name="fname" id="fname" placeholder="Firstname">
          </div>
          <div class="form-group">
            <label for="sname">User Surname</label>
            <input type="text" class="form-control" id="sname" name="sname" placeholder="Surname" required>
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
          </div>
          <div class="form-group">
            <label for="pass">Password</label>
            <input type="password" class="form-control" id="pass" name="pass" placeholder="Password" required>
          </div>
          <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="enabled" name="enabled">Enabled
                </label>
            </div>
           </div>
           <div class="form-group">
            <select class="form-control" name="access" id="access">
                <option value="1" selected>Regular</option>
                <option value="2">Admin</option>
                <option value="3">Superuser</option>
            </select>
            </div>
            <div class="form-group">
                <div class="pull-right">
                <button type="button" class="btn btn-default" onclick="hideDiv('addForm'); showDiv('userTable');">Cancel</button>
                <button type="submit" class="btn btn-primary">Add User</button>
                </div>
            </div>
    </form>
</div>


<div id="editForm" class="addForm col-md-5" style="display:none;">
    
    <form role="form" action="users.php" method="post" class="form-horizontal"> 
        <label><h3>Update User Details</h3></label>
        <div class="form-group">
            <label for="eid">User Id</label>
            <input type="text" class="form-control" name="eid" id="eid" readonly>
        </div>
        <div class="form-group">
            <label for="efname">User Firstname</label>
            <input type="text" class="form-control" name="efname" id="efname" placeholder="Firstname">
          </div>
          <div class="form-group">
            <label for="esname">User Surname</label>
            <input type="text" class="form-control" id="esname" name="esname" placeholder="Surname" required>
          </div>
          <div class="form-group">
            <label for="eemail">Email</label>
            <input type="email" class="form-control" id="eemail" name="eemail" placeholder="Email" required>
          </div>
          <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="eenabled" name="eenabled">Enabled
                </label>
            </div>
           </div>
           <div class="form-group">
            <select class="form-control" name="eaccess" id="eaccess">
                <option value="1">Regular</option>
                <option value="2">Admin</option>
                <option value="3">Superuser</option>
            </select>
            </div>
            <div class="form-group">
                <div class="pull-right">
                <button type="button" class="btn btn-default" onclick="hideDiv('editForm'); showDiv('userTable');">Cancel</button>
                <button type="submit" class="btn btn-primary">Update Details</button>
                </div>
            </div>
</form>
</div>

<div id="delForm" style="display:none;">
    <form id="formDelete" action="users.php" class="form-inline" method="post"> 
        <p>Delete User:</p>    
        <p>
            <span class="bg-danger">Warning you are about to delete this record, this cannot be un-done!</span>
            Are you sure you want to remove :
        </p>
        <p>
            <div class="form-group">
            <input type="text" name="dname" id="dname" readonly="true" class="form-control"> </div> 
            Id :  
            <div class="form-group">
            <input type ="text" name="did" id="did" class="form-control" readonly="true"> </div>  ?
            <div class="form-group">
            <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('delForm');">
            <input type ="submit" value="Confirm Removal" class="btn btn-danger">
            </div> 
        </p>     
    </form> 
</div>
<script src="js/users.js"></script>
<?php
include ('includes/templates/footer.html');
?>