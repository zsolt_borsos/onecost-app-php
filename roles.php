<?php
$page_title = "Roles";
include ('includes/scripts/appfunctions.php');
session_start();
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

if ($_SESSION['Access'] < 2){
    setErrorMsg("You have no permission to see that page.");
    redirect_user('index.php');
}

include ('includes/templates/header.html');

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if (isset($_POST['name'])){
        $details = array();
        $details['name'] = $_POST['name'];
        $details['desc'] = $_POST['desc'];
        if (addRole($details)){
            setMsg("Role added.");   
            reloadMe();
        }else{
            setErrorMsg("Role could not be added.");
            reloadMe();
        }
    }
    if (isset($_POST['erid'])){
        $details = array();
        $details['name'] = $_POST['ename'];
        $details['desc'] = $_POST['edesc'];
        $details['id'] = $_POST['erid'];
        if (updateRole($details)){
            setMsg("Role updated.");
            reloadMe();
        }else{
            setErrorMsg("Role could not be updated.");
            reloadMe();
        }
    }
    
    if (isset($_POST['did'])){
        if (deleteRole($_POST['did'])){
            setMsg("Role deleted.");   
            reloadMe();
        }else{
            setErrorMsg("Role could not be deleted.");
            reloadMe();
        }   
        
    }
}

?>

<div class="page-header">
    <h2>User Roles</h2>
</div>

<div>
    <p>
        <button type="button" class="btn btn-primary btnAdd">Add Role</button>
        <input type="button" value="Toggle Search" class="btn btn-primary" onclick="toggle('divSearch');">
        <button type="button" class="btn btn-default" onclick="location.reload();">Reset Search</button>
    </p>
</div>    

<div id="divSearch" style="display:none;">
    <form id="search" action="roles.php" class="form-inline" method="post"> 
    <div class="form-group">
        <input type="text" name="txtSearch" id="txtSearch" placeholder="Search roles..." class="form-control"> </div>
        ||
        <div class="form-group">
        <input type ="submit" value="Search" class="btn btn-primary">
        </div> 
    </form>
</div>

<div class="table-responsive info" id="roleTable">
<p>List of user roles:</p>
    <table class="table table-bordered table-hover text-center">
		<tr>
		<!-- <th>Role Id</th> -->
		<th>Role Name</th>
        <th>Description</th>
        <th>Edit</th>
        <th>Delete</th>
		</tr>
        <?php 
        if (isset($_POST['txtSearch'])){
            $results = getRolesByName($_POST['txtSearch']);
        }else{
            $results = getAllRoles();
        }
            while ($row = $results->fetch()){
                echo '<tr>';
                    //echo '<td>' . $row['Id'] . '</td>';
                    echo '<td>' . $row['Name'] . '</td>';
                    echo '<td>' . $row['Description'] . '</td>';
                    echo '<td> <input type="button" value="edit" class="btn btn-info" onclick="grabForEdit(\''.$row['Id'].'\', \''.$row['Name'].'\', \''.$row['Description'].'\');"></td>';
                    echo '<td> <input type="button" value="delete" class="btn btn-danger"              onclick="grabForDelete(\''.$row['Id'].'\',\''.$row['Name'].'\');"></td>';
                    echo '</tr>';
            }
        
        ?>
    </table>
</div>

<div id="addForm" class="addForm" style="display:none;">
    <form class="form-inline" role="form" action="roles.php" method="post">
  <div class="form-group">
    <label class="sr-only" for="name">Role Name</label>
    <input type="text" class="form-control" name="name" id="name" placeholder="Role name">
  </div>
        ||
  <div class="form-group">
    <label class="sr-only" for="desc">Role Description</label>
    <input type="text" class="form-control" id="desc" name="desc" placeholder="Description">
  </div>
        ||
    <button type="button" class="btn btn-default" onclick="hideDiv('addForm'); showDiv('roleTable');">Cancel</button>
    <button type="submit" class="btn btn-primary">Add Role</button>
</form>
</div>


<div id="editForm" class="editForm" style="display:none;">
    <form class="form-inline" role="form" action="roles.php" method="post">
  
  <div class="form-group">
    <label class="sr-only" for="erid">Role Id</label>
    <input type="text" class="form-control" name="erid" id="erid" placeholder="Role Id" readonly>
  </div>
        ||
  <div class="form-group">
    <label class="sr-only" for="ename">Role Name</label>
    <input type="text" class="form-control" name="ename" id="ename" placeholder="Role name">
  </div>
        ||
  <div class="form-group">
    <label class="sr-only" for="edesc">Role Description</label>
    <input type="text" class="form-control" id="edesc" name="edesc" placeholder="Description">
  </div>
        ||
    <button type="button" class="btn btn-default" onclick="hideDiv('editForm');">Cancel</button>
    <button type="submit" class="btn btn-primary">Update Role</button>
</form>
</div>

<div id="divDelete" style="display:none;">
    <form id="formDelete" action="roles.php" class="form-inline" method="post"> 
        <p>Delete Role:</p>    
        <p>
            <span class="bg-danger">Warning you are about to delete this record, this cannot be un-done!</span>
            Are you sure you want to remove :
        </p>
        <p>
            <div class="form-group">
            <input type="text" name="dname" id="dname" readonly="true" class="form-control"> </div> 
            Id :  
            <div class="form-group">
            <input type ="text" name="did" id="did" class="form-control" readonly="true"> </div>  ?
            <div class="form-group">
            <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divDelete');">
            <input type ="submit" value="Confirm Removal" class="btn btn-danger">
            </div> 
        </p>     
    </form> 
</div>

<script type="text/javascript" src="js/roles.js"></script>
<?php
include ('includes/templates/footer.html');
?>
