<?php
$page_title = "Categories";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

include ('includes/templates/header.html');

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //print_r($_POST);
    if (isset($_POST['name'])){
        $details = array();
        $details['name'] = $_POST['name'];
        if (isset($_POST['sub'])){
            $details['pid'] = $_POST['pid'];               
        }else{
            $details['pid'] = 0;           
        }
        if (addCategory($details)){
            setMsg("Category added.");   
            reloadMe();
        }else{
            setErrorMsg("Category could not be added.");
            reloadMe();
        }    
        
    }
    if (isset($_POST['ecid'])){
        $details = array();
        $details['name'] = $_POST['ename'];
        $details['cid'] = $_POST['ecid'];
        if (isset($_POST['esub'])){
            $details['pid'] = $_POST['epid'];
        }else{
            $details['pid'] = 0;           
        }  
        if (updateCategory($details)){
            setMsg("Category updated.");
            reloadMe();
        }else{
            setErrorMsg("Category could not be updated.");
            reloadMe();
        }
    }
    
    if (isset($_POST['did'])){
        if (deleteCategory($_POST['did'])){
            setMsg("Category deleted.");   
            reloadMe();
        }else{
            setErrorMsg("Category could not be deleted.");
            reloadMe();
        }   
        
    }
    
}

?>


<div class="page-header">
    <h2>Product Categories</h2>
</div>

<div>
    <p>
        <button type="button" class="btn btn-primary btnAdd">Add Category</button>
        <input type="button" value="Toggle Search" class="btn btn-primary" onclick=" $('#divSearch').toggle();">
        <button type="button" class="btn btn-default" onclick="location.reload();">Reset Search</button>
    </p>
</div>    

<div id="divSearch" style="display:none;">
    <form id="search" action="category.php" class="form-inline" method="post"> 
    <div class="form-group">
        <input type="text" name="txtSearch" id="txtSearch" placeholder="Search categories..." class="form-control"> </div>
        ||
        <div class="form-group">
        <input type ="submit" value="Search" class="btn btn-primary">
        </div> 
    </form>
</div>

<div class="table-responsive info col-md-12" id="myTable">
<p>List of categories:</p>
    <table class="table table-bordered table-hover text-center">
		<tr>
		<!-- <th>Category Id</th> -->
		<th>Category Name</th>
        <th>Parent Category</th>
        <th>Edit</th>
        <th>Delete</th>
		</tr>
        <?php 
        if (isset($_POST['txtSearch'])){
            $results = getCategoryByName($_POST['txtSearch']);
        }else{
            $results = getAllCategory();
        }
        //print_r($results);
            while ($row = $results->fetch()){
                //print_r($row);
                echo '<tr>';
                    //echo '<td>' . $row['Id'] . '</td>';
                    echo '<td>' . $row['Name'] . '</td>';
                    // check if it is a main category or not
                    if ($row['pName']){
                        echo '<td>' . $row['pName'] . '</td>';
                    }else{
                        echo '<td>Main Category</td>';
                    }
                    echo '<td> <input type="button" value="edit" class="btn btn-info" onclick="grabForEdit(\''.$row['Id'].'\', \''.$row['Name'].'\', \''.$row['ParentId'].'\');"></td>';
                    echo '<td> <input type="button" value="delete" class="btn btn-danger"              onclick="grabForDelete(\''.$row['Id'].'\',\''.$row['Name'].'\');"></td>';
                    echo '</tr>';
            }
        
        ?>
    </table>
</div>

<div id="addForm" class="addForm" style="display:none;">
    <form class="form-inline" role="form" action="category.php" method="post">
        <div class="form-group">
            <label class="sr-only" for="name">Category Name</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Category name" required>
        </div>
        ||
        <div class="form-group">
            <div class="checkbox">
                <label>
                <input type="checkbox" id="sub" name="sub">Subcategory?
                </label>
            </div>
        </div>
        <div class="form-group" style="display:none;" id="parent">
            <label for="pid">Select parent category:</label>
        <?php
            //details: [sid] = id/name, [table] = table name, [col] = column to show, [valCol] = value for columns,
            $details = array();
            $details['sid'] = "pid";
            $details['table'] = "category";
            $details['col'] = "Name";
            $details['valCol'] = "Id";
            createSelect($details);
        ?>
        </div>
        ||
    <button type="button" class="btn btn-default" onclick="hideDiv('addForm'); showDiv('myTable');">Cancel</button>
    <button type="submit" class="btn btn-primary">Add Category</button>
</form>
</div>


<div id="editForm" class="editForm" style="display:none;">
    <form class="form-inline" role="form" action="category.php" method="post">
  
  <div class="form-group">
    <label class="sr-only" for="erid">Category Id</label>
    <input type="text" class="form-control" name="ecid" id="ecid" placeholder="Category Id" readonly>
  </div>
        ||
  <div class="form-group">
    <label class="sr-only" for="ename">Category Name</label>
    <input type="text" class="form-control" name="ename" id="ename" placeholder="Category name">
  </div>
        ||
    <div class="form-group">
        <div class="checkbox">
            <label>
            <input type="checkbox" id="esub" name="esub">Subcategory?
            </label>
        </div>
    </div>
  <div class="form-group" style="display:none;" id="eparent">
      <label for="epid">Select parent category:</label>
      <?php
        //details: [sid] = id/name, [table] = table name, [col] = column to show, [valCol] = columns for Value,
        $details = array();
        $details['sid'] = "epid";
        $details['table'] = "category";
        $details['col'] = "Name";
        $details['valCol'] = "Id";
        createSelect($details);
      ?>
  </div>
        ||
    <button type="button" class="btn btn-default" onclick="$('#editForm').toggle(false);">Cancel</button>
    <button type="submit" class="btn btn-primary">Update Category</button>
</form>
</div>


<div id="divDelete" style="display:none;">
    <form id="formDelete" action="category.php" class="form-inline" method="post" role="form"> 
        <p>Delete Category:</p>    
        <p>
            <span class="bg-danger">Warning you are about to delete this record, this cannot be un-done!</span>
            Are you sure you want to remove :
        </p>
        <p>
            <div class="form-group">
            <input type="text" name="dname" id="dname" readonly="true" class="form-control"> </div> 
            Id :  
            <div class="form-group">
            <input type ="text" name="did" id="did" class="form-control" readonly="true"> </div>  ?
            <div class="form-group">
            <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divDelete');">
            <input type ="submit" value="Confirm Removal" class="btn btn-danger">
            </div> 
        </p>     
    </form> 
</div>



<script type="text/javascript" src="js/category.js"></script>
<?php
include ('includes/templates/footer.html');
?>