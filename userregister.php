<?php # register.php
// This page handles user registration

$page_title = 'Register';
include ('includes/scripts/appfunctions.php');
include ('includes/templates/header.html');

function showErrorMsg($msg){
    echo '<div class="alert alert-danger alert-dismissable fade in">';
    echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    echo $msg;
    echo '</div>';   
    
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $sname = $_POST['sname'];
	$fname = $_POST['fname'];
	$pass = $_POST['pass'];
	$email = $_POST['email'];

	if ($sname && $fname && $email && $pass){
		
			//check for email if it is in the database already
			if (isEmailInDb($email)){
				showErrorMsg("This email is already registered in the database.");	
			}else{
				$pass = sha1($pass);
				if (registerUser($fname, $sname, $email, $pass)){
					setMsg("Registration successful.");
                    echo '<script type="text/javascript">
                            location.href="login.php";
                            </script>';     
				}else{
					showErrorMsg("Registration failed. Please try again.");
				}
			}	// end of email check
		
		//reload the page for the fancy msg
		
	} //end of variables check
}	//end of POST check

// Display the form:

include ('includes/forms/userregister.inc.php');

include ('includes/templates/footer.html');
?>
