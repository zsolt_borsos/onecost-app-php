<?php
$page_title = "Tracker";
include ('includes/scripts/appfunctions.php');
session_start();
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

// on this page I will need to connect to the db and check the stored values for each page.
// tracker table: page name, status, assigned
function getAllTask(){
    $db = connectDb();
	$sql = "SELECT * FROM tracker ORDER BY Status ASC";
	$result = $db->query($sql);
	$db = null;
    return $result;
}

include ('includes/templates/header.html');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
    if ($_POST['action'] == 'add'){
            $db = connectDb();
            $task = htmlspecialchars($_POST['tname']);
            if (isset($_POST['tnote'])){
                $note = htmlspecialchars($_POST['tnote']);
                $sql = "INSERT INTO tracker (Task, Notes) VALUES ('$task', '$note')";
            }else{
                $sql = "INSERT INTO tracker (Task) VALUES ('$task')";
            }
            $result = $db->query($sql);
            $db = null;
    }
    
    
            
    
    // if tid set
    if (isset($_POST['tid'])){
        
        if ($_POST['action'] == 'assign'){
            $name = $_SESSION['fname'] . ' ' . $_SESSION['sname'];
            $tid = $_POST['tid'];
            $db = connectDb();
            $sql = "UPDATE tracker SET Assigned = '$name' WHERE Id = '$tid'";
            $result = $db->query($sql);
            $db = null;
        }    
        
        if ($_POST['action'] == 'remove'){
            $db = connectDb();
            $tid = $_POST['tid'];
            $sql = "UPDATE tracker SET Assigned = NULL WHERE Id = '$tid'";
            $result = $db->query($sql);
            $db = null; 
        }
        
        if ($_POST['action'] == 'delete'){
            $db = connectDb();
            $tid = $_POST['tid'];
            $sql = "DELETE FROM tracker WHERE Id = '$tid'";
            $result = $db->query($sql);
            $db = null; 
        }
        
        if ($_POST['action'] == 'update'){
            $db = connectDb();
            $tid = $_POST['tid'];
            $status = $_POST['status'];
            if ((isset($_POST['utnote'])) && ($_POST['utnote'] != '')){
                $note = htmlspecialchars($_POST['utnote']);
                $sql = "UPDATE tracker SET Notes = '$note', Status = '$status' WHERE Id = '$tid'";
            }else{
             $sql = "UPDATE tracker SET Status = $status WHERE Id = '$tid'";
            }
            $result = $db->query($sql);
            $db = null; 
        }
        
    
    }
    
    
} // end of POST check

?>


<div id="page-header">
    <h2>Progress tracker</h2>
</div>

<div class="row">
    
    <div class="col-md-7 col-sm-7">
        <form class="form-inline" role="form" action="tracker.php" method="post" id="updateForm" name="updateForm">
            <h2>Update task</h2>
            <div class="form-group">
                <?php
                $details = array();
                $details['sid'] = "tid";
                $details['table'] = "tracker";
                $details['col'] = "Task";
                $details['valCol'] = "Id";
                createSelect($details);
                ?>
            </div>
            <div class="form-group">
                <select class="form-control" id="status" name="status">
                    <option value="0">Needs doing</option>
                    <option value="1">In progress</option>
                    <option value="2">Done</option>
                </select>
            </div>
             <div class="form-group">
                <label class="sr-only" for="utnote">Notes</label>
                <input type="text" class="form-control" name="utnote" id="utnote" placeholder="Task notes...">
            </div>
            <input type="hidden" name="action" id="action"  value="update">
        <button type="submit" class="btn btn-primary" form="updateForm">Update task</button>
    </form>
    </div>
    
    <div class="col-md-5 col-sm-5">
        <form class="form-inline" role="form" action="tracker.php" method="post" id="addForm" name="addForm">
            <h2>Add a new task</h2>
            <div class="form-group">
                <label class="sr-only" for="tname">Task Name</label>
                <input type="text" class="form-control" name="tname" id="tname" placeholder="Task name..." required>
            </div>
            
            <div class="form-group">
                <label class="sr-only" for="tnote">Notes</label>
                <input type="text" class="form-control" name="tnote" id="tnote" placeholder="Task notes...">
            </div>
            <input type="hidden" value="add" name="action" id="action">
        <button type="submit" form="addForm" class="btn btn-primary">Add Task</button>
    </form>
    </div>
    
</div>

<div class="table-responsive row">
		<table class="table table-bordered table-hover text-center">
		<tr>
            <th>Task</th>
            <th>Status</th>
            <th>Assigned To</th>
            <th>Notes</th>
            <th>Assign</th>
            <th>Remove Task</th>
		</tr>
        <?php 
        $results = getAllTask();
        while ($row = $results->fetch()){
            echo '<tr>';
                echo '<td>' . $row['Task'] . '</td>';
                echo '<td>';
                if ($row['Status'] == '0'){
                    echo 'Needs doing';   
                }else if($row['Status'] == '1'){
                    echo 'In progress';
                }else{
                    echo 'Done';   
                }
                echo '</td>';
                echo '<td>' . $row['Assigned'] .'</td>';
                echo '<td>';
                if (isset($row['Notes'])){
                    echo $row['Notes'];
                }
                echo '</td>';
                echo '<td>';
                if ($row['Assigned'] == ''){
                echo '<button type="button" class="btn btn-primary btnEdit" value="'. $row['Id'] .'">Assign Me</button>';
                }else{
                    $myName = $_SESSION['fname'] . ' ' . $_SESSION['sname'];
                    if ($row['Assigned'] == $myName){
                    echo '<button type="button" class="btn btn-warning btnRemove" value="'. $row['Id'] .'">Remove me</button>';
                    }            
                }
            echo '</td>
                    <td>
                        <button type="button" class="btn btn-danger btnDelete" value="'. $row['Id'] .'">Remove Task</button>
                    </td></tr>';
                
            
        }
        ?>
        
                   
            
        
       
    </table>
</div>

<script type="text/javascript" src="js/tracker.js"></script>


<?php
include ('includes/templates/footer.html');
?>

