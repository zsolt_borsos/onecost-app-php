<?php
$page_title = "CC Summary";
include ('includes/scripts/appfunctions.php');
include ('ccsummary.code.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}
include ('includes/templates/header.html');
/* POST TEMPLATE 
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //print_r($_POST); DEBUG INFO 
    if (isset($_POST["txtName"])) // Checks if the submit is from ADD NEW...
    {
        //connect to the database
        $db = connectDb();    
        //sql query as string
        $sql = "insert into client (Name, Actref, Notes) select :name, :actref, :notes ";    
        $stmt = $db->prepare($sql);
        $stmt->Execute();
    } 
}*/

?>

<div class="page-header"><h2>CC Summary</h2></div>

<div>
    <h4>Viewing Cost Comparison for : <?php if (isset($_GET["ccId"])){ getHeader($_GET["ccId"]);}   ?></h4>
</div>

<!--tabs -->
<div class="row">
    <div class="col-md-12">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
          <li class="active"><a href="#summary" data-toggle="tab">Summary</a></li>
          <li><a href="#pcsaving" data-toggle="tab">% Saving</a></li>
          <li><a href="#savingperitem" data-toggle="tab">Saving Per item</a></li>
          <li><a href="#totalsaving" data-toggle="tab">Total Saving</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <!-- SUMMARY -->
          <div class="tab-pane active" id="summary">
              <table class="table table-bordered table-hover text-center">
                  <tr>
                      <th colspan="4" class="btn-primary">Client Supplier</th>
                      <th></th>
                      <th colspan="4" class="btn-primary">OneCost Proposal</th>
                  </tr>
                  <tr>
                      <th>ProductCode</th>
                      <th>Description</th>
                      <th>Packsize</th>
                      <th>Price</th>
                      <th>QTY</th>
                      <th>ProductCode</th>
                      <th>Description</th>
                      <th>Packsize</th>
                      <th>Price</th>
                  </tr>
                  <?php 
                  if (isset($_GET["ccId"])){ $invId = $_GET["ccId"];} else {$invId = "x";}
                  summaryTableBuilder($invId);
                  ?> 
              </table>
          </div>
          <div class="tab-pane" id="pcsaving">
              <!--% saving --> 
                <table class="table table-bordered table-hover text-center">
                    <tr>
                        <th colspan="2" style="background-color:lightblue">Client Supplier</th> 
                        <th colspan="2" style="background-color:lightblue">OneCost Proposal</th>
                        <th style="background-color:lawngreen">% Saving </th> 
                    </tr>
                    <tr>
                        <th>Product Description</th>
                        <th>Pro Rata Price</th>
                        <th>Product Description</th>
                        <th>Pro Rata Price</th> 
                        <td style="background-color:lawngreen">(negative represents price increase!)</td>
                    </tr>
                    <?php 
                    if (isset($_GET["ccId"])){ $invId = $_GET["ccId"];} else {$invId = "x";}
                    percentageTableBuilder($invId);
                    ?> 
                    <tr>
                      <td colspan="4" class="pull-right">Avg Saving Per Item:</td>
                      <td>
                          <?php percentagePerItem_avg($invId); ?>
                      </td>
                  </tr>
                </table> 
          </div>
          <div class="tab-pane" id="savingperitem">
              <!--saving per item -->
              <table class="table table-bordered table-hover text-center">
                    <tr>
                        <th colspan="2" style="background-color:lightblue">Client Supplier</th> 
                        <th colspan="2" style="background-color:lightblue">OneCost Proposal</th>
                        <th style="background-color:lawngreen">Saving (&pound;)</th> 
                    </tr>
                    <tr>
                        <th>Product Description</th>
                        <th>Pro Rata Price</th>
                        <th>Product Description</th>
                        <th>Pro Rata Price</th> 
                        <td style="background-color:lawngreen">(negative represents price increase!)</td> 
                    </tr>
                    <?php 
                    if (isset($_GET["ccId"])){ $invId = $_GET["ccId"];} else {$invId = "x";}
                    savingPerItemTableBuilder($invId);
                    ?> 
                  <tr>
                      <td colspan="4" class="pull-right">Avg Saving Per Item:</td>
                      <td>
                          <?php savingPerItem_avg($invId); ?>
                      </td>
                  </tr>
                </table>
          </div>
          <div class="tab-pane" id="totalsaving">
              <!--saving per item -->
              <table class="table table-bordered table-hover text-center">
                    <tr>
                        <th colspan="2" style="background-color:lightblue">Client Supplier</th> 
                        <th colspan="2" style="background-color:lightblue">OneCost Proposal</th>
                        <th style="background-color:lawngreen">Saving Total (&pound;)</th> 
                    </tr>
                    <tr>
                        <th>Product Description</th>
                        <th>Pro Rata Price</th>
                        <th>Product Description</th>
                        <th>Pro Rata Price</th> 
                        <td style="background-color:lawngreen">(negative represents price increase!)</td> 
                    </tr>
                    <?php 
                    if (isset($_GET["ccId"])){ $invId = $_GET["ccId"];} else {$invId = "x";}
                    savingTotalTableBuilder($invId);
                    ?> 
                  <tr>
                      <td colspan="4" class="pull-right">Total Saving :</td>
                      <td>
                          <?php savingTotal_sum($invId); ?>
                      </td>
                  </tr>
                </table> 
          </div>
        </div>
    </div> 
</div>



<?php
include ('includes/templates/footer.html');
?>
