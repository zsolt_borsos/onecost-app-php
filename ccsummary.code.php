<?php
function summaryTableBuilder($id){
    //print_r($id); 
    //connect to the database
    $db = connectDb(); 
    if ($id == "x") {   $sql = "SELECT * FROM vwsummary "; }
    else { $sql = "SELECT * FROM vwsummary where ccId = " . $id . " order by xId"; }
    $result = $db->query($sql);
    $db = null;   
    
    while ($row = $result->fetch())
    {
        echo '<tr>';
        echo '<td>' .  $row['InvPC'] .  '</td>';
        echo '<td>' .  $row['InvDesc'] .  '</td>';
        echo '<td>' .  $row['InvPS'] .  '</td>';
        echo '<td>' .  $row['invPrice'] .  '</td>';
        echo '<td>' .  $row['Qty'] . '</td>';
        echo '<td>' .  $row['MatchPC'] .  '</td>';
        echo '<td>' .  $row['MatchDesc'] .  '</td>';
        echo '<td>' .  $row['matchPS'] .  '</td>';
        echo '<td>' .  $row['MatchPrice'] .  '</td>';
        echo '</tr>'; 
    } 
}

function percentageTableBuilder($id){
    //print_r($id); 
    //connect to the database
    $db = connectDb(); 
    if ($id == "x") {   $sql = "SELECT * FROM vwpcsaving "; }
    else { $sql = "SELECT * FROM vwpcsaving  where ccId = " . $id. " order by xId"; }
    $result = $db->query($sql);
    $db = null;   
    
    while ($row = $result->fetch())
    {
        echo '<tr>';
        echo '<td>' .  $row['InvDesc'] .  '</td>';
        echo '<td>' .  $row['InvProRata'] .  '</td>';
        echo '<td>' .  $row['MatchDesc'] .  '</td>';
        echo '<td>' .  $row['MatchProRata'] .  '</td>';
        echo '<td>' .  $row['Pc_Saving'] .  '</td>';
        echo '</tr>'; 
    } 
}


function savingPerItemTableBuilder($id){
    //print_r($id); 
    //connect to the database
    $db = connectDb(); 
    if ($id == "x") {   $sql = "SELECT * FROM vwsavingperitem "; }
    else { $sql = "SELECT * FROM vwsavingperitem where ccId = " . $id. " order by xId"; }
    $result = $db->query($sql);
    $db = null;   
    
    while ($row = $result->fetch())
    {
        echo '<tr>';
        echo '<td>' .  $row['InvDesc'] .  '</td>';
        echo '<td>' .  $row['InvProRata'] .  '</td>';
        echo '<td>' .  $row['MatchDesc'] .  '</td>';
        echo '<td>' .  $row['MatchProRata'] .  '</td>';
        echo '<td>' .  $row['SavingPerItem'] .  '</td>';
        echo '</tr>'; 
    } 
}

function savingTotalTableBuilder($id){
    //print_r($id); 
    //connect to the database
    $db = connectDb(); 
    if ($id == "x") {   $sql = "SELECT * FROM vwsavingtotal "; }
    else { $sql = "SELECT * FROM vwsavingtotal where ccId = " . $id. " order by xId"; }
    $result = $db->query($sql);
    $db = null;   
    
    while ($row = $result->fetch())
    {
        echo '<tr>';
        echo '<td>' .  $row['InvDesc'] .  '</td>';
        echo '<td>' .  $row['InvProRata'] .  '</td>';
        echo '<td>' .  $row['MatchDesc'] .  '</td>';
        echo '<td>' .  $row['MatchProRata'] .  '</td>';
        echo '<td>' .  $row['TotalSaving'] .  '</td>';
        echo '</tr>'; 
    } 
}

function percentagePerItem_avg($id){
    $db = connectDb(); 
    if ($id == "x") {   $sql = "SELECT cast((avg(Pc_Saving)) as decimal(7,2)) FROM vwpcsaving "; }
    else { $sql = "SELECT cast((avg(Pc_Saving)) as decimal(7,2)) FROM vwpcsaving where ccId = " . $id . " order by xId"; }
    $result = $db->query($sql);
    $db = null;   
    echo $result->fetchColumn();
}

function savingPerItem_avg($id){
    $db = connectDb(); 
    if ($id == "x") {   $sql = "SELECT cast((avg(SavingPerItem)) as decimal(7,2)) FROM vwsavingperitem "; }
    else { $sql = "SELECT cast((avg(SavingPerItem)) as decimal(7,2)) FROM vwsavingperitem where ccId = " . $id . " order by xId"; }
    $result = $db->query($sql);
    $db = null;   
    echo $result->fetchColumn();
}

function savingTotal_sum($id){
    $db = connectDb(); 
    if ($id == "x") {   $sql = "SELECT cast(sum(TotalSaving) as decimal(9,2)) FROM vwsavingtotal order by xId"; }
    else { $sql = "SELECT cast(sum(TotalSaving) as decimal(9,2)) FROM vwsavingtotal where ccId = " . $id . "  order by xId"; }
    $result = $db->query($sql);
    $db = null;   
    echo $result->fetchColumn();
}


function getHeader($id){
    $db = connectDb(); 
    $sql = "SELECT display from vwrpt_ccsummaryddl where Id = " . $id; 
    $result = $db->query($sql);
    $db = null;   
    echo $result->fetchColumn();
}





?>
