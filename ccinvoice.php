<?php
$page_title = "Manage Invoice";
include ('includes/scripts/appfunctions.php');
session_start();
//security check
// If no session value is present, redirect the user:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	redirect_user('login.php');	
}

//check for GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	
    //check for must have GET stuff (or redirect back to manager page?)
	if (!isset($_GET['ccId'])){
		echo("No CC Id.");
    }else{
        $ccId = $_GET['ccId'];
    }
    if (!isset($_GET['invId'])){
		echo("No Invoice Id.");
    }else{
        $invId = $_GET['invId'];   
    }
    /*
    if (isset($_GET['suppId'])){
        $suppId = $_GET['suppId'];
        $suppDetails = array();
        $supplier = getSupplier($suppId);
        $suppDetails = $supplier->fetch(PDO::FETCH_ASSOC);
    }
    */
    
    //get invoice info
    $invDetails = array();
    $invoice = getInvoice($invId);
    $invDetails = $invoice->fetch(PDO::FETCH_ASSOC);
    if ($invDetails['SupplierId'] != ''){
        $suppId = $invDetails['SupplierId'];
    }
    $imgPath = 'images/invoices/' . $invDetails['CcId'] . '/' . $invDetails['ImageName'];
    $notes = $invDetails['Notes'];
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
  
    
    
}

include ('includes/templates/header.html');
?>

<!-- Modal for: add supplier -->
<div class="modal fade" id="suppModal" tabindex="-1" role="dialog" aria-labelledby="suppModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="suppModalLabel">Add Supplier</h4>
        </div>
        <div class="modal-body">
            <form id="addSupp" name="addSupp" action="includes/scripts/invoiceHandler.php" method="post"> 
                
                <div class="form-group">
                    <input type="text" name="suppName" id="suppName" placeholder="Enter Name..." class="form-control" required="required">   
                </div> 
                
                <div class="form-group">
                    <input type="text" name="suppDesc" id="suppDesc" placeholder="Enter Description" class="form-control"> </div> 
                
                <div class="form-group">
                    <input type="text" name="suppNotes" id="suppNotes" placeholder="Enter Notes" class="form-control"> </div>
                
                <div class="form-group">
                    <select class="form-control" name="suppType" id="suppType">
                        <option value="0" >OneCost Supplier</option>
                        <option value="1" selected>Client Supplier</option>
                        <option value="2">Both</option>
                    </select>
                </div>    
            </form>    
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" form="addSupp"  class="btn btn-primary">Add Supplier</button>
        </div>
    </div>
  </div>
</div>



<!-- Modal for:  search for product in Db from selected Supplier-->
<div class="modal fade" id="prodModal" tabindex="-1" role="dialog" aria-labelledby="prodModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="prodModalLabel">Select a Product</h4>
        </div>
        <div class="modal-body">
            <form id="selectProd" name="selectProd" action="#" method="get">
                <div class="form-group">
                    <div class="input-group">
                    <input type="search" id="prodSearch" name="prodSearch" class="form-control" placeholder="Type to search...">
                    <span class="input-group-btn"><button class="btn btn-default resetBtn" type="button">Reset</button></span>
                    </div>
                </div>
                <div class="table-responsive" id="myTable">
                    <table class="table table-bordered table-hover text-center" id="prodSearchTable">
                        <tr>
                            <th scope="col">Code</th>
                            <th scope="col">Name</th>
                            <th scope="col">Packsize</th>
                            <th scope="col">Select</th>
                        </tr> 
                        <tbody id="prodBody">
                    <?php
                        if (isset($suppId)){
                            $result = getProductsBySupp($suppId);
                            $counter = 0;
                            while($row = $result->fetch()){
                                echo '<tr>';
                                echo '<td>' . $row['ProductCode'] . '</td>';
                                echo '<td>' . $row['Description'] . '</td>';
                                echo '<td>' . $row['Packsize'] . '</td>';
                                echo '<td> <button type="submit" value="'. $row['Id'] .'" class="btn btn-primary prodSelectBtn" for="selectProd">Select</button> </td>';
                                $counter++;
                            }
                            if ($counter == 0){
                                echo '<td colspan="4">No products from this supplier.</td>';   
                            }
                        }else{
                            echo '<td colspan="4">No supplier selected</td>';   
                        }
                    ?>
                        </tbody>
                    </table>
                </div>
                
            </form>    
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>



<!-- Top control panel-->

<div id="invoiceTop" class="navbar navbar-fixed-top mynav" role="form">
    <form class="form-inline" action="includes/scripts/invoiceHandler.php" method="post" name="invoiceTopForm" id="invoiceTopForm">
        <div class="form-group">
            <a href="ccmanager.php?ccId=<?php echo $ccId; ?>" class="btn btn-default backBtn">Back to CC Manager</a> 
        </div>
        
       
        
        <div class="form-group col-md-offset-1">
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#suppModal">Add supplier</button>
        </div>
        <div class="form-group">
            <label for="suppId">Select Supplier:</label>
              <?php
                if(isset($suppId)){
                echo '<input type="text" name="suppSel" id="suppSel" value="'. $invDetails['Name'].'" class="form-control" readonly>';
                }else{  
                $details = array();
                $details['sid'] = "suppId";
                $details['table'] = "supplier";
                $details['col'] = "Name";
                $details['valCol'] = "Id";
                createSelect($details);
                }  
            ?>
            
        </div>
        <div class="form-group">
            <label for="invDate">Invoice Date</label>
            <?php 
                if (isset($suppId)){
                    echo '<input type="date" name="date" id="date" class="form-control" value="'. $invDetails['InvoiceDate'] .'" readonly>';   
                }else{
                    echo '<input type="date" name="date" id="date" class="form-control" required>';   
                }
            ?>    
        </div>
        
        <div class="form-group">
            <?php
                if(isset($suppId)){
                    echo '<button type="button" class="btn btn-warning resetDetailsBtn">Reset details</button>';
                }else{
                    echo '<button type="submit" class="btn btn-info">Set details</button>';   
                }
            ?>
           
        </div>
       
        
         <!-- hidden stuff for post-->
            <input type="hidden" name="ccId" id="ccId" value="<?php echo $ccId; ?>">
            <input type="hidden" name="invId" id="invId" value="<?php echo $invId; ?>">
            <input type="hidden" name="action" id="action" value="setDetails">
         <!-- hidden stuff for post end-->
        
                

         <div class="form-group pull-right">
              <button type="button" class="btn btn-default notesBtn" data-container="body" data-toggle="popover" 
            data-placement="bottom" data-content="<span class='notesTxt'><?php if (isset($notes))echo $notes; ?></span>" data-html="true" title="Invoice notes">Notes</button>
            <?php
                if ($invDetails['Status'] == 0){
                    echo '<button type="button" class="btn btn-primary finishBtn" value="' .$invDetails['Status'].'" data-invid="'. $invId.'">Finish Invoice</button>';
                }else{
                     echo '<button type="button" class="btn btn-danger finishBtn" value="' .$invDetails['Status'].'" data-invid="'. $invId.'">Unlock Invoice</button>';
                }
                
            ?>
        </div>
    </form>

</div>
<!-- Top control panel end-->



<!-- Add form-->
    <div id="addForm" class="invoiceAddForm well" role="form">
        <form action="includes/scripts/invoiceHandler.php" method="post" id="addProdForm" name="addProdForm" class="form-horizontal">         
            <div class="form-group">
                <label>Add Products to Invoice</label>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#prodModal">
                        Select Product
                    </button>
            </div>        
            
            <div class="form-group">
                <label for="prodCode">Product Code</label>
                <div class="input-group">
                    <input type="text" name="prodCode" id="prodCode" class="form-control" placeholder="Enter product code">
                    <span class="input-group-addon">
                        <input type="checkbox" id="prodCheck" name="prodCheck" checked>
                    </span>
                </div> 
            </div>
                
                <div class="form-group">
                    <label for="prodDesc">Name/Description</label>
                    <input type="text" name="prodDesc" id="prodDesc" placeholder="Enter Name/Description" class="form-control" required> 
                </div> 
                
                <div class="form-group">
                     <label for="prodPack">Packsize</label>
                    <input type="text" name="prodPack" id="prodPack" placeholder="Enter Packsize" class="form-control" required> 
                </div>
            
            
            <div class="form-group">
                <label for="qty">Quantity</label>
                <input type="text" class="form-control" id="qty" name="qty" placeholder="Quantity" required>
            </div>
            
            <div class="form-group">
                <label for="total">Total Paid</label>
                <input type="text" class="form-control" id="total" name="total" placeholder="Total paid" required>
            </div>
            <div class="form-group">
            <button type="submit" form="addProdForm" class="btn btn-primary pull-right">Add Product</button>
            </div>
            <input type="hidden" name="inDb" id="inDb" value="0">
            <input type="hidden" name="ccId" id="ccId" value="<?php echo $ccId; ?>">
            <input type="hidden" name="invId" id="invId" value="<?php echo $invId; ?>">
            <input type="hidden" name="suppId" id="suppId" <?php if(isset($suppId)) echo ' value="' .$suppId. '"';?> >
            
        </form>
    </div>
<!-- Add form-->

<!-- Page Title-->
<div class="col-md-offset-2 col-lg-offset-2 col-sm-offset-2 col-xs-offset-2">

    <div class="page-header">
        <br>
        
        <h2 id="mainTitle">
            <?php 
                if ($invDetails['Status'] == 0){
                    echo 'Manage Invoice:';
                }else{
                   echo "This invoice is now locked.";
                }
            ?>
        </h2>
    </div>



    <!-- Invoice items-->
    <div class="row" id="recordsTable">
        <div class="table-responsive">
            <table class="table table-bordered table-hover text-center">
                <tr>
                    <th scope="col">Product Code</th>
                    <th scope="col">Description</th>
                    <th scope="col">Packsize</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Price</th>
                    <th scope="col">Delete</th>
                    <th scope="col">Edit</th>
                </tr>
                    <?php
                        $result = getProductInInvoice($invId);
                        while($row = $result->fetch(PDO::FETCH_ASSOC)){
                            //print_r($row);
                            echo '<tr>';
                            echo '<td>' . $row['ProductCode'] . '</td>';
                            echo '<td>' . $row['Description'] . '</td>';
                            echo '<td>' . $row['Packsize'] . '</td>';
                            echo '<td>' . $row['Qty'] . '</td>';
                            echo '<td>' . $row['PriceLeft'] . '</td>';
                            echo '<td> <button type="button" data-remove-info="'.htmlspecialchars($row["Description"]).'" value="'. $row['pilId'] .'" class="btn btn-danger delBtn">Remove</button> </td>';
                            echo '<td> <button type="button" data-edit-info="'. htmlspecialchars(json_encode($row)) .'" class="btn btn-info editBtn" ">Edit</button> </td>';
                            echo '</tr>';
                        }
                        
                        ?>
            </table>
        </div>
    </div>
    <!-- Invoice items end-->
    
    
    <!-- Edit item in invoice-->
    <div id="editItem" style="display:none;">
    <form action="includes/scripts/invoiceHandler.php" method="post" id="editItemForm" name="editItemForm" class="form-inline">      
        <div class="form-group">
            <label for="eprodCode" class="sr-only">Product Code</label>
            <input type="text" name="eprodCode" id="eprodCode" placeholder="Enter Product Code" class="form-control" required="required">   
        </div> 
            
        <div class="form-group">
            <label for="eprodDesc" class="sr-only">Name/Description</label>
            <input type="text" name="eprodDesc" id="eprodDesc" placeholder="Enter Name/Description" class="form-control" required> 
        </div> 
        
        <div class="form-group">
             <label for="eprodPack" class="sr-only">Packsize</label>
            <input type="text" name="eprodPack" id="eprodPack" placeholder="Enter Packsize" class="form-control" required> 
        </div>
    
        
        <div class="form-group">
            <label for="eqty" class="sr-only">Quantity</label>
            <input type="text" class="form-control" id="eqty" name="eqty" placeholder="Quantity" required>
        </div>
        
        <div class="form-group">
            <label for="etotal" class="sr-only">Total Paid</label>
            <input type="text" class="form-control" id="etotal" name="etotal" placeholder="Total paid" required>
        </div>
        <div class="form-group">
            <button type="button" class="btn btn-default" onclick="$('#editItem').toggle(false);">Cancel</button>
            <input type="submit" form="editItemForm" class="btn btn-primary" value="Update details">
        </div>
        <input type="hidden" name="prodLeftId" id="prodLeftId">
        <input type="hidden" name="priceLeftId" id="priceLeftId">
        <input type="hidden" name="pilId" id="pilId"> 
        <input type="hidden" name="ccId" id="ccId" value="<?php echo $ccId; ?>">
        <input type="hidden" name="invId" id="invId" value="<?php echo $invId; ?>">
        <input type="hidden" name="suppId" id="suppId" <?php if(isset($suppId)) echo ' value="' .$suppId. '"';?> >
        </form>
    
    </div>
    
    
    <!-- Edit item in invoice end-->
    
    <!-- Delete item from invoice -->
    
    <div id="divDelete" style="display:none;">
        <form id="formDelete" action="includes/scripts/invoiceHandler.php" class="form-inline" method="post" role="form"> 
            Are you sure you want to remove this product from the invoice?:
            <p>
                <div class="form-group">
                <input type="text" name="dname" id="dname" readonly="true" class="form-control"> 
                <input type="hidden" name="did" id="did">
                <input type="hidden" name="ccId" id="ccId" value="<?php echo $ccId; ?>">
                <input type="hidden" name="invId" id="invId" value="<?php echo $invId; ?>">
                <input type="hidden" name="suppId" id="suppId" <?php if(isset($suppId)) echo ' value="' .$suppId. '"';?> >
                </div>
                
                <div class="form-group">
                <input type="button" value="Cancel" class="btn btn-default" onclick="hideDiv('divDelete');">
                <input type ="submit" value="Confirm Removal" class="btn btn-danger">
                </div> 
            </p>     
        </form> 
    </div>
    
    <!-- Delete item from invoice end-->
    
    
    <!-- Invoice image and add product form-->
    <div class="row">
        <div class="page-header">
            <h3>Invoice Details: <span><button type="button" class="btn btn-default rotate">Rotate</button></span></h3>
        </div>
        
    
        <div id="image" class="invoiceImg">
            <img src="<?php echo $imgPath; ?>" width="100%">
        </div>
        
    </div>
<!-- Invoice image and add product form end-->
</div>


<script type="text/javascript" src="js/ccinvoice.js"></script>

<?php
include ('includes/templates/footer.html');
?>
